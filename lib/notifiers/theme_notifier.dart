import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeNotifier extends ChangeNotifier {
  final String key = "theme";
  SharedPreferences _sharedPreferences;
  bool _isDarkTheme = false;

  ThemeNotifier() {
    _loadFromPreference();
  }

  bool get getTheme => _isDarkTheme;

  void switchTheme() {
    _isDarkTheme = !_isDarkTheme;
    _saveToPreference();
    notifyListeners();
  }

  void _initSharedPreference() async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
  }

  void _loadFromPreference() async {
    await _initSharedPreference();
    _isDarkTheme = _sharedPreferences.getBool(key) ?? false;
    notifyListeners();
  }

  void _saveToPreference() async {
    _initSharedPreference();
    _sharedPreferences.setBool(key, _isDarkTheme);
  }

}
