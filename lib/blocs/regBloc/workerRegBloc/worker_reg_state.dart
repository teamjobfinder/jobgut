import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:meta/meta.dart';

abstract class WorkerRegState extends Equatable {}

// 1.
class WorkerRegInitial extends WorkerRegState {
  @override
  List<Object> get props => null;
}

// 2.
class WorkerRegLoading extends WorkerRegState {
  @override
  List<Object> get props => null;
}

// 3. same state for new reg and link
class WorkerRegSuccess extends WorkerRegState {
  WorkerModel workerModel;

  WorkerRegSuccess({
    @required this.workerModel,
  });

  @override
  List<Object> get props => null;
}

// 4.
class WorkerRegFailure extends WorkerRegState {
  String message;

  WorkerRegFailure({@required this.message});

  @override
  List<Object> get props => null;
}

// 5. for google, facebook
class ThirdPartySignInEmployer extends WorkerRegState {
  @override
  List<Object> get props => null;
}

// 6. for google, facebook
class ThirdPartySignInWorker extends WorkerRegState {
  @override
  List<Object> get props => null;
}

// 7. for google, facebook
class LinkWithThirdPartySuccess extends WorkerRegState {
  @override
  List<Object> get props => null;
}
