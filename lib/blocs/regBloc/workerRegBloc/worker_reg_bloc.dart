import 'package:jobs/blocs/regBloc/workerRegBloc/worker_reg_event.dart';
import 'package:jobs/blocs/regBloc/workerRegBloc/worker_reg_state.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

class WorkerRegBloc extends Bloc<WorkerRegEvent, WorkerRegState> {
  WorkersRepository workersRepository;

  WorkerRegBloc({@required this.workersRepository});

  @override
  WorkerRegState get initialState => WorkerRegInitial();

  @override
  Stream<WorkerRegState> mapEventToState(WorkerRegEvent event) async* {
    if (event is WorkerRegButtonPressed) {
      yield* mapWorkerRegToState(event);
    } else if (event is GoogleSignInBtnPressed) {
      yield* mapGoogleSignInToState(event);
    } else if (event is LinkGuestWorkerWithEmail) {
      yield* mapLinkWithEmailEventToState(event);
    } else if (event is LinkGuestWorkerWithGmail) {
      yield* mapLinkWithGoogleToState(event);
    } else if (event is FacebookSignInBtnPressed) {
      yield* _mapFbSignInEventToState(event);
    } else if (event is LinkGuestWorkerWithFacebook) {
      yield* _mapLinkWithFacebookToState(event);
    }
  }

  Stream<WorkerRegState> mapWorkerRegToState(
      WorkerRegButtonPressed event) async* {
    yield WorkerRegLoading();
    try {
      await workersRepository.registerWithEmailpassword(
        email: event.email,
        password: event.password,
      );
      await workersRepository.addWorker(event.workerModel);
      await workersRepository.addInUsers();
      yield WorkerRegSuccess(workerModel: event.workerModel);
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

  Stream<WorkerRegState> mapGoogleSignInToState(
      GoogleSignInBtnPressed event) async* {
    yield WorkerRegLoading();
    try {
      String userType = await workersRepository.signInWithGoogle();
      if (userType == AppStringConstants.workerType) {
        yield ThirdPartySignInWorker();
      } else if (userType == AppStringConstants.employerType) {
        yield ThirdPartySignInEmployer();
      }
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

  Stream<WorkerRegState> mapLinkWithEmailEventToState(
      LinkGuestWorkerWithEmail event) async* {
    yield WorkerRegLoading();
    try {
      await workersRepository.linkWithEmail(
        email: event.email,
        password: event.password,
      );
      await workersRepository.addWorker(event.workerModel);
      await workersRepository.addInUsers();
      yield WorkerRegSuccess(workerModel: event.workerModel);
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

  Stream<WorkerRegState> mapLinkWithGoogleToState(
      LinkGuestWorkerWithGmail event) async* {
    yield WorkerRegLoading();
    try {
      await workersRepository.linkWithGoogle();
      yield LinkWithThirdPartySuccess();
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

  Stream<WorkerRegState> _mapLinkWithFacebookToState(
      LinkGuestWorkerWithFacebook event) async* {
    yield WorkerRegLoading();
    try {
      await workersRepository.linkWithFacebook();
      yield LinkWithThirdPartySuccess();
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

  // map fb sign in btn press to state
  Stream<WorkerRegState> _mapFbSignInEventToState(
      FacebookSignInBtnPressed event) async* {
    yield WorkerRegLoading();
    try {
      String userType = await workersRepository.signInWithFacebook();
      if (userType == AppStringConstants.workerType) {
        yield ThirdPartySignInWorker();
      } else if (userType == AppStringConstants.employerType) {
        yield ThirdPartySignInEmployer();
      }
    } catch (e) {
      yield WorkerRegFailure(message: e.toString());
    }
  }

}
