import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:meta/meta.dart';

// guest sign in btn pressed is handled by LoginBloc
abstract class WorkerRegEvent extends Equatable {}

// 1.
class WorkerRegButtonPressed extends WorkerRegEvent {
  String email, password;
  WorkerModel workerModel;

  WorkerRegButtonPressed({
    @required this.email,
    @required this.password,
    @required this.workerModel,
  });

  @override
  List<Object> get props => null;
}

// 2.
class GoogleSignInBtnPressed extends WorkerRegEvent {
  @override
  List<Object> get props => null;
}

// 3. link guest with email with
class LinkGuestWorkerWithEmail extends WorkerRegEvent {
  String email, password;
  WorkerModel workerModel;

  LinkGuestWorkerWithEmail({
    @required this.email,
    @required this.password,
    @required this.workerModel,
  });

  @override
  List<Object> get props => null;
}

// 4. link guest with gmail, facebook
class LinkGuestWorkerWithGmail extends WorkerRegEvent {
  @override
  List<Object> get props => null;
}

// 5. facebook sign in
class FacebookSignInBtnPressed extends WorkerRegEvent {
  @override
  List<Object> get props => null;
}

class LinkGuestWorkerWithFacebook extends WorkerRegEvent {
  @override
  List<Object> get props => null;
}
