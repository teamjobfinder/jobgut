import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:meta/meta.dart';

abstract class EmployerRegState extends Equatable {}

// 1.
class EmployerRegInitial extends EmployerRegState {
  @override
  List<Object> get props => null;
}

// 2.
class EmployerRegLoading extends EmployerRegState {
  @override
  List<Object> get props => null;
}

// 3.
class EmployerRegSuccess extends EmployerRegState {
  EmployerModel employerModel;

  EmployerRegSuccess({
    @required this.employerModel,
  });

  @override
  List<Object> get props => null;
}

// 4.
class EmployerRegFailure extends EmployerRegState {
  String message;

  EmployerRegFailure({@required this.message});

  @override
  List<Object> get props => null;
}

// 5. for facebook, goole
class ThirdPartySignInEmployer extends EmployerRegState {
  @override
  List<Object> get props => null;
}

// 6. for facebook, goole
class ThirdPartySignInWorker extends EmployerRegState {
  @override
  List<Object> get props => null;
}

