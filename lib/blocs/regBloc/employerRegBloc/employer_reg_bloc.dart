import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/regBloc/employerRegBloc/employer_reg_event.dart';
import 'package:jobs/blocs/regBloc/employerRegBloc/employer_reg_state.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:meta/meta.dart';
import 'package:jobs/data/repositories/employers_repository.dart';

class EmployerRegBloc extends Bloc<EmployerRegEvent, EmployerRegState> {
  EmployersRepository employersRepository;

  EmployerRegBloc({@required this.employersRepository});

  @override
  EmployerRegState get initialState => EmployerRegInitial();

  @override
  Stream<EmployerRegState> mapEventToState(EmployerRegEvent event) async* {
    if (event is EmployerRegBtnPressed) {
      yield* mapEmployerRegBtnPressedToState(event);
    } else if (event is GoogleSignInBtnPressed) {
      yield* mapGoogleSignInBtnPressToState(event);
    } else if (event is FacebookSignInBtnPressed) {
      yield* mapFbSignInEventToState(event);
    }
  }

  // map reg btn press
  Stream<EmployerRegState> mapEmployerRegBtnPressedToState(
      EmployerRegBtnPressed event) async* {
    yield EmployerRegLoading();
    try {
      await employersRepository.registerWithEmailpassword(
        email: event.email,
        password: event.password,
      );
      await employersRepository.addEmployer(event.employerModel);
      await employersRepository.addInUsers();
      yield EmployerRegSuccess(employerModel: event.employerModel);
    } catch (e) {
      yield EmployerRegFailure(message: e.toString());
    }
  }

  // map google sign in btn press to state
  Stream<EmployerRegState> mapGoogleSignInBtnPressToState(
      GoogleSignInBtnPressed event) async* {
    yield EmployerRegLoading();
    try {
      String userType = await employersRepository.signInWithGoogle();
      if (userType == AppStringConstants.workerType) {
        yield ThirdPartySignInWorker();
      } else if (userType == AppStringConstants.employerType) {
        yield ThirdPartySignInEmployer();
      }
    } catch (e) {
      yield EmployerRegFailure(message: e.toString());
    }
  }

  // map fb sign in btn press to state
  Stream<EmployerRegState> mapFbSignInEventToState(
      FacebookSignInBtnPressed event) async* {
    yield EmployerRegLoading();
    try {
      String userType = await employersRepository.signInWithFacebook();
      if (userType == AppStringConstants.workerType) {
        yield ThirdPartySignInWorker();
      } else if (userType == AppStringConstants.employerType) {
        yield ThirdPartySignInEmployer();
      }
    } catch (e) {
      yield EmployerRegFailure(message: e.toString());
    }
  }
}
