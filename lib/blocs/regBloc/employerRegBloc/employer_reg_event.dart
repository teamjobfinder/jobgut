import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:meta/meta.dart';

abstract class EmployerRegEvent extends Equatable {}

// 1.
class EmployerRegBtnPressed extends EmployerRegEvent {
  String email, password;
  EmployerModel employerModel;

  EmployerRegBtnPressed({
    @required this.email,
    @required this.password,
    @required this.employerModel,
  });

  @override
  List<Object> get props => null;
}

// 2.
class GoogleSignInBtnPressed extends EmployerRegEvent {

  @override
  List<Object> get props => null;
  
}

// 3.
class FacebookSignInBtnPressed extends EmployerRegEvent {
  @override
  List<Object> get props => null;
}
