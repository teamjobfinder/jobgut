import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:meta/meta.dart';

abstract class ActiveJobsState extends Equatable {}

// 1.
class ActiveJobsLoading extends ActiveJobsState {
  @override
  List<Object> get props => [];
}

// 2.
class ActiveJobsLoaded extends ActiveJobsState {
  final List<JobPostDocumentModel> activeJobs;

  ActiveJobsLoaded({@required this.activeJobs});

  @override
  List<Object> get props => [activeJobs];
}

// 3.
class NoActiveJobs extends ActiveJobsState {
  @override
  List<Object> get props => [];
}

// 4.
class ActiveJobsLoadFailure extends ActiveJobsState {
  final String message;

  ActiveJobsLoadFailure({@required this.message});
  @override
  List<Object> get props => [message];
}

// 5.
class JobPostSuccessFullyDeleted extends ActiveJobsState {
  @override
  List<Object> get props => [];
}

// 6.
class JobPostDeleteFailure extends ActiveJobsState {
  final String message;

  JobPostDeleteFailure({@required this.message});
  @override
  List<Object> get props => [message];
}
