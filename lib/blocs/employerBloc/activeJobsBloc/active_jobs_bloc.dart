import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/employerBloc/activeJobsBloc/active_jobs_event.dart';
import 'package:jobs/blocs/employerBloc/activeJobsBloc/active_jobs_state.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/utils/helper.dart';

class ActiveJobsBloc extends Bloc<ActiveJobsEvent, ActiveJobsState> {
  final String _TAG = "ActiveJobsBloc";
  StreamSubscription _activeJobSubscription;

  JobsRepository _jobsRepository = JobsRepository();
  EmployersRepository _employersRepository = EmployersRepository();

  @override
  ActiveJobsState get initialState => ActiveJobsLoading();

  @override
  Stream<ActiveJobsState> mapEventToState(ActiveJobsEvent event) async* {
    if (event is FetchActiveJobs) {
      Helper.logPrint(_TAG, "event : ${event.toString()}");
      yield* _mapFetchActiveJobsToState(event);
    } else if (event is ActiveJobsUpdated) {
      Helper.logPrint(_TAG, "event : ${event.toString()}");
      yield* _mapActiveJobsUpdatedToState(event);
    } else if (event is DeleteJobPost) {
      yield* _mapDeleteJobPostToState(event);
    }
  }

  // 1.
  Stream<ActiveJobsState> _mapFetchActiveJobsToState(
      FetchActiveJobs event) async* {
    String userId = await _employersRepository.getUserId();
    try {
      _activeJobSubscription =
          _jobsRepository.getJobPostsOfAnEmployer(userId).listen((jobs) {
        add(ActiveJobsUpdated(jobs: jobs));
      });
    } catch (e) {
      yield ActiveJobsLoadFailure(message: e.toString());
    }
  }

  // 2.
  Stream<ActiveJobsState> _mapActiveJobsUpdatedToState(
      ActiveJobsUpdated event) async* {
    int numOfActiveJobs = event.jobs.length;
    if (numOfActiveJobs == 0) {
      yield NoActiveJobs();
    } else {
      yield ActiveJobsLoaded(activeJobs: event.jobs);
    }
  }

  // 3.
  Stream<ActiveJobsState> _mapDeleteJobPostToState(DeleteJobPost event) async* {
    try {
      await _jobsRepository
          .deleteAJobPost(event.documentReference);
      yield JobPostSuccessFullyDeleted();
    } catch (e) {
      yield JobPostDeleteFailure(message: e.toString());
    }
  }

  @override
  Future<void> close() {
    _activeJobSubscription?.cancel();
    return super.close();
  }
}
