import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:meta/meta.dart';

abstract class ActiveJobsEvent extends Equatable {}

// 1.
class FetchActiveJobs extends ActiveJobsEvent {
  @override
  List<Object> get props => [];
}

// 2.
class ActiveJobsUpdated extends ActiveJobsEvent {
  final List<JobPostDocumentModel> jobs;

  ActiveJobsUpdated({@required this.jobs});

  @override
  List<Object> get props => [jobs];
}

class EditJobPost extends ActiveJobsEvent {
  final JobPostDocumentModel jobPostDocumentModel;

  EditJobPost({@required this.jobPostDocumentModel});

  @override
  List<Object> get props => [jobPostDocumentModel];
}

// 4.
class DeleteJobPost extends ActiveJobsEvent {
  final DocumentReference documentReference;

  DeleteJobPost({@required this.documentReference});

  @override
  List<Object> get props => [documentReference];
}

class ShareJobPost extends ActiveJobsEvent {
  final JobPostDocumentModel jobPostDocumentModel;

  ShareJobPost({@required this.jobPostDocumentModel});
  @override
  List<Object> get props => [jobPostDocumentModel];
}
