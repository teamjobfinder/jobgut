import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PhoneVerificationEvent extends Equatable {}

// 1.
class CheckVerificationStatus extends PhoneVerificationEvent {
  @override
  List<Object> get props => null;
}

// 2.
class SubmitPhoneNumber extends PhoneVerificationEvent {

  final String phoneNumber;

  SubmitPhoneNumber({@required this.phoneNumber});

  @override
  List<Object> get props => null;
}

// 3.
class SubmitOTP extends PhoneVerificationEvent {

  final String otp;

  SubmitOTP({@required this.otp});

  @override
  List<Object> get props => null;
}
