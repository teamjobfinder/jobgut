import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PhoneVerificationState extends Equatable {}

// 1.
class PhoneVerificationInitial extends PhoneVerificationState {
  @override
  List<Object> get props => null;
}

// 2.
class PhoneVerificationLoading extends PhoneVerificationState {
  @override
  List<Object> get props => null;
}

// 3.
class OTPSent extends PhoneVerificationState {
  @override
  List<Object> get props => null;
}

// 4.
class PhoneVerificationSuccess extends PhoneVerificationState {
  @override
  List<Object> get props => null;
}

// 5.
class PhoneVerificationFailure extends PhoneVerificationState {
  final String message;

  PhoneVerificationFailure({@required this.message});

  @override
  List<Object> get props => null;
}
