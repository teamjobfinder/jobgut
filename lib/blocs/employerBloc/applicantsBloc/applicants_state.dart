import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:meta/meta.dart';

abstract class ApplicantsState extends Equatable {}

class ApplicantsInitial extends ApplicantsState {
  @override
  List<Object> get props => [];
}

class ApplicantsLoading extends ApplicantsState {
  @override
  List<Object> get props => [];
}

class ApplicantsLoaded extends ApplicantsState {

  final List<WorkerModel> applicants;

  ApplicantsLoaded({@required this.applicants});

  @override
  List<Object> get props => [applicants];
}

class ApplicantsLoadFailure extends ApplicantsState {

  final String message;

  ApplicantsLoadFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
