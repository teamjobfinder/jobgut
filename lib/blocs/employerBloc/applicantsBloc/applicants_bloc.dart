import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/employerBloc/applicantsBloc/applicants_event.dart';
import 'package:jobs/blocs/employerBloc/applicantsBloc/applicants_state.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/employers_repository.dart';

class ApplicantsBloc extends Bloc<ApplicantsEvent, ApplicantsState> {
  StreamSubscription _applicantsIdListSubscription;
  EmployersRepository _employersRepository = EmployersRepository();

  @override
  ApplicantsState get initialState => ApplicantsInitial();

  @override
  Stream<ApplicantsState> mapEventToState(ApplicantsEvent event) async* {
    if (event is FetchApplicantsId) {
      yield* _mapFetchApplicantsIdToState(event);
    } else if (event is UpdatedListOfApplicantsId) {
      yield* _mapUpdatedListOfApplicantsIdToState(event);
    }
  }

  Stream<ApplicantsState> _mapFetchApplicantsIdToState(
      FetchApplicantsId event) async* {
    yield ApplicantsLoading();
    try {
      _applicantsIdListSubscription?.cancel();
      _applicantsIdListSubscription = _employersRepository
          .getListOfApplicantsId(event.documentReference)
          .listen((ids) {
        add(UpdatedListOfApplicantsId(applicantsId: ids));
      });
    } catch (e) {
      yield ApplicantsLoadFailure(message: e.toString());
    }
  }

  Stream<ApplicantsState> _mapUpdatedListOfApplicantsIdToState(
      UpdatedListOfApplicantsId event) async* {
    try {
      List<WorkerModel> applicants =
          await _employersRepository.getApplicants(event.applicantsId);
      yield ApplicantsLoaded(applicants: applicants);
    } catch (e) {
      yield ApplicantsLoadFailure(message: e.toString());
    }
  }

  @override
  Future<void> close() {
    _applicantsIdListSubscription?.cancel();
    return super.close();
  }
}
