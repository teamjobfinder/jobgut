import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ApplicantsEvent extends Equatable {}

class FetchApplicantsId extends ApplicantsEvent {
  final DocumentReference documentReference;

  FetchApplicantsId({@required this.documentReference});
  @override
  List<Object> get props => [documentReference];
}

class UpdatedListOfApplicantsId extends ApplicantsEvent {

  final List<String> applicantsId;

  UpdatedListOfApplicantsId({@required this.applicantsId});

  @override
  List<Object> get props => [applicantsId];
}