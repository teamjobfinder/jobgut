import 'dart:async';
import 'package:jobs/blocs/employerBloc/employerProfilePageBloc/employer_profile_page_event.dart';
import 'package:jobs/blocs/employerBloc/employerProfilePageBloc/employer_profile_page_state.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:bloc/bloc.dart';

class EmployerProfilePageBloc
    extends Bloc<EmployerProfilePageEvent, EmployerProfilePageState> {
      final String _TAG = "EmployerProfilePageBloc";
  EmployersRepository _employersRepository = EmployersRepository();
  StreamSubscription _employerProfileSubscription;
  
  @override
  EmployerProfilePageState get initialState => EmployerProfilePageInitial();

  @override
  Stream<EmployerProfilePageState> mapEventToState(
      EmployerProfilePageEvent event) async* {
    if (event is GetEmployerProfile) {
      yield* _mapGetEmployerProfileToState(event);
    } else if (event is EmployerProfileUpdated) {
      yield* _mapEmployerProfileUpdatedToState(event);
    } else if (event is SignOutEmployer) {
      yield* _mapSignOutEmployerToState(event);
    }
  }

  Stream<EmployerProfilePageState> _mapGetEmployerProfileToState(
      GetEmployerProfile event) async* {
    yield EmployerProfilePageLoading();
    String userId = await _employersRepository.getUserId();
    try {
      _employerProfileSubscription =
          _employersRepository.getEmployer(userId).listen((employerModel) {
        add(EmployerProfileUpdated(employerModel: employerModel));
      });
    } catch (e) {
      yield EmployerProfilePageError(message: e.toString());
    }
  }

  Stream<EmployerProfilePageState> _mapEmployerProfileUpdatedToState(
      EmployerProfileUpdated event) async* {
    yield EmployerProfilePageLoaded(employerModel: event.employerModel);
  }


  Stream<EmployerProfilePageState> _mapSignOutEmployerToState(SignOutEmployer event) async* {
    await _employersRepository.signOut();
    yield EmployerLoggedOut();
  }

  @override
  Future<void> close() {
    _employerProfileSubscription?.cancel();
    return super.close();
  }
}