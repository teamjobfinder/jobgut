import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:meta/meta.dart';

abstract class EmployerProfilePageState extends Equatable {}

// 1.
class EmployerProfilePageInitial extends EmployerProfilePageState {
  @override
  List<Object> get props => [];
}

// 2.
class EmployerProfilePageLoading extends EmployerProfilePageState {
  @override
  List<Object> get props => [];
}

// 3.
class EmployerProfilePageLoaded extends EmployerProfilePageState {
  final EmployerModel employerModel;

  EmployerProfilePageLoaded({@required this.employerModel});

  @override
  List<Object> get props => [employerModel];
}

// 4.
class EmployerProfilePageError extends EmployerProfilePageState {
  final String message;

  EmployerProfilePageError({@required this.message});

  @override
  List<Object> get props => [message];
}

class EmployerLoggedOut extends EmployerProfilePageState {
  @override
  List<Object> get props => [];
}
