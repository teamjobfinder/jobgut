import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:jobs/data/models/employer_model.dart';

abstract class EmployerProfilePageEvent extends Equatable {}

// 1.
class GetEmployerProfile extends EmployerProfilePageEvent {
  @override
  List<Object> get props => [];
}

// 2.
class EmployerProfileUpdated extends EmployerProfilePageEvent {
  final EmployerModel employerModel;

  EmployerProfileUpdated({@required this.employerModel});
  @override
  List<Object> get props => [employerModel];
}

class SignOutEmployer extends EmployerProfilePageEvent {
  @override
  List<Object> get props => [];
}
