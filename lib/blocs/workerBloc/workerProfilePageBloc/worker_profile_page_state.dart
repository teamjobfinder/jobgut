import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:meta/meta.dart';

abstract class WorkerProfilePageState extends Equatable {}

// 1.
class WorkerProfilePageInitial extends WorkerProfilePageState {
  @override
  List<Object> get props => [];
}

// 2.
class WorkerProfilePageLoading extends WorkerProfilePageState {
  @override
  List<Object> get props => [];
}

// 3.
class WorkerProfilePageLoaded extends WorkerProfilePageState {

  final WorkerModel workerModel;
  
  WorkerProfilePageLoaded({@required this.workerModel});

  @override
  List<Object> get props => [workerModel];
}

// 4.
class WorkerProfilePageError extends WorkerProfilePageState {

  final String message;

  WorkerProfilePageError({@required this.message});

  @override
  List<Object> get props => [message];
}

class WorkerSignedOut extends WorkerProfilePageState {
  @override
  List<Object> get props => [];
}