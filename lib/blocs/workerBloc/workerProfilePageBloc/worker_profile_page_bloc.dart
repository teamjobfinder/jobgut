import 'dart:async';
import 'package:jobs/blocs/workerBloc/workerProfilePageBloc/worker_profile_page_event.dart';
import 'package:jobs/blocs/workerBloc/workerProfilePageBloc/worker_profile_page_state.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:bloc/bloc.dart';

class WorkerProfilePageBloc
    extends Bloc<WorkerProfilePageEvent, WorkerProfilePageState> {
  WorkersRepository _workersRepository = WorkersRepository();
  StreamSubscription _workerProfileSubscription;

  @override
  WorkerProfilePageState get initialState => WorkerProfilePageInitial();

  @override
  Stream<WorkerProfilePageState> mapEventToState(
      WorkerProfilePageEvent event) async* {
    if (event is FetchWorkerProfile) {
      yield* _mapFetchWorkerProfileToState(event);
    } else if (event is WorkerProfileUpdated) {
      yield* _mapWorkerProfileUpdatedToState(event);
    } else if (event is LogOutWorker) {
      yield* _mapLogOutWorkerToState(event);
    }
  }

  Stream<WorkerProfilePageState> _mapFetchWorkerProfileToState(
      FetchWorkerProfile event) async* {
    yield WorkerProfilePageLoading();
    String userId = await _workersRepository.getUserId();
    try {
      _workerProfileSubscription =
          _workersRepository.getWorkerProfile(userId).listen((workerModel) {
        add(WorkerProfileUpdated(workerModel: workerModel));
      });
    } catch (e) {
      yield WorkerProfilePageError(message: e.toString());
    }
  }

  Stream<WorkerProfilePageState> _mapWorkerProfileUpdatedToState(
      WorkerProfileUpdated event) async* {
    yield WorkerProfilePageLoaded(workerModel: event.workerModel);
  }

  Stream<WorkerProfilePageState> _mapLogOutWorkerToState(LogOutWorker event) async* {
    await _workersRepository.signOut();
    yield WorkerSignedOut();
  }

  @override
  Future<void> close() {
    _workerProfileSubscription?.cancel();
    return super.close();
  }
}