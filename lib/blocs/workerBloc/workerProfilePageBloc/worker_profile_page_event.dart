import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:meta/meta.dart';

abstract class WorkerProfilePageEvent extends Equatable {}

class FetchWorkerProfile extends WorkerProfilePageEvent {
  @override
  List<Object> get props => [];
}

class WorkerProfileUpdated extends WorkerProfilePageEvent {
  final WorkerModel workerModel;

  WorkerProfileUpdated({@required this.workerModel});

  @override
  List<Object> get props => [workerModel];
}

class LogOutWorker extends WorkerProfilePageEvent {
  @override
  List<Object> get props => [];
}
