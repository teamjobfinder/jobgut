import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:meta/meta.dart';

abstract class SearchPageState extends Equatable {}

// 1.
class SearchInitial extends SearchPageState {
  @override
  List<Object> get props => null;
}

// 2.
class SearchLoading extends SearchPageState {
  @override
  List<Object> get props => null;
}

// 3.
class SearchLoaded extends SearchPageState {

  final List<JobPostModel> jobs;

  SearchLoaded({@required this.jobs});

  @override
  List<Object> get props => null;
}

// 4.
class NoJobsFound extends SearchPageState {
  @override
  List<Object> get props => null;
}

// 5.
class SearchFailed extends SearchPageState {

  final String message;

  SearchFailed({@required this.message});

  @override
  List<Object> get props => null;
}
