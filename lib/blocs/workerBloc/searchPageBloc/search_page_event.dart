import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:meta/meta.dart';

abstract class SearchPageEvent extends Equatable {}

// 1.
class SearchByNationalityLanguage extends SearchPageEvent {
  final String nationality;
  final String language;

  SearchByNationalityLanguage(
      {@required this.nationality, @required this.language});

  @override
  List<Object> get props => null;
}

// 2.
// as jobs will be fetched as stream, this is an extra event for that
// rather it looks like a state
class SearchUpdated extends SearchPageEvent {
  final List<JobPostModel> jobs;

  SearchUpdated({@required this.jobs});

  @override
  List<Object> get props => null;
}
