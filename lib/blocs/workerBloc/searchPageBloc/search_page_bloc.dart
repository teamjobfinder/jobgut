import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/workerBloc/searchPageBloc/search_page_event.dart';
import 'package:jobs/blocs/workerBloc/searchPageBloc/search_page_state.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/data/services/worker_service.dart';
import 'package:meta/meta.dart';

class SearchPageBloc extends Bloc<SearchPageEvent, SearchPageState> {
  JobsRepository jobsRepository;
  StreamSubscription _searchedJobsSubscription;
  WorkerService workerService;

  SearchPageBloc({@required this.jobsRepository}) {
    workerService = WorkerService();
  }

  @override
  SearchPageState get initialState => SearchInitial();

  @override
  Stream<SearchPageState> mapEventToState(SearchPageEvent event) async* {
    if (event is SearchByNationalityLanguage) {
      yield* _mapSearchByNationalityLanguageToStates(event);
    } else if (event is SearchUpdated) {
      yield* _mapSearchUpdatedEventToStates(event);
    }
  }

  Stream<SearchPageState> _mapSearchByNationalityLanguageToStates(
      SearchByNationalityLanguage event) async* {
    print("Added search event");
    yield SearchLoading();
    try {
      _searchedJobsSubscription?.cancel();
      print("trying to search jobs");
      _searchedJobsSubscription = workerService
          .searchJobsByNationalityAndLanguage(event.nationality, event.language)
          .listen((jobs) {
        print("trying to listen jobs : $jobs");
        add(SearchUpdated(jobs: jobs));
      }, onDone: () {
        print("listened done");
      }, onError: (e) {
        print("strim error");
      });
    } catch (e) {
      yield SearchFailed(message: e.toString());
    }
  }

  Stream<SearchPageState> _mapSearchUpdatedEventToStates(
      SearchUpdated event) async* {
    print("Added updated event");
    if (event.jobs.length == 0) {
      yield NoJobsFound();
    } else {
      yield SearchLoaded(jobs: event.jobs);
    }
  }

  @override
  Future<void> close() {
    _searchedJobsSubscription?.cancel();
    return super.close();
  }
}
