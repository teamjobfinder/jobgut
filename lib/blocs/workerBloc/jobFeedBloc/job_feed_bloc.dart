import 'dart:async';
import 'package:jobs/blocs/workerBloc/jobFeedBloc/job_feed_event.dart';
import 'package:jobs/blocs/workerBloc/jobFeedBloc/job_feed_state.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/data/repositories/location_repository.dart';
import 'package:jobs/utils/helper.dart';
import 'package:bloc/bloc.dart';

class JobFeedBloc extends Bloc<JobFeedEvent, JobFeedState> {
  final String _TAG = "JobFeedBloc";

  final LocationRepository _locationRepository = LocationRepository();
  final JobsRepository _jobsRepository = JobsRepository();
  StreamSubscription _gpsStatusSubscription,
      _jobsSubscription,
      _jobsInLocationsubscription,
      _jobsInKoreaSubscription;

  @override
  JobFeedState get initialState => LoadingRequest();

  @override
  Stream<JobFeedState> mapEventToState(JobFeedEvent event) async* {
    if (event is CheckGps) {
      yield* _mapCheckGpsToState();
    } else if (event is FetchAllJobPosts) {
      Helper.logPrint(_TAG, "FetchAllJobPosts called");
      yield* _mapFetchAllJobPostsToState(event);
    } else if (event is JobPostsRefreshed) {
      Helper.logPrint(_TAG, "JobPostsRefreshed called");
      yield* _mapJobPostsRefreshed(event);
    } else if (event is GetCurrentLocation) { // [omitted]
      Helper.logPrint(_TAG, "GetCurrentLocation called");
      yield* _mapGetCurrentLocation();
    } else if (event is FetchJobPostsForLocation) { // [omitted]
      Helper.logPrint(_TAG, "FetchJobPostsForLocation called");
      yield* _mapFetchJobPostsForLocation(event);
    } else if (event is FetchJobPostsInSouthKorea) {
      Helper.logPrint(_TAG, "FetchJobPostsInSouthKorea called");
      yield* _mapFetchJobPostsInSouthKoreaToState(event);
    }
  }

  // 1.
  Stream<JobFeedState> _mapCheckGpsToState() async* {
    yield LoadingRequest();
    _gpsStatusSubscription?.cancel();
    _gpsStatusSubscription = _locationRepository.checkGps().listen((value) {
      Helper.logPrint(_TAG, value.toString());
      if (value) {
        print("Adding GetCurrentLocation");
        add(FetchJobPostsInSouthKorea());
      } else {
        print("Adding FetchAllJobPosts");
        add(FetchAllJobPosts());
      }
    });
  }

  // 2.
  Stream<JobFeedState> _mapFetchAllJobPostsToState(
      FetchAllJobPosts event) async* {
    yield LoadingJobPosts();
    try {
      // _jobsSubscription?.cancel();
      // _jobsInKoreaSubscription?.cancel();
      _jobsSubscription = _jobsRepository.getAllTheJobsPosted().listen((jobs) {
        add(JobPostsRefreshed(jobs: jobs));
      });
    } catch (e) {
      yield Error(message: e.toString());
    }
  }

  Stream<JobFeedState> _mapFetchJobPostsInSouthKoreaToState(
      FetchJobPostsInSouthKorea event) async* {
    yield LoadingJobPosts();
    try {
      // _jobsSubscription?.cancel();
      // _jobsInKoreaSubscription?.cancel();
      _jobsInKoreaSubscription = _jobsRepository.getJobsInSouthkorea().listen((jobs) {
        add(JobPostsRefreshed(jobs: jobs));
      });
    } catch (e) {
      yield Error(message: e.toString());
    }
  }

  // 3. [omitted]
  Stream<JobFeedState> _mapFetchJobPostsForLocation(
      FetchJobPostsForLocation event) async* {
    yield LoadingJobPosts();
    try {
      _jobsSubscription?.cancel();
      _jobsInLocationsubscription?.cancel();
      _jobsInLocationsubscription = _jobsRepository
          .getJobsInALocation(event.location.country)
          .listen((jobs) {
        Helper.logPrint(_TAG, "The jobs : ${jobs.toString()}");
        add(JobPostsRefreshed(jobs: jobs));
      });
    } catch (e) {
      yield Error(message: e.toString());
    }
  }

  // 4.
  Stream<JobFeedState> _mapJobPostsRefreshed(JobPostsRefreshed event) async* {
    yield JobPostsLoaded(jobPosts: event.jobs);
  }

  // 5. [omitted]
  Stream<JobFeedState> _mapGetCurrentLocation() async* {
    yield LoadingJobPosts();
    try {
      TheLocation theLocation = await _locationRepository.getCurrentLocation();
      Helper.logPrint(_TAG, "${theLocation.country}");
      add(FetchJobPostsForLocation(location: theLocation));
    } catch (e) {
      Helper.logPrint(_TAG, "Error getting cLocation : ${e.toString()}");
      add(FetchAllJobPosts());
    }
  }

  @override
  Future<void> close() {
    _gpsStatusSubscription?.cancel();
    _jobsSubscription?.cancel();
    _jobsInLocationsubscription?.cancel();
    _jobsInKoreaSubscription?.cancel();
    return super.close();
  }
}
