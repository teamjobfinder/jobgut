import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:meta/meta.dart';

abstract class JobFeedState extends Equatable {}

// 1.
/// Loading state of requests like get location, gps status etc.
class LoadingRequest extends JobFeedState {
  @override
  List<Object> get props => [];
}

// 2. [omitted]
/// state for current location [TheLocation]
class CurrentLocationLoaded extends JobFeedState {
  final TheLocation theLocation;
  CurrentLocationLoaded({@required this.theLocation});
  @override
  List<Object> get props => [theLocation];
}

// 3.
class LoadingJobPosts extends JobFeedState {
  @override
  List<Object> get props => [];
}

// 4.
/// state for successful fetching of job posts [JobPostModel]
class JobPostsLoaded extends JobFeedState {
  final List<JobPostDocumentModel> jobPosts;

  JobPostsLoaded({@required this.jobPosts});
  @override
  List<Object> get props => [jobPosts];
}

// 5.
/// shows error message [String]
class Error extends JobFeedState {
  final String message;

  Error({@required this.message});
  @override
  List<Object> get props => [message];
}
