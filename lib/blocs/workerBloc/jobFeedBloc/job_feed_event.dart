import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:meta/meta.dart';

abstract class JobFeedEvent extends Equatable {}

// 1.
class CheckGps extends JobFeedEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}

// 2.
/// Fetch all job posts over the world
class FetchAllJobPosts extends JobFeedEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}

// 3. [omitted]
class GetCurrentLocation extends JobFeedEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}

//4. [omitted]
class FetchJobPostsForLocation extends JobFeedEvent {
  final TheLocation location;
  FetchJobPostsForLocation({@required this.location});
  @override
  List<Object> get props => [location];
}

// 5. additional for a special case
class FetchJobPostsInSouthKorea extends JobFeedEvent {
  @override
  List<Object> get props => [];
}

// 6. when job posts loaded new job post added
class JobPostsRefreshed extends JobFeedEvent {
  final List<JobPostDocumentModel> jobs;

  JobPostsRefreshed({@required this.jobs});
  @override
  List<Object> get props => [jobs];
}
