import 'dart:async';
import 'package:jobs/blocs/workerBloc/appliesBloc/applies_event.dart';
import 'package:jobs/blocs/workerBloc/appliesBloc/applies_state.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/utils/helper.dart';
import 'package:bloc/bloc.dart';

class AppliesBloc extends Bloc<AppliesEvent, AppliesState> {
  final String _TAG = "AppliesBloc";
  WorkersRepository _workersRepository = WorkersRepository();
  JobsRepository _jobsRepository = JobsRepository();

  StreamSubscription _appliedJobsSubscription;

  @override
  AppliesState get initialState => AppliesInitial();

  @override
  Stream<AppliesState> mapEventToState(AppliesEvent event) async* {
    Helper.logPrint(_TAG, "Event added");
    if (event is FetchAppliedJobPosts) {
      yield* _mapFetchAppliedJobPostsToState(event);
    } else if (event is AppliedJobsRefreshed) {
      yield* _mapAppliedJobsRefreshedToState(event);
    }
  }

  Stream<AppliesState> _mapFetchAppliedJobPostsToState(
      AppliesEvent event) async* {
    Helper.logPrint(_TAG, event.toString());
    yield AppliesLoading();
    bool isGuest = await _workersRepository.isGuest();
    if (isGuest == false) {
      Helper.logPrint(_TAG, "guest false");
      String userId = await _workersRepository.getUserId();
      Helper.logPrint(_TAG, "id : $userId");
      try {
        Helper.logPrint(_TAG, "listening...");
        _appliedJobsSubscription?.cancel();
        _appliedJobsSubscription =
            _jobsRepository.getAppliedJobs(userId).listen((jobs) {
              Helper.logPrint(_TAG, "listened");
          add(AppliedJobsRefreshed(jobs: jobs));
        });
      } catch (e) {
        Helper.logPrint(_TAG, "Something Wrong");
        yield AppliesLoadFailure(message: e.toString());
      }
    } else {
      yield GuestState();
    }
  }

  Stream<AppliesState> _mapAppliedJobsRefreshedToState(
      AppliedJobsRefreshed event) async* {
    Helper.logPrint(_TAG, event.toString());
    if (event.jobs.length == 0) {
      Helper.logPrint(_TAG, "Not Applied Yet");
      yield NotAppliedYet();
    } else {
      Helper.logPrint(_TAG, "Has Applied");
      yield AppliesLoaded(jobs: event.jobs);
    }
  }

  @override
  Future<void> close() {
    _appliedJobsSubscription?.cancel();
    return super.close();
  }
}
