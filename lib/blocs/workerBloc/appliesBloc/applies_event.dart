import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:jobs/data/models/job_post_document_model.dart';

abstract class AppliesEvent extends Equatable {}

class FetchAppliedJobPosts extends AppliesEvent {
  @override
  List<Object> get props => [];
}

class AppliedJobsRefreshed extends AppliesEvent {
  final List<JobPostDocumentModel> jobs;
  AppliedJobsRefreshed({@required this.jobs});
  
  @override
  List<Object> get props => [jobs];
}
