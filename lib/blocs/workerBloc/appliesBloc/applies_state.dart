import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:jobs/data/models/job_post_document_model.dart';

abstract class AppliesState extends Equatable {}

// 1.
class AppliesInitial extends AppliesState {
  @override
  List<Object> get props => [];
}

// 2.
class AppliesLoading extends AppliesState {
  @override
  List<Object> get props => [];
}

// 3.
class AppliesLoaded extends AppliesState {
  final List<JobPostDocumentModel> jobs;

  AppliesLoaded({@required this.jobs});
  @override
  List<Object> get props => [jobs];
}

// 4.
class AppliesLoadFailure extends AppliesState {
  final String message;
  AppliesLoadFailure({@required this.message});
  @override
  List<Object> get props => [message];
}

// 5.
class NotAppliedYet extends AppliesState {
  @override
  List<Object> get props => [];
}

// 6.
class GuestState extends AppliesState {
  @override
  List<Object> get props => [];
}