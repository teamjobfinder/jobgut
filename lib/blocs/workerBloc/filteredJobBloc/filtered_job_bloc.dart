import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/workerBloc/filteredJobBloc/filtered_job_event.dart';
import 'package:jobs/blocs/workerBloc/filteredJobBloc/filtered_job_state.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/utils/helper.dart';

class FilteredJobBloc extends Bloc<FilteredJobEvent, FilteredJobState> {
  final String _tag = "FilteredJobBloc";
  StreamSubscription _filteredJobsSubs;

  JobsRepository _jobsRepository = JobsRepository();

  @override
  FilteredJobState get initialState => FilteredJobsInitial();

  @override
  Stream<FilteredJobState> mapEventToState(FilteredJobEvent event) async* {
    if (event is FetchJobsByCategoryAndType) {
      yield* _mapFetchJobsByCategoryAndTypeToState(event);
    } else if (event is CategoryAndTypeFilterUpdated) {
      yield* _mapCategoryAndTypeFilterUpdatedToState(event);
    }
  }

  Stream<FilteredJobState> _mapFetchJobsByCategoryAndTypeToState(
      FetchJobsByCategoryAndType event) async* {
    Helper.logPrint(_tag, "Added ${event.toString()}");
    yield FilteredJobsLoading();
    try {
      Helper.logPrint(_tag, "Trying");
      _filteredJobsSubs?.cancel();
      _filteredJobsSubs = _jobsRepository
          .getFilteredJobsByCategoryAndType(event.category, event.type)
          .listen((jobs) {
            Helper.logPrint(_tag, "Adding Event");
        add(
          CategoryAndTypeFilterUpdated(
            filteredJobs: jobs,
            type: event.type,
            category: event.category,
          ),
        );
      });
    } catch (e) {
      yield FilteredJobsLoadFailure(message: e.toString());
    }
  }

  Stream<FilteredJobState> _mapCategoryAndTypeFilterUpdatedToState(
      CategoryAndTypeFilterUpdated event) async* {
    Helper.logPrint(_tag, "Added ${event.toString()}");
    if (event.filteredJobs.length == 0) {
      Helper.logPrint(_tag, "No Jobs Found");
      yield NoFilteredJobsFound(
        category: event.category,
        type: event.type,
      );
    } else {
      Helper.logPrint(_tag, "Jobs Loaded");
      yield FilteredJobsLoaded(filteredJobs: event.filteredJobs);
    }
  }

  @override
  Future<void> close() {
    _filteredJobsSubs?.cancel();
    return super.close();
  }

  @override
  void onEvent(FilteredJobEvent event) {
    // TODO: implement onEvent
    super.onEvent(event);
  }

}
