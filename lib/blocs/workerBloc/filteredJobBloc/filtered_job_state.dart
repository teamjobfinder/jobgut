import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:meta/meta.dart';

abstract class FilteredJobState extends Equatable {}

class FilteredJobsInitial extends FilteredJobState {
  @override
  List<Object> get props => [];
}

class FilteredJobsLoading extends FilteredJobState {
  @override
  List<Object> get props => [];
}

class FilteredJobsLoaded extends FilteredJobState {
  final List<JobPostDocumentModel> filteredJobs;

  FilteredJobsLoaded({@required this.filteredJobs});

  @override
  List<Object> get props => [filteredJobs];
}

class NoFilteredJobsFound extends FilteredJobState {

  final String type, category;

  NoFilteredJobsFound({@required this.type, @required this.category});

  @override
  List<Object> get props => [type, category];
}

class FilteredJobsLoadFailure extends FilteredJobState {
  final String message;

  FilteredJobsLoadFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
