import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:meta/meta.dart';

abstract class FilteredJobEvent extends Equatable {}

class FetchJobsByCategoryAndType extends FilteredJobEvent {
  final String category;
  final String type;

  FetchJobsByCategoryAndType({@required this.category, @required this.type});

  @override
  List<Object> get props => [category, type];
}

class CategoryAndTypeFilterUpdated extends FilteredJobEvent {
  final List<JobPostDocumentModel> filteredJobs;
  final String category;
  final String type;

  CategoryAndTypeFilterUpdated({
    @required this.filteredJobs,
    @required this.category,
    @required this.type,
  });

  @override
  List<Object> get props => [filteredJobs, category, type];
}
