import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {}

// 1.
class LoginInitial extends LoginState {
  @override
  List<Object> get props => null;
}

// 2.
class LoginLoading extends LoginState {
  @override
  List<Object> get props => null;
}

// 3.
class EmployerLoginSuccess extends LoginState {
  @override
  List<Object> get props => null;
}

// 4.
class WorkerLoginSuccess extends LoginState {
  @override
  List<Object> get props => null;
}

// 5.
class LoginFailure extends LoginState {
  String message;

  LoginFailure({@required this.message});

  @override
  List<Object> get props => null;
}
