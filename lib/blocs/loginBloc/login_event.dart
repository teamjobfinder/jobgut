import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginEvent extends Equatable {}

// 1.
class LoginButtonPressed extends LoginEvent {
  String email, password;

  LoginButtonPressed({
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => null;
}

// 2.
class SignInWorkerAsGuest extends LoginEvent {
  @override
  List<Object> get props => null;
}

// 3.
// isWorker  is to check from which context this btn is pressed ; worker or employer; needed for new sign in
class GoogleSignInPressed extends LoginEvent {
  bool isWorker;

  GoogleSignInPressed({@required this.isWorker});

  @override
  List<Object> get props => null;
}

// 4.
// isWorker  is to check from which context this btn is pressed ; worker or employer; needed for new sign in
class FacebookSignInPressed extends LoginEvent {

  bool isWorker;

  FacebookSignInPressed({@required this.isWorker});

  @override
  List<Object> get props => null;
}
