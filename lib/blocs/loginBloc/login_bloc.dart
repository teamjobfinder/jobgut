import 'package:firebase_auth/firebase_auth.dart';
import 'package:jobs/blocs/loginBloc/login_event.dart';
import 'package:jobs/blocs/loginBloc/login_state.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  static const String tag = "LOGIN_BLOC";

  AuthService authService;
  UsersService usersService;
  WorkersRepository workersRepository;
  EmployersRepository employersRepository;

  LoginBloc({
    @required this.authService,
    @required this.usersService,
    @required this.workersRepository,
    @required this.employersRepository,
  });

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield* mapLoginEventToState(event);
    } else if (event is SignInWorkerAsGuest) {
      yield* mapGuestSignInEventToState(event);
    } else if (event is GoogleSignInPressed) {
      yield* mapGoogleSignInEventToState(event);
    } else if (event is FacebookSignInPressed) {
      yield* _mapFacebookSignInEventToState(event);
    }
  }

  // map login event to states
  Stream<LoginState> mapLoginEventToState(LoginButtonPressed event) async* {
    print("$tag Mapping email login");
    yield LoginLoading();
    try {
      print("$tag trying email login");
      await authService.signInWithEmailAndPassword(
        email: event.email,
        password: event.password,
      );
      String userType = await usersService.employerOrWorker();
      if (userType == AppStringConstants.workerType) {
        yield WorkerLoginSuccess();
      } else if (userType == AppStringConstants.employerType) {
        yield EmployerLoginSuccess();
      }
    } catch (e) {
      print("$tag err email login ${e.toString()}");
      yield LoginFailure(message: e.toString());
    }
  }

  // map guest sign in to states
  Stream<LoginState> mapGuestSignInEventToState(
      SignInWorkerAsGuest event) async* {
    yield LoginLoading();
    try {
      await authService.signInAsGuest();
      yield WorkerLoginSuccess();
    } catch (e) {
      yield LoginFailure(message: e.toString());
    }
  }

  // map google sign in btn press to state
  // does extra task as here is no repository
  Stream<LoginState> mapGoogleSignInEventToState(
      GoogleSignInPressed event) async* {
    yield LoginLoading();
    try {
      // 1.
      AuthResult authResult = await authService.signInWithGoogle();
      // 2.
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        // 3.
        bool isWorker = event.isWorker;
        // 4a
        if (isWorker) {
          WorkerModel workerModel = WorkerModel(
            email: authResult.user.email,
            name: authResult.user.displayName,
          );
          await workersRepository.addWorker(workerModel);
          await workersRepository.addInUsers();
          yield WorkerLoginSuccess();
        } else {
          //4b
          EmployerModel employerModel = EmployerModel(
            email: authResult.user.email,
            name: authResult.user.displayName,
            isVerified: false,
          );
          await employersRepository.addEmployer(employerModel);
          await employersRepository.addInUsers();
          yield EmployerLoginSuccess();
        }
      } else {
        // 5
        String userType = await usersService.employerOrWorker();
        // 6a
        if (userType == AppStringConstants.workerType) {
          yield WorkerLoginSuccess();
        } else if (userType == AppStringConstants.employerType) {
          // 6b
          yield EmployerLoginSuccess();
        }
      }
    } catch (e) {
      yield LoginFailure(message: e.toString());
    }
  }

  Stream<LoginState> _mapFacebookSignInEventToState(
      FacebookSignInPressed event) async* {
    yield LoginLoading();
    try {
      // 1.
      AuthResult authResult = await authService.signInWithFacebook();
      // 2.
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        // 3.
        bool isWorker = event.isWorker;
        // 4a
        if (isWorker) {
          WorkerModel workerModel = WorkerModel(
            email: authResult.user.email,
            name: authResult.user.displayName,
          );
          await workersRepository.addWorker(workerModel);
          await workersRepository.addInUsers();
          yield WorkerLoginSuccess();
        } else {
          //4b
          EmployerModel employerModel = EmployerModel(
            email: authResult.user.email,
            name: authResult.user.displayName,
            isVerified: false,
          );
          await employersRepository.addEmployer(employerModel);
          await employersRepository.addInUsers();
          yield EmployerLoginSuccess();
        }
      } else {
        // 5
        String userType = await usersService.employerOrWorker();
        // 6a
        if (userType == AppStringConstants.workerType) {
          yield WorkerLoginSuccess();
        } else if (userType == AppStringConstants.employerType) {
          // 6b
          yield EmployerLoginSuccess();
        }
      }
    } catch (e) {
      yield LoginFailure(message: e.toString());
    }
  }

}
