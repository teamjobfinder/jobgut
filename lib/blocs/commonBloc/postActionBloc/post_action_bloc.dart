import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:jobs/blocs/commonBloc/postActionBloc/post_action_event.dart';
import 'package:jobs/blocs/commonBloc/postActionBloc/post_action_state.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/utils/helper.dart';

class PostActionBloc extends Bloc<PostActionEvent, PostActionState> {
  final String _TAG = "_PostDetailsPageState";
  WorkersRepository _workersRepository = WorkersRepository();
  JobsRepository _jobsRepository = JobsRepository();
  StreamSubscription _applicationsSubscription;

  @override
  PostActionState get initialState => PostActionInitial();

  @override
  Stream<PostActionState> mapEventToState(PostActionEvent event) async* {
    if (event is CheckIfGuest) {
      yield* _mapCheckIfGuestToState(event);
    } else if (event is CheckIfApplied) {
      yield* _mapCheckIfAppliedToState(event);
    } else if (event is ApplyOnJob) {
      yield* _mapApplyOnJobToState(event);
    } else if (event is FetchAllApplications) {
      yield* _mapFetchAllApplicationsToState(event);
    } else if (event is UpdatedApplications) {
      yield* _mapUpdatedApplicationsToState(event);
    }
  }

  // 1.
  Stream<PostActionState> _mapCheckIfGuestToState(CheckIfGuest event) async* {
    bool isGuest = await _workersRepository.isGuest();
    if (isGuest) {
      yield GuestState();
    } else {
      add(CheckIfApplied(documentReference: event.documentReference));
    }
  }

  // 2.
  Stream<PostActionState> _mapCheckIfAppliedToState(
      CheckIfApplied event) async* {
    try {
      Helper.logPrint(_TAG, "DocRef : ${event.documentReference}");
      bool hasApplied =
          await _jobsRepository.hasAppliedOnAJob(event.documentReference);
      Helper.logPrint(_TAG, "Applied : $hasApplied");
      if (hasApplied) {
        yield AlreadyApplied();
      } else {
        yield NotApplied(documentReference: event.documentReference);
      }
    } catch (e) {
      yield ApplicationCheckFailed(message: e.toString());
    }
  }

  // 3.
  Stream<PostActionState> _mapApplyOnJobToState(ApplyOnJob event) async* {
    yield Applying();
    try {
      await _jobsRepository.applyOnASingleJob(event.documentReference);
      yield SuccessfullyApplied();
    } catch (e) {
      yield FailedToApply(message: e.toString());
    }
  }

  // 4. number of applications on a post
  Stream<PostActionState> _mapFetchAllApplicationsToState(
      FetchAllApplications event) async* {
    yield ApplicationsLoading();
    try {
      _applicationsSubscription?.cancel();
      _applicationsSubscription = _jobsRepository
          .getNumberOfApplicationsInAJob(event.documentReference)
          .listen((noOfApplications) {
        add(
          UpdatedApplications(
            applications: noOfApplications,
            documentReference: event.documentReference,
          ),
        );
      });
    } catch (e) {
      yield ApplicationsLoadFailure(message: e.toString());
    }
  }

  // 5.
  Stream<PostActionState> _mapUpdatedApplicationsToState(
      UpdatedApplications event) async* {
    if (event.applications == 0) {
      yield NoApplication();
    } else {
      yield ApplicationsLoaded(
        totalApplications: event.applications,
        documentReference: event.documentReference,
      );
    }
  }

  @override
  Future<void> close() {
    _applicationsSubscription?.cancel();
    return super.close();
  }
}
