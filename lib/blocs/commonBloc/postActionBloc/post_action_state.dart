import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PostActionState extends Equatable {}

// 0.
class PostActionInitial extends PostActionState {
  @override
  List<Object> get props => [];
}

// 1. for worker
class GuestState extends PostActionState {
  @override
  List<Object> get props => [];
}

// 2. for worker
class AlreadyApplied extends PostActionState {
  @override
  List<Object> get props => [];
}

// 3. for worker
class NotApplied extends PostActionState {
  final DocumentReference documentReference;

  NotApplied({@required this.documentReference});
  @override
  List<Object> get props => [documentReference];
}

// 4.
class ApplicationCheckFailed extends PostActionState {
  final String message;

  ApplicationCheckFailed({@required this.message});
  @override
  List<Object> get props => [message];
}

// 5. for worker
class Applying extends PostActionState {
  @override
  List<Object> get props => [];
}

// 6. for worker
class SuccessfullyApplied extends PostActionState {
  @override
  List<Object> get props => [];
}

// 7. for worker
class FailedToApply extends PostActionState {
  final String message;

  FailedToApply({@required this.message});
  @override
  List<Object> get props => [message];
}

// 8. for employer
class ApplicationsLoading extends PostActionState {
  @override
  List<Object> get props => [];
}

// 9. for employer
class ApplicationsLoaded extends PostActionState {
  final int totalApplications;
  final DocumentReference documentReference;

  ApplicationsLoaded({
    @required this.totalApplications,
    @required this.documentReference,
  });

  @override
  List<Object> get props => [totalApplications, documentReference];
}

// 10. for employer
class NoApplication extends PostActionState {
  @override
  List<Object> get props => [];
}

// 11. for employer
class ApplicationsLoadFailure extends PostActionState {
  final String message;

  ApplicationsLoadFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
