import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PostActionEvent extends Equatable {}

// 1. for worker
class CheckIfApplied extends PostActionEvent {
  final DocumentReference documentReference;

  CheckIfApplied({@required this.documentReference});

  @override
  List<Object> get props => [documentReference];
}

// 2. for worker
class ApplyOnJob extends PostActionEvent {
  final DocumentReference documentReference;

  ApplyOnJob({@required this.documentReference});

  @override
  List<Object> get props => [documentReference];
}

// 3. for worker
class CheckIfGuest extends PostActionEvent {
  final DocumentReference documentReference;

  CheckIfGuest({@required this.documentReference});
  @override
  List<Object> get props => [documentReference];
}

// 4. for employer
class FetchAllApplications extends PostActionEvent {
  final DocumentReference documentReference;

  FetchAllApplications({@required this.documentReference});

  @override
  List<Object> get props => [documentReference];
}

// 5. for employer
class UpdatedApplications extends PostActionEvent {
  final int applications;
  final DocumentReference documentReference;

  UpdatedApplications({@required this.applications, @required this.documentReference});

  @override
  List<Object> get props => [applications, documentReference];
}