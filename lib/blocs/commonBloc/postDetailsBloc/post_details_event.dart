import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:meta/meta.dart';

abstract class PostDetailsEvent extends Equatable {}

// 1.
// done
class FetchPostDetails extends PostDetailsEvent {
  final DocumentSnapshot documentSnapshot;

  FetchPostDetails({@required this.documentSnapshot});

  @override
  List<Object> get props => [documentSnapshot];
}

// 2.
class TranslateJobPost extends PostDetailsEvent {
  final String languageCode;
  final JobPostModel jobPostModel;

  TranslateJobPost({@required this.languageCode, @required this.jobPostModel});

  @override
  List<Object> get props => [languageCode, jobPostModel];
}
