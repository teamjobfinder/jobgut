import 'dart:async';
import 'package:jobs/blocs/commonBloc/postDetailsBloc/post_details_event.dart';
import 'package:jobs/blocs/commonBloc/postDetailsBloc/post_details_state.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:bloc/bloc.dart';

class PostDetailsBloc extends Bloc<PostDetailsEvent, PostDetailsState> {
  JobsRepository _jobsRepository = JobsRepository();
  EmployersRepository _employersRepository = EmployersRepository();

  @override
  PostDetailsState get initialState => PostLoading();

  @override
  Stream<PostDetailsState> mapEventToState(PostDetailsEvent event) async* {
    if (event is FetchPostDetails) {
      yield* _mapfetchPostDetailsToState(event);
    } else if (event is TranslateJobPost) {
      yield* _mapTranslateJobPostToState(event);
    }
  }

  // 1.
  Stream<PostDetailsState> _mapfetchPostDetailsToState(
      FetchPostDetails event) async* {
    yield PostLoading();
    try {
      JobPostModel jobPostModel =
          _jobsRepository.getJobPostFromSnapShot(event.documentSnapshot);
      EmployerModel employerModel =
          await _employersRepository.getEmployerOfAPost(event.documentSnapshot);
      yield PostLoaded(
        jobPostModel: jobPostModel,
        employerModel: employerModel,
      );
    } catch (e) {
      yield PostLoadFailure(message: e.toString());
    }
  }

  // 2.
  Stream<PostDetailsState> _mapTranslateJobPostToState(
      TranslateJobPost event) async* {
    yield Translating();
    try {
      JobPostModel jobPostModel = await _jobsRepository.translateJobPost(
          event.languageCode, event.jobPostModel);
      yield TranslationSuccessful(jobPostModel: jobPostModel);
    } catch (e) {
      yield TranslationFailure(message: e.toString());
    }
  }

  
}
