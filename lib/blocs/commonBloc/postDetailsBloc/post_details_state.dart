import 'package:equatable/equatable.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:meta/meta.dart';

abstract class PostDetailsState extends Equatable {}

// 1.
// done
class PostLoading extends PostDetailsState {
  @override
  List<Object> get props => [];
}

// 2.
// done
class PostLoaded extends PostDetailsState {
  final JobPostModel jobPostModel;
  // to get updated company name
  final EmployerModel employerModel;

  PostLoaded({@required this.jobPostModel, @required this.employerModel});

  @override
  List<Object> get props => [jobPostModel, employerModel];
}

// 3.
// done
class PostLoadFailure extends PostDetailsState {
  final String message;

  PostLoadFailure({@required this.message});

  @override
  List<Object> get props => [message];
}


// 4.
class Translating extends PostDetailsState {
  @override
  List<Object> get props => [];
}

// 5.
class TranslationSuccessful extends PostDetailsState {
  final JobPostModel jobPostModel;

  TranslationSuccessful({@required this.jobPostModel});
  @override
  List<Object> get props => [jobPostModel];
}

// 6.
class TranslationFailure extends PostDetailsState {
  final String message;

  TranslationFailure({@required this.message});
  @override
  List<Object> get props => [message];
}
