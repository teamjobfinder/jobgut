import 'package:equatable/equatable.dart';

abstract class Authevent extends Equatable {}

class AppStartedEvent extends Authevent {
  @override
  List<Object> get props => null;
}
