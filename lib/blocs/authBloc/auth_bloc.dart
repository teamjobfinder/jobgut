import 'package:jobs/blocs/authBloc/auth_event.dart';
import 'package:jobs/blocs/authBloc/auth_state.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';


 /// checks auth state and user type
 /// Has dependency on [AuthService] and [UsersService]
 
class AuthBloc extends Bloc<Authevent, AuthState> {
  AuthService authService;
  UsersService usersService;

  AuthBloc({@required this.authService, @required this.usersService});

  @override
  AuthState get initialState => AuthInitial();

  @override
  Stream<AuthState> mapEventToState(Authevent event) async* {
    if (event is AppStartedEvent) {
      yield* mapAuthStartedEventToState(event);
    }
  }

  // map AppStartedEvent to states
  Stream<AuthState> mapAuthStartedEventToState(AppStartedEvent event) async* {
    yield AuthLoading();
    try {
      bool hasSignedInUser = await authService.isSignedIn();
      if (hasSignedInUser) {
        String userType = await usersService.employerOrWorker();
        if (userType == AppStringConstants.workerType) {
          yield WorkerLoggedIn();
        } else if (userType == AppStringConstants.employerType) {
          yield EmployerLoggedIn();
        }
      } else {
        yield UnAutheticatedState();
      }
    } catch (e) {
      yield AuthFailure(message: e.toString());
    }
  }
}
