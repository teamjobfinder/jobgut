import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthState extends Equatable {}

// 1.
class AuthInitial extends AuthState {
  @override
  List<Object> get props => null;
}

// 2.
class AuthLoading extends AuthState {
  @override
  List<Object> get props => null;
}

// 3.
class UnAutheticatedState extends AuthState {
  @override
  List<Object> get props => null;
}

// 4.
class EmployerLoggedIn extends AuthState {
  @override
  List<Object> get props => null;
}

// 5.
class WorkerLoggedIn extends AuthState {
  @override
  List<Object> get props => null;
}

// 6.
class AuthFailure extends AuthState {

  String message;

  AuthFailure({@required this.message});

  @override
  List<Object> get props => null;
}

