import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/authBloc/auth_bloc.dart';
import 'package:jobs/blocs/authBloc/auth_event.dart';
import 'package:jobs/blocs/authBloc/auth_state.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/dynamic_url_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/notifiers/theme_notifier.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/common/splash_screen.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/ui/pages/worker/worker_home.dart';
import 'package:jobs/ui/theme/app_theme.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => ThemeNotifier(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthService authService = AuthService();
  UsersService usersService = UsersService();
  DynamicUrlService _dynamicUrlService = DynamicUrlService();

  @override
  void initState() {
    super.initState();
    _dynamicUrlService.handleDynamicLinks();
  }

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return Consumer<ThemeNotifier>(
      builder: (context, themeNotifier, child) {
        return MaterialApp(
          navigatorKey: Get.key,
          debugShowCheckedModeBanner: false,
          title: AppStringConstants.appTitle,
          theme: themeNotifier.getTheme ? darkTheme : lightTheme,
          home: BlocProvider(
            create: (context) => AuthBloc(
              authService: authService,
              usersService: usersService,
            )..add(AppStartedEvent()),
            child: const Appp(),
          ),
        );
      },
    );
  }
}

class Appp extends StatelessWidget {
  const Appp();

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthFailure) {}
      },
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is AuthInitial) {
            debugPrint("AUTH INITIAL");
            return const SplashScreen();
          } else if (state is AuthLoading) {
            debugPrint("AUTH LOADING");
            return const SplashScreen();
          } else if (state is UnAutheticatedState) {
            debugPrint("AUTH UNAUTHENTICATED");
            return const EmployeeEmployerPage();
          } else if (state is EmployerLoggedIn) {
            debugPrint("EMPLOYER");
            return const EmployerHomePage();
          } else if (state is WorkerLoggedIn) {
            debugPrint("Worker");
            return const WorkerHome();
          } else if (state is AuthFailure) {
            debugPrint("auth failure");
            return const EmployeeEmployerPage();
          }
        },
      ),
    );
  }
}
