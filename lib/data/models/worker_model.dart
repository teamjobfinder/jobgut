import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class WorkerModel {
  String name, location, email, profilPicUrl, about, profession;
  int phoneNumber;
  String countryCode;
  // make it List<String>
  String skills, gender;

  WorkerModel({
    @required this.name,
    this.location,
    @required this.email,
    this.profilPicUrl,
    this.phoneNumber,
    this.skills,
    this.about,
    this.profession,
    this.countryCode,
    this.gender,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'location': location,
      'email': email,
      'profilPicUrl': profilPicUrl,
      'phoneNumber': phoneNumber,
      'skills': skills,
      'about': about,
      'profession': profession,
      'countryCode': countryCode,
      'gender': gender,
    };
  }

  static WorkerModel fromMap(Map<String, dynamic> json) {
    return WorkerModel(
      name: json['name'],
      profilPicUrl: json['profilPicUrl'],
      location: json['location'],
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      about: json['about'],
      skills: json['skills'],
      profession: json['profession'],
      countryCode: json['countryCode'],
      gender: json['gender'],
    );
  }

  static WorkerModel fromSnapshot(DocumentSnapshot snap) {
    return WorkerModel(
      name: snap.data['name'],
      profilPicUrl: snap.data['profilPicUrl'],
      location: snap.data['location'],
      email: snap.data['email'],
      phoneNumber: snap.data['phoneNumber'],
      about: snap.data['about'],
      skills: snap.data['skills'],
      profession: snap.data['profession'],
      countryCode: snap.data['countryCode'],
      gender: snap.data['gender'],
    );
  }
}
