import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:meta/meta.dart';

class JobPostDocumentModel {
  final DocumentReference documentReference;
  final JobPostModel jobPostModel;
  final DocumentSnapshot documentSnapshot;

  const JobPostDocumentModel({
    @required this.documentReference,
    @required this.jobPostModel,
    @required this.documentSnapshot,
  });
}
