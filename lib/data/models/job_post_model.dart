import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

class JobPostModel {
  static const String _TAG = "JobPostModel";
  int vacancies;
  bool isValid;
  String date, deadline;
  String jobPosition,
      title,
      companyLogoUrl,
      jobCategory,
      jobDescription,
      experience,
      jobType,
      schedule,
      address,
      salaryRange,
      currency,
      salaryType,
      companyName,
      jobImageUrl,
      language,
      preferredGender,
      nationality; // employer's nationality

  // location; //
  String country,
      administrativeArea,
      subAdministrativeArea,
      name,
      locality,
      subLocality,
      thoroughfare;
  double latitude, longitude;

  JobPostModel({
    this.date,
    this.deadline,
    this.jobPosition,
    @required this.title,
    this.companyLogoUrl,
    this.jobCategory,
    @required this.jobDescription,
    this.experience,
    this.jobType,
    this.schedule,
    this.address,
    this.salaryRange,
    this.currency,
    this.salaryType,
    this.vacancies,
    this.isValid,
    this.companyName,
    // location
    this.country,
    this.administrativeArea,
    this.subAdministrativeArea,
    this.name,
    this.locality,
    this.subLocality,
    this.thoroughfare,
    this.latitude,
    this.longitude,
    this.jobImageUrl,
    this.language,
    this.preferredGender,
    this.nationality,
  });

  Map<String, dynamic> toMap() {
    return {
      'date': date.toString(),
      'deadline': deadline.toString(),
      'jobPosition': jobPosition,
      'title': title,
      'companyLogoUrl': companyLogoUrl,
      'jobDescription': jobDescription,
      'experience': experience,
      'jobType': jobType,
      'schedule': schedule,
      'address': address,
      'salaryRange': salaryRange,
      'currency': currency,
      'jobcategory': jobCategory,
      'salaryType': salaryType,
      'vacancies': vacancies,
      'isValid': isValid,
      'companyName': companyName, // searchable
      'companyNameLowerCase':
          companyName != null ? companyName.toString().toLowerCase() : "",

      // location
      'country': country != null ? country : "Not Provided", // searchAble
      'countryLowerCase': country != null ? country.toLowerCase() : "",
      'administrativeArea':
          administrativeArea != null ? administrativeArea : "Not Provided",
      'subAdministrativeArea': subAdministrativeArea != null
          ? subAdministrativeArea
          : "Not Provided",
      'name': name != null ? name : "Not Provided",
      'locality': locality != null ? locality : "Not Provided",
      'subLocality': subLocality != null ? subLocality : "Not Provided",
      'thoroughfare': thoroughfare != null ? thoroughfare : "Not Provided",
      'latitude': latitude,
      'longitude': longitude,
      'jobImageUrl': jobImageUrl,
      'language': language,
      'preferredGender': preferredGender,
      'nationality': nationality,
      AppStringConstants.createdAt: FieldValue.serverTimestamp(),
    };
  }

  static JobPostModel fromSnapshot(DocumentSnapshot snap) {
    try {
      return JobPostModel(
        salaryType: snap.data['salaryType'],
        jobCategory: snap.data['jobcategory'],
        salaryRange: snap.data['salaryRange'],
        currency: snap.data['currency'],
        address: snap.data['address'],
        schedule: snap.data['schedule'],
        jobType: snap.data['jobType'],
        experience: snap.data['experience'],
        jobDescription: snap.data['jobDescription'],
        companyLogoUrl: snap.data['companyLogoUrl'],
        title: snap.data['title'],
        jobPosition: snap.data['jobPosition'],
        deadline: snap.data['deadline'],
        date: snap.data['date'],
        isValid: snap.data['isValid'],
        vacancies: snap.data['vacancies'],
        companyName: snap.data['companyName'],
        // location
        country: snap.data['country'],
        administrativeArea: snap.data['administrativeArea'],
        subAdministrativeArea: snap.data['subAdministrativeArea'],
        name: snap.data['name'], // area name
        locality: snap.data['locality'],
        subLocality: snap.data['subLocality'],
        thoroughfare: snap.data['thoroughfare'],
        latitude: snap.data['latitude'],
        longitude: snap.data['longitude'],
        jobImageUrl: snap.data['jobImageUrl'],
        language: snap.data['language'],
        preferredGender: snap.data['preferredGender'],
        nationality: snap.data['nationality'],
      );
    } on NoSuchMethodError catch (e) {
      Helper.logPrint(_TAG, "Error : ${e.toString()}");
      return null;
    }
  }
}
