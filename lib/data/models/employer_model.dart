import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

// from here name and logoUrl to be passed to JobPostModel
class EmployerModel {
  String name, location, email, logoUrl, description, websiteUrl, nationality, countryCode;
  String phoneNumber;
  bool isVerified;
  double latitude, longitude;

  EmployerModel({
    @required this.name,
    this.location,
    @required this.email,
    this.logoUrl,
    this.phoneNumber,
    this.description,
    this.websiteUrl,
    this.nationality,
    this.countryCode,
    this.isVerified,
    this.latitude,
    this.longitude,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name, //  searchable,
      'nameLowerCase': name.toString().toLowerCase(),
      'location': location,
      'email': email,
      'logoUrl': logoUrl,
      'phoneNumber': phoneNumber,
      'description': description,
      'websiteUrl': websiteUrl,
      'nationality': nationality,
      'countryCode': countryCode,
      'isVerified': isVerified,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  static EmployerModel fromSnapshot(DocumentSnapshot snap) {
    return EmployerModel(
      name: snap.data['name'],
      location: snap.data['location'],
      email: snap.data['email'],
      logoUrl: snap.data['logoUrl'],
      phoneNumber: snap.data['phoneNumber'],
      description: snap.data['description'],
      websiteUrl: snap.data['websiteUrl'],
      nationality: snap.data['nationality'],
      countryCode: snap.data['countryCode'],
      isVerified: snap.data['isVerified'],
      latitude: snap.data['latitude'],
      longitude: snap.data['longitude'],
    );
  }

  static EmployerModel fromMap(Map<String, dynamic> json) {
    return EmployerModel(
      name: json['name'],
      logoUrl: json['logoUrl'],
      location: json['location'],
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      description: json['description'],
      websiteUrl: json['websiteUrl'],
      nationality: json['nationality'],
      countryCode: json['countryCode'],
      isVerified: json['isVerified'],
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }
}
