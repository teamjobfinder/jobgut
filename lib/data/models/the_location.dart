import 'package:cloud_firestore/cloud_firestore.dart';

class TheLocation {
  String country,
      administrativeArea,
      subAdministrativeArea,
      name,
      locality,
      subLocality,
      thoroughfare;
  double latitude, longitude;

  TheLocation({
    this.country,
    this.administrativeArea,
    this.subAdministrativeArea,
    this.name,
    this.locality,
    this.subLocality,
    this.thoroughfare,
    this.latitude,
    this.longitude,
  });

  Map<String, dynamic> toMap() {
    return {
      'country': country,
      'administrativeArea': administrativeArea,
      'subAdministrativeArea': subAdministrativeArea,
      'name': name,
      'locality': locality,
      'subLocality': subLocality,
      'thoroughfare': thoroughfare,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  static TheLocation fromJson(Map<String, dynamic> json) {
    return TheLocation(
      country: json['country'],
      administrativeArea: json['administrativeArea'],
      subAdministrativeArea: json['subAdministrativeArea'],
      name: json['name'],
      locality: json['locality'],
      subLocality: json['subLocality'],
      thoroughfare: json['thoroughfare'],
      latitude: json['position']['latitude'],
      longitude: json['position']['longitude'],
    );
  }

  static TheLocation fromSnapshot(DocumentSnapshot snap) {
    return TheLocation(
      country: snap.data['country'],
      administrativeArea: snap.data['administrativeArea'],
      subAdministrativeArea: snap.data['subAdministrativeArea'],
      name: snap.data['name'],
      locality: snap.data['locality'],
      subLocality: snap.data['subLocality'],
      thoroughfare: snap.data['thoroughfare'],
      latitude: snap.data['latitude'],
      longitude: snap.data['longitude'],
    );
  }
}
