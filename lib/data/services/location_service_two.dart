import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/utils/location_helper.dart';

/// No pottential errors to handle
class LocationServiceTwo {
  final Geolocator geolocator = Geolocator()
    ..forceAndroidLocationManager = true;

  // 1. get current location
  Future<TheLocation> readCurrentLocation() async {
    List<Placemark> placeMarks;
    await geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {
      placeMarks = await Geolocator().placemarkFromCoordinates(
        position.latitude,
        position.longitude,
        localeIdentifier: "en_US",
      );
    }).catchError((e) {
      print("Location error $e");
    });
    var json = placeMarks[0].toJson();
    TheLocation location = TheLocation.fromJson(json);
    return location;
    // Position position = await geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.medium);
    // var json = position.toJson();
    // TheLocation theLocation = TheLocation.fromJson(json);
    // return theLocation;
  }

  // 2. search place and add marker
  Future<Placemark> searchALocation(
      String searchText, GoogleMapController googleMapController) async {
    var placemarks = await geolocator.placemarkFromAddress(searchText);
    return placemarks[0];
  }

  // 3. checks gps and requests
  Future<void> requestLocation() async {
    await LocationHelper().requestLocation();
  }

  // 4.
  /// checks device GPS status
  Stream<bool> gpsStatusStream() async* {
    bool enabled;
    while (true) {
      try {
        bool isEnabled = await Geolocator().isLocationServiceEnabled();
        if (enabled != isEnabled) {
          enabled = isEnabled;
          yield enabled;
        }
      } catch (error) {}
      await Future.delayed(Duration(seconds: 5));
    }
  }
}
