import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/utils/helper.dart';
import 'package:uuid/uuid.dart';

/// No errors to handle
class EmployerService {
  final String _TAG = "EmployerService";
  var postsCollection = Firestore.instance.collection(AppStringConstants.posts);
  var employersCollection =
      Firestore.instance.collection(AppStringConstants.employers);
  var usersCollection = Firestore.instance.collection(AppStringConstants.users);
  var workersCollection =
      Firestore.instance.collection(AppStringConstants.workers);

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  // 1. create profile
  Future<void> createProfile(EmployerModel employerModel) async {
    FirebaseUser user = await firebaseAuth.currentUser();
    await employersCollection.document(user.uid).setData(
      {AppStringConstants.profile: employerModel.toMap()},
    );
  }

  // 2. read profile as stream
  Stream<EmployerModel> readProfileSTream(String userId) {
    return employersCollection.document(userId).snapshots().map((docSnapshot) {
      Map map =
          Map<String, dynamic>.from(docSnapshot[AppStringConstants.profile]);
      EmployerModel employerModel = EmployerModel.fromMap(map);
      return employerModel;
    });
  }

  /**
   * This causes error from applies page, while opening details page
   */
  Future<EmployerModel> readEmployerOfAPost(
      DocumentSnapshot documentSnapshot) async {
    DocumentSnapshot profileSnapshot =
        await documentSnapshot.reference.parent().parent().get();
    Helper.logPrint(_TAG, "profile snap : $profileSnapshot");
    Map map =
        Map<String, dynamic>.from(profileSnapshot[AppStringConstants.profile]);
    EmployerModel employerModel = EmployerModel.fromMap(map);
    return employerModel;
  }

  // read profile as future
  Future<EmployerModel> readEmployerProfile() async {
    var user = await firebaseAuth.currentUser();
    var employerSnap = await employersCollection.document(user.uid).get();
    var map =
        Map<String, dynamic>.from(employerSnap[AppStringConstants.profile]);
    var employerModel = EmployerModel.fromMap(map);
    return employerModel;
  }

  // 3. update profile
  Future<void> updateProfile(EmployerModel employerModel) async {
    var user = await FirebaseAuth.instance.currentUser();
    await employersCollection.document(user.uid).setData(
      {AppStringConstants.profile: employerModel.toMap()},
    );
  }

  // 4. attach in users collection
  Future<void> attachInUsers() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    await usersCollection
        .document(user.uid)
        .setData({"type": AppStringConstants.employerType});
  }

  // 5. upload job image and rerturns download link
  Future<String> createJobImage(File image) async {
    var user = await FirebaseAuth.instance.currentUser();
    var uid = user.uid;
    Uuid uuid = Uuid();
    var imageId = uuid.v1();
    StorageReference reference = FirebaseStorage.instance
        .ref()
        .child("${AppStringConstants.jobImagesRef}$uid/$imageId");
    StorageUploadTask uploadTask = reference.putFile(image);
    var downLoadUri = await (await uploadTask.onComplete).ref.getDownloadURL();
    return downLoadUri.toString();
  }

  // 7. get posts reference of an employer
  // used internally in this class
  Future<CollectionReference> getPostsRef() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    var ref = employersCollection
        .document(user.uid)
        .collection(AppStringConstants.posts);
    return ref;
  }

  // 7. create a job post
  // used in [JobsRepository]
  Future<void> createAPost(JobPostModel jobPostModel) async {
    await Firestore.instance.runTransaction((transaction) async {
      await getPostsRef().then((postsRef) {
        postsRef.add(jobPostModel.toMap());
      });
    });
  }

  // 8. update a job post
  // used in [JobsRepository]
  Future<void> updateAPost(
      DocumentReference documentReference, JobPostModel jobPostModel) async {
    await Firestore.instance.runTransaction((transaction) async {
      transaction.update(documentReference, jobPostModel.toMap());
    });
  }

  // 9. delete a job post
  // used in [JobsRepository]
  /// It won't delete the subcollection, e.g "Applications"
  /// so null check is done inside [JobPostModel] which throws [NoSuchMethodError]
  /// null check also done in the related widget [FeedThumbnail]
  Future<void> deleteAPost(DocumentReference documentReference) async {
    await Firestore.instance.runTransaction((transaction) async {
      transaction.delete(documentReference);
    });
  }

  // 10. stream of posts. need to add an event to listen this stream
  // used in [JobsRepository]
  Stream<List<JobPostDocumentModel>> readAllJobsOfAnEmployer(String userId) {
    CollectionReference postsCollection = employersCollection
        .document(userId)
        .collection(AppStringConstants.posts);
    return postsCollection
        .orderBy(AppStringConstants.createdAt, descending: true)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents.map((docSnapshot) {
        DocumentReference documentReference = docSnapshot.reference;
        JobPostModel jobPostModel = JobPostModel.fromSnapshot(docSnapshot);
        JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
          documentReference: documentReference,
          jobPostModel: jobPostModel,
          documentSnapshot: docSnapshot,
        );
        return jobPostDocumentModel;
      }).toList();
    });
  }

  // get number of applications on a post
  Stream<int> readNumberOfApplications(DocumentReference documentReference) {
    CollectionReference applicationsCollection =
        documentReference.collection(AppStringConstants.applications);
    return applicationsCollection.snapshots().map((querySnapshot) {
      return querySnapshot.documents.length;
    });
  }

  // get list of applicants as stream
  Stream<List<String>> readStreamOfApplicantsId(
      DocumentReference documentReference) {
    Helper.logPrint(_TAG, "reading...");
    CollectionReference applicationsCollection =
        documentReference.collection(AppStringConstants.applications);
    return applicationsCollection.snapshots().map((querySnapshot) {
      Helper.logPrint(_TAG, "reading...2");
      // document ids in applications collection are querySnapshot
      // List<Ids> = QuerySnapshot
      // Id = DocumentSnapshot
      return querySnapshot.documents.map((docSnapshot) {
        Helper.logPrint(_TAG, "reading...3");
        Helper.logPrint(_TAG, docSnapshot.documentID);
        return docSnapshot.documentID;
      }).toList();
    });
  }

  // 6. get list of applicants
  Future<List<WorkerModel>> readListOfApplicants(List<String> workerIds) async {
    List<WorkerModel> list = List<WorkerModel>();
    for (var wid in workerIds) {
      DocumentSnapshot documentSnapshot =
          await workersCollection.document(wid).get();
      var map = Map<String, dynamic>.from(
          documentSnapshot[AppStringConstants.profile]);
      var workerModel = WorkerModel.fromMap(map);
      list.add(workerModel);
    }
    return list;
  }
}
