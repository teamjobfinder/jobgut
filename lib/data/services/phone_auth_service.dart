import 'package:firebase_auth/firebase_auth.dart';

class PhoneAuthService {
  static String verificationId;
  bool isVerificationSuccess = false;

  final PhoneVerificationCompleted verified =
      (AuthCredential credential) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    try {
      await user.linkWithCredential(credential);
    } catch (e) {}
  };

  final PhoneVerificationFailed verificationfailed =
      (AuthException authException) {};

  final PhoneCodeSent smsSent = (String verificationId, [int forceResend]) {
    verificationId = verificationId; // need to return it
  };

  final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
    verificationId = verId; // need to return it
  };

  Future<void> verifyPhoneNumber(String phoneNumber) async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      timeout: const Duration(seconds: 100),
      verificationCompleted: verified,
      verificationFailed: verificationfailed,
      codeSent: smsSent,
      codeAutoRetrievalTimeout: autoTimeout,
    );
  }

  Future<void> verifyWithOTP(smsCode, verId, phoneNumber) async {
    var user = await FirebaseAuth.instance.currentUser();
    AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);
    await user.linkWithCredential(credential);
  }
}
