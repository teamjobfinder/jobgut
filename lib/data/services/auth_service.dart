import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jobs/data/services/phone_auth_service.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:meta/meta.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

/// errors are handled
class AuthService {
  static const String tag = "AUTH_SERVICE";

  PhoneAuthService phoneAuthService = PhoneAuthService();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  // 1. register with email and passsword
  // used by [IUserRepository]
  /// Throws Some [Exception] if fails
  Future<void> registerWithEmailAndPassword(
      {@required String email, @required String password}) async {
    try {
      await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 2. sign in with email and password
  // used by [IUserRepository]
  /// Throws Some [Exception] if fails
  Future<void> signInWithEmailAndPassword(
      {@required String email, @required String password}) async {
    try {
      print("$tag trying email login");
      await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      print("$tag err email login ${e.toString()}");
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 3. google sign in
  // used by [IUserRepository]
  /// Throws Some [Exception] if fails
  Future<AuthResult> signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      try {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        return await firebaseAuth.signInWithCredential(credential);
      } on PlatformException catch (e) {
        throw Exception(e.message);
      } on SocketException catch (_) {
        throw Exception(AppStrings.noInternetConnection);
      }
    } else {
      throw Exception(AppStrings.noGoogleAccountFound);
    }
  }

  // 4. check signed in or not
  // used by [IUserRepository] and [AuthBloc]
  Future<bool> isSignedIn() async {
    var currentUser = await firebaseAuth.currentUser();
    return currentUser != null;
  }

  // 5. sign out
  // used by [IUserRepository]
  Future<void> signOut() async {
    await firebaseAuth.signOut();
    await googleSignIn.signOut();
  }

  // 6. check if user is guest
  // used by [IUserRepository]
  Future<bool> isGuest() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    return user.isAnonymous;
  }

  // 7. sign in as guest
  // used by [WorkerRepository]
  /// Throws Some [Exception] if fails
  Future<void> signInAsGuest() async {
    try {
      await firebaseAuth.signInAnonymously();
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 8. link account with email
  // used by [WorkerRepository]
  /// Throws Some [Exception] if fails
  Future<void> linkAccountWithEmail({
    @required String email,
    @required String password,
  }) async {
    FirebaseUser user = await firebaseAuth.currentUser();
    AuthCredential credential = EmailAuthProvider.getCredential(
      email: email,
      password: password,
    );
    try {
      await user.linkWithCredential(credential);
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 9. link account with google
  // used by [WorkerRepository]
  /// Throws Some [Exception] if fails
  Future<AuthResult> linkAccountWithGoogle() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    final GoogleSignInAccount googleUser = await googleSignIn.signIn();

    if (googleUser != null) {
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      try {
        return await user.linkWithCredential(credential);
      } on PlatformException catch (e) {
        throw Exception(e.message);
      } on SocketException catch (_) {
        throw Exception(AppStrings.noInternetConnection);
      }
    } else {
      throw Exception("No Google Account Found");
    }
  }

  // 10. link user with phone number
  // used by [EmployerrRepository]
  Future<void> linkCredentialWithPhone(String phoneNumber) async {
    await phoneAuthService.verifyPhoneNumber(phoneNumber);
  }

  // 11. link user with otp code
  // used by [EmployerrRepository]
  Future<void> linkCredentialWithOTP(
    String smsCode,
    String verId,
    String phoneNumber,
  ) async {
    await phoneAuthService.verifyWithOTP(smsCode, verId, phoneNumber);
  }

  // 12. check if the user is new
  bool isNewUser(AuthResult authResult) {
    return authResult.additionalUserInfo.isNewUser;
  }

  // 13. sign in with facebook
  Future<AuthResult> signInWithFacebook() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.error:
        print("fb Error");
        throw Exception(FacebookLoginStatus.error.toString());
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("fb CancelledByUser");
        throw Exception(FacebookLoginStatus.error.toString());
        break;
      case FacebookLoginStatus.loggedIn:
        print("fb LoggedIn");
        final token = result.accessToken.token;
        AuthCredential fbCredential =
            FacebookAuthProvider.getCredential(accessToken: token);
        try {
          return await firebaseAuth.signInWithCredential(fbCredential);
        } on PlatformException catch (e) {
          throw Exception(e.message);
        } on SocketException catch (_) {
          throw Exception(AppStrings.noInternetConnection);
        }
        break;
    }
  }

  Future<AuthResult> linkAccountWithFacebook() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.error:
        print("fb Error");
        throw Exception(FacebookLoginStatus.error.toString());
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("fb CancelledByUser");
        throw Exception(FacebookLoginStatus.error.toString());
        break;
      case FacebookLoginStatus.loggedIn:
        print("fb LoggedIn");
        final token = result.accessToken.token;
        AuthCredential fbCredential =
            FacebookAuthProvider.getCredential(accessToken: token);
        try {
          return await user.linkWithCredential(fbCredential);
        } on PlatformException catch (e) {
          throw Exception(e.message);
        } on SocketException catch (_) {
          throw Exception(AppStrings.noInternetConnection);
        }
        break;
    }
  }

  // get firebase user id
  Future<String> getFirebaseUserId() async {
    var user = await firebaseAuth.currentUser();
    return user.uid;
  }


}
