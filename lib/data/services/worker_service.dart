import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/utils/helper.dart';
import 'package:jobs/utils/translation_service.dart';

/// No potential Errors to handle
class WorkerService {
  final String _TAG = "WorkerService";
  var applicationsCollection =
      Firestore.instance.collection(AppStringConstants.applications);
  var postsCollection = Firestore.instance.collection(AppStringConstants.posts);
  var employersCollection =
      Firestore.instance.collection(AppStringConstants.employers);
  var usersCollection = Firestore.instance.collection(AppStringConstants.users);
  var workersCollection =
      Firestore.instance.collection(AppStringConstants.workers);

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  // 1. create profile
  Future<void> createAnWorker(WorkerModel workerModel) async {
    FirebaseUser user = await firebaseAuth.currentUser();
    await workersCollection.document(user.uid).setData(
      {AppStringConstants.profile: workerModel.toMap()},
    );
  }

  // 2. attach in users collection
  Future<void> attachInUsers() async {
    FirebaseUser user = await firebaseAuth.currentUser();
    await usersCollection
        .document(user.uid)
        .setData({"type": AppStringConstants.workerType});
  }

  // used in bloc for reading profile
  Stream<WorkerModel> readWorkerProfilestream(String userId) {
    return workersCollection.document(userId).snapshots().map((docSnapshot) {
      Map map =
          Map<String, dynamic>.from(docSnapshot[AppStringConstants.profile]);
      WorkerModel employerModel = WorkerModel.fromMap(map);
      return employerModel;
    });
  }

  // 3. read profile
  // internally used here, not in bloc for reading profile
  Future<WorkerModel> readProfile() async {
    var user = await firebaseAuth.currentUser();
    var workerSnap = await workersCollection.document(user.uid).get();
    var map = Map<String, dynamic>.from(workerSnap[AppStringConstants.profile]);
    var workerModel = WorkerModel.fromMap(map);
    return workerModel;
  }

  // 4. update profile
  Future<void> updateProfile(WorkerModel workerModel) async {
    var user = await firebaseAuth.currentUser();
    await workersCollection.document(user.uid).setData(
      {AppStringConstants.profile: workerModel.toMap()},
    );
  }

  // 5. check if applied on a job
  // used in [JobsRepository]
  Future<bool> hasApplied(DocumentReference documentReference) async {
    var user = await firebaseAuth.currentUser();
    var document = await documentReference
        .collection(AppStringConstants.applications)
        .document(user.uid)
        .get();
    if (document == null || !document.exists) {
      return false;
    } else {
      return true;
    }
  }

  // 6. apply on a job
  // used in [JobsRepository]
  Future<void> applyOnAJob(DocumentReference jobPostReference) async {
    var user = await firebaseAuth.currentUser();
    var workerModel = await readProfile();
    await Firestore.instance.runTransaction((transaction) {
      jobPostReference
          .collection(AppStringConstants.applications)
          .document(user.uid)
          .setData(
        {
          "Profile": workerModel.toMap(),
          "workerId": user.uid,
          "jobPostId": jobPostReference,
        },
      );
    });
  }

  // 7. read list of applied jobs
  // used in [JobsRepository]
  Stream<List<JobPostDocumentModel>> readListOfAppliedJobs(String userId) {
    Helper.logPrint(_TAG, "Reading...");
    return Firestore.instance
        .collectionGroup("applications")
        .where(
          "workerId",
          isEqualTo: userId,
        )
        .snapshots()
        .map((querySnapshot) {
      Helper.logPrint(_TAG, "Reading 2");
      return querySnapshot.documents.map((docSnapshot) {
        DocumentReference jobPostDocRef = docSnapshot.data["jobPostId"];
        jobPostDocRef.snapshots().map((jobPostDocSnapshot) {
          JobPostModel jobPostModel =
              JobPostModel.fromSnapshot(jobPostDocSnapshot);
          JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
            jobPostModel: jobPostModel,
            documentReference: jobPostDocRef,
            documentSnapshot: jobPostDocSnapshot,
          );
          return jobPostDocumentModel;
        });
      }).toList();
    });
  }

  // 8. read jobs in a location
  // used in [JobsRepository]
  // TODO a bit confusion with location_search_result_page.dart
  Stream<List<JobPostDocumentModel>> readJobsInALocation(String location) {
    var collectionGroup =
        Firestore.instance.collectionGroup(AppStringConstants.posts);
    return collectionGroup
        .orderBy(AppStringConstants.createdAt, descending: true)
        .where(AppStringConstants.country, isEqualTo: location)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents.map((docSnapshot) {
        DocumentReference documentReference = docSnapshot.reference;
        JobPostModel jobPostModel = JobPostModel.fromSnapshot(docSnapshot);
        JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
          documentReference: documentReference,
          jobPostModel: jobPostModel,
          documentSnapshot: docSnapshot,
        );
        return jobPostDocumentModel;
      }).toList();
    });
  }

  // jobs in south korea
  Stream<List<JobPostDocumentModel>> readJobsInSouthkorea() {
    var collectionGroup =
        Firestore.instance.collectionGroup(AppStringConstants.posts);
    return collectionGroup
        .orderBy(AppStringConstants.createdAt, descending: true)
        .where(AppStringConstants.country, isEqualTo: "South Korea")
        .snapshots()
        .map((snapshot) {
      return snapshot.documents.map((docSnapshot) {
        DocumentReference documentReference = docSnapshot.reference;
        JobPostModel jobPostModel = JobPostModel.fromSnapshot(docSnapshot);
        JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
          documentReference: documentReference,
          jobPostModel: jobPostModel,
          documentSnapshot: docSnapshot,
        );
        return jobPostDocumentModel;
      }).toList();
    });
  }

  // 9. read all the jobs over the world
  // used in [JobsRepository]
  Stream<List<JobPostDocumentModel>> readAllTheJobsPosted() {
    var collectionGroup =
        Firestore.instance.collectionGroup(AppStringConstants.posts);
    return collectionGroup
        .orderBy(AppStringConstants.createdAt, descending: true)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents.map((docSnapshot) {
        DocumentReference documentReference = docSnapshot.reference;
        JobPostModel jobPostModel = JobPostModel.fromSnapshot(docSnapshot);
        JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
          documentReference: documentReference,
          jobPostModel: jobPostModel,
          documentSnapshot: docSnapshot,
        );
        return jobPostDocumentModel;
      }).toList();
    });
  }

  // 10. read filtered jobs by category and type
  // used in [JobsRepository]
  Stream<List<JobPostDocumentModel>> readFilteredJobsByCategoryAndType(
    String category,
    String type,
  ) {
    var collectionGroup =
        Firestore.instance.collectionGroup(AppStringConstants.posts);
    return collectionGroup
        .orderBy(AppStringConstants.createdAt, descending: true)
        .where(AppStringConstants.workerCategory, isEqualTo: category)
        .where(AppStringConstants.workerJobType, isEqualTo: type)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents.map((documentSnapshot) {
        DocumentReference documentReference = documentSnapshot.reference;
        JobPostModel jobPostModel = JobPostModel.fromSnapshot(documentSnapshot);
        JobPostDocumentModel jobPostDocumentModel = JobPostDocumentModel(
          documentReference: documentReference,
          documentSnapshot: documentSnapshot,
          jobPostModel: jobPostModel,
        );
        return jobPostDocumentModel;
      }).toList();
    });
  }

  // 11. search jobs by nationality and language
  // used in [JobsRepository]
  Stream<List<JobPostModel>> searchJobsByNationalityAndLanguage(
    String nationality,
    String language,
  ) {
    print("service called for searching");
    var collectionGroup =
        Firestore.instance.collectionGroup(AppStringConstants.posts);
    return collectionGroup
        .orderBy(AppStringConstants.createdAt, descending: true)
        .where(AppStringConstants.nationality, isEqualTo: nationality)
        .where(AppStringConstants.language, isEqualTo: language)
        .snapshots()
        .map((snapshot) {
      return snapshot.documents
          .map((doc) => JobPostModel.fromSnapshot(doc))
          .toList();
    });
  }

  // 12. both for worker and employer
  // fetch a job post from documentsnapshot
  // used in [JobsRepository]
  JobPostModel readJobPostFromSnapshot(DocumentSnapshot documentSnapshot) {
    JobPostModel jobPostModel = JobPostModel.fromSnapshot(documentSnapshot);
    return jobPostModel;
  }

  // 13. both for worker and employer
  Future<JobPostModel> translateJobPost(
      String languageCode, JobPostModel jobPostModel) async {
    return await TranslationService.translateJobPost(
        languageCode, jobPostModel);
  }
}
