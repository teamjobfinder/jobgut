import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:get/get.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/ui/pages/common/shared_post_details_page.dart';
import 'package:jobs/utils/helper.dart';

class DynamicUrlService {
  final String _TAG = "DynamicUrlService";

  // create dynaic link
  Future<String> createPostLink(
      JobPostModel jobPostModel, DocumentReference documentReference) async {
    Helper.logPrint(_TAG, "creating post link");
    String title = jobPostModel.title;
    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: "https://jobgut.page.link",
      link: Uri.parse("https://jobgut.page.link/post?title=$title"),
      androidParameters: AndroidParameters(
        packageName: "com.jobbee.jobs",
        minimumVersion: 11,
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        title: title,
        description: "Job Posted in JobGut App",
      ),
    );
    final ShortDynamicLink shortDynamicLink = await parameters.buildShortLink();
    final Uri shortUrl = shortDynamicLink.shortUrl;
    Helper.logPrint(_TAG, "link : ${shortUrl.toString()}");
    return shortUrl.toString();
  }

  // handle dynaic link
  Future handleDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    _handleDeepLink(data);
    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData dynamicLink) async {
        _handleDeepLink(dynamicLink);
      },
      onError: (OnLinkErrorException e) async {
        Helper.logPrint(_TAG, "Error : ${e.toString()}");
      },
    );
  }

  void _handleDeepLink(PendingDynamicLinkData data) {
    Helper.logPrint(_TAG, "Handling deep link");
    final Uri deepLink = data?.link;
    if (deepLink != null) {
      var title = deepLink.queryParameters['title'];
      Get.to(SharedPostDetailsPage(title: title));
      Helper.logPrint(_TAG, "link : $deepLink");
    } else {
      Helper.logPrint(_TAG, "Handling deep link null");
    }
  }
}
