import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:jobs/res/strings/app_string_constants.dart';

/// No potential errors to handle
class UsersService {

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  
  // 1. check type of user
  // used by [AuthBloc]
  Future<String> employerOrWorker() async {
    var user = await firebaseAuth.currentUser();
    DocumentSnapshot ds =
        await Firestore.instance.collection(AppStringConstants.users).document(user.uid).get();
    return ds[AppStringConstants.type];
  }

  // 2. upload users profile photo to database and returns download url
  Future<String> uploadProfileImageToDatabase(File image) async {
    var user = await FirebaseAuth.instance.currentUser();
    var uid = user.uid;
    StorageReference reference =
        FirebaseStorage.instance.ref().child("${AppStringConstants.profileImageRef}$uid/");
    StorageUploadTask uploadTask = reference.putFile(image);
    var downLoadUri = await (await uploadTask.onComplete).ref.getDownloadURL();
    return downLoadUri.toString();
  }

}