import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/services/employer_service.dart';
import 'package:jobs/data/services/location_service_two.dart';
import 'package:jobs/data/services/worker_service.dart';

class JobsRepository {
  EmployerService employerService = EmployerService();
  WorkerService workerService = WorkerService();
  LocationServiceTwo locationService = LocationServiceTwo();

  // 1. add a job
  Future<void> addAJob(JobPostModel jobPostModel) async {
    return employerService.createAPost(jobPostModel);
  }

  // 2. update a job post
  Future<void> updateAJobPost(
      DocumentReference documentReference, JobPostModel jobPostModel) async {
    return employerService.updateAPost(documentReference, jobPostModel);
  }

  // 3. delete a job post
  /// It won't delete the subcollection, e.g "Applications"
  /// 
  /// so null check is done inside [JobPostModel] which throws [NoSuchMethodError]
  /// 
  /// null check also done in the related widget [FeedThumbnail]
  Future<void> deleteAJobPost(DocumentReference documentReference) async {
    return employerService.deleteAPost(documentReference);
  }

  // 4. read jobs of an employee
  Stream<List<JobPostDocumentModel>> getJobPostsOfAnEmployer(String userId) {
    return employerService.readAllJobsOfAnEmployer(userId);
  }

  // 5. get all the jobs posted over the world
  Stream<List<JobPostDocumentModel>> getAllTheJobsPosted() {
    return workerService.readAllTheJobsPosted();
  }

  // 6. get jobs in a location
  Stream<List<JobPostDocumentModel>> getJobsInALocation(String location) {
    return workerService.readJobsInALocation(location);
  }

  Stream<List<JobPostDocumentModel>> getJobsInSouthkorea() {
    return workerService.readJobsInSouthkorea();
  }

  // 7. check if already applied in a job
  Future<bool> hasAppliedOnAJob(DocumentReference documentReference) async {
    return workerService.hasApplied(documentReference);
  }

  // 8. apply on a single job
  Future<void> applyOnASingleJob(DocumentReference documentReference) {
    return workerService.applyOnAJob(documentReference);
  }

  // 9. get list of applied jobs
  Stream<List<JobPostDocumentModel>> getAppliedJobs(String userId) {
    return workerService.readListOfAppliedJobs(userId);
  }

  // 10. get category jobtype filtered jobs
  Stream<List<JobPostDocumentModel>> getFilteredJobsByCategoryAndType(
      String category, String type) {
    return workerService.readFilteredJobsByCategoryAndType(category, type);
  }

  // 11. search jobs by nationality and language
  Stream<List<JobPostModel>> getJobsByNationalityAndLanguage(
    String nationality,
    String language,
  ) {
    print("repository called for searching");
    return workerService.searchJobsByNationalityAndLanguage(
        nationality, language);
  }

  // 12. both for worker and employer
  // used in [JobDetailsPage]
  JobPostModel getJobPostFromSnapShot(DocumentSnapshot documentSnapshot) {
    return workerService.readJobPostFromSnapshot(documentSnapshot);
  }

  // 13. for employer
  // used in [JobDetailsPage]
  Stream<int> getNumberOfApplicationsInAJob(
      DocumentReference documentReference) {
    return employerService.readNumberOfApplications(documentReference);
  }

  // 14. both for worker and employer
  // used in [JobDetailsPage]
  Future<JobPostModel> translateJobPost(
      String languageCode, JobPostModel jobPostModel) async {
    return await workerService.translateJobPost(languageCode, jobPostModel);
  }
}
