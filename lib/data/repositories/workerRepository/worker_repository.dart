import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';

class WorkerRepository {
  FirebaseAuth firebaseAuth;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  UsersRepository usersRepository;

  var workersCollection = Firestore.instance.collection("workers");

  WorkerRepository() {
    firebaseAuth = FirebaseAuth.instance;
    usersRepository = UsersRepository();
  }

  Future<FirebaseUser> signUpWorkerWithEmailPass(
      String email, String pass) async {
    try {
      var authResult = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: pass,
      );
      print("REPO : ${authResult.user.email}");
      return authResult.user;
    } on PlatformException catch (_) {
      String authError = "";
      throw Exception(authError);
    }
  }

  // sign in worker as guest
  Future<FirebaseUser> signInWorkerAsGuest() async {
    AuthResult result = await FirebaseAuth.instance.signInAnonymously();
    return result.user;
  }

  // sign in with email and password
  Future<FirebaseUser> signInWorker(String email, String password) async {
    try {
      var authresult = await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return authresult.user;
    } on PlatformException catch (_) {
      String authError = "";
      // switch (e.code) {
      //   case ErrorCodes.ERROR_C0DE_NETWORK_ERROR:
      //     authError = ErrorMessages.ERROR_C0DE_NETWORK_ERROR;
      //     break;
      //   case ErrorCodes.ERROR_USER_NOT_FOUND:
      //     authError = ErrorMessages.ERROR_USER_NOT_FOUND;
      //     break;
      //   case ErrorCodes.ERROR_TOO_MANY_REQUESTS:
      //     authError = ErrorMessages.ERROR_TOO_MANY_REQUESTS;
      //     break;
      //   case ErrorCodes.ERROR_INVALID_EMAIL:
      //     authError = ErrorMessages.ERROR_INVALID_EMAIL;
      //     break;
      //   case ErrorCodes.ERROR_CODE_USER_DISABLED:
      //     authError = ErrorMessages.ERROR_CODE_USER_DISABLED;
      //     break;
      //   case ErrorCodes.ERROR_CODE_WRONG_PASSWORD:
      //     authError = ErrorMessages.ERROR_CODE_WRONG_PASSWORD;
      //     break;
      //   case ErrorCodes.ERROR_CODE_EMAIL_ALREADY_IN_USE:
      //     authError = ErrorMessages.ERROR_CODE_EMAIL_ALREADY_IN_USE;
      //     break;
      //   case ErrorCodes.ERROR_OPERATION_NOT_ALLOWED:
      //     authError = ErrorMessages.ERROR_OPERATION_NOT_ALLOWED;
      //     break;
      //   case ErrorCodes.ERROR_CODE_WEAK_PASSWORD:
      //     authError = ErrorMessages.ERROR_CODE_WEAK_PASSWORD;
      //     break;
      //   default:
      //     authError = ErrorMessages.DEFAULT;
      //     break;
      // }
      throw Exception(authError);
    }
  }

  // sign out
  Future<void> signOutWorker() async {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
  }

  // check signIn
  Future<bool> isWorkerSignedIn() async {
    var currentUser = await firebaseAuth.currentUser();
    return currentUser != null;
  }

  Future<bool> isGuest() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user.isAnonymous;
  }

  // link guest with email
  Future<FirebaseUser> linkGuestAccountWithEmail(
      String email, String password) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    AuthCredential credential = EmailAuthProvider.getCredential(
      email: email,
      password: password,
    );
    AuthResult result = await user.linkWithCredential(credential);
    return result.user;
  }

  // link guest with google
  // not handled in bloc
  Future linkGuestWithGoogle() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final GoogleSignInAccount googleUser = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await user.linkWithCredential(credential).then((authresult) async {
      WorkerModel workerModel = WorkerModel(
        email: authresult.user.email,
        name: authresult.user.displayName,
      );
      addGoogleSignedWorker(workerModel);
    }).catchError((e) {
      Fluttertoast.showToast(
        msg: "Google sign in error : ${e.toString()}",
      );
    });
  }

  Future<void> addGoogleSignedWorker(WorkerModel workerModel) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance.collection("workers").document(user.uid).setData(
      {"Profile": workerModel.toMap()},
    );
    await Firestore.instance
        .collection("users")
        .document(user.uid)
        .setData({"type": "Worker"});
  }

  // get current user
  Future<FirebaseUser> getCurrentWorkerId() async {
    return await FirebaseAuth.instance.currentUser();
  }

  // add an employer to workers collection
  Future<void> addAnWorker(WorkerModel workerModel) async {
    FirebaseUser user = await getCurrentWorkerId();
    await Firestore.instance.collection("workers").document(user.uid).setData(
      {"Profile": workerModel.toMap()},
    );
    await Firestore.instance
        .collection("users")
        .document(user.uid)
        .setData({"type": "Worker"});
  }

  // hasAppliedOnAJobPost
  Future<bool> hasApplied(DocumentReference documentReference) async {
    var user = await getCurrentWorkerId();
    var document = await documentReference
        .collection("applications")
        .document(user.uid)
        .get();
    if (document == null || !document.exists) {
      return false;
    } else {
      return true;
    }
  }

  Future<void> applyOnThisJob(DocumentReference jobPostReference) async {
    var user = await getCurrentWorkerId();
    var workerModel = await getCurrentWorker();
    await Firestore.instance.runTransaction((transaction) {
      jobPostReference.collection("applications").document(user.uid).setData(
        {
          "Profile": workerModel.toMap(),
          "workerId": user.uid,
          "jobPostId": jobPostReference,
        },
      );
    });
  }

  Future<WorkerModel> getCurrentWorker() async {
    var user = await FirebaseAuth.instance.currentUser();
    var workerSnap = await workersCollection.document(user.uid).get();
    var map = Map<String, dynamic>.from(workerSnap["Profile"]);
    var workerModel = WorkerModel.fromMap(map);
    return workerModel;
  }

  Future<List<JobPostModel>> getListOfAppliedJobs(
      List<DocumentSnapshot> documentReference) async {
    List<JobPostModel> list = List<JobPostModel>();

    for (var ref in documentReference) {
      DocumentReference jobPostDocRef = ref.data["jobPostId"];
      DocumentSnapshot documentSnapshot = await jobPostDocRef.get();
      var model = JobPostModel.fromSnapshot(documentSnapshot);
      list.add(model);
    }

    return list;
  }

  // search posts in a country
  // wont need it when we convert it in service
  Future<List<JobPostModel>> searchJobsInALocation(
      List<DocumentSnapshot> documentSnapshots) async {
    List<JobPostModel> list = List<JobPostModel>();
    for (var snap in documentSnapshots) {
      JobPostModel model = JobPostModel.fromSnapshot(snap);
      print("Title : ${model.title}");
      list.add(model);
    }
    print("Found Jobs : ${list.length}");
    return list;
  }

  // update worker profile
  Future<void> updateWorkerProfile(WorkerModel workerModel) async {
    var user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance.collection("workers").document(user.uid).setData(
      {"Profile": workerModel.toMap()},
    ).then((_) {
      print("Profile update successful");
    }).catchError((e) {
      print("Error while updating profile : ${e.toString()}");
    });
  }
}
