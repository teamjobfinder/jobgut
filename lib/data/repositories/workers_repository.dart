import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/i_user_repository.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/data/services/worker_service.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/utils/failure.dart';
import 'package:meta/meta.dart';

/// Potential errors are handled
class WorkersRepository implements IUserRepository {
  AuthService authService = AuthService();
  WorkerService workerService = WorkerService();
  UsersService usersService = UsersService();

  @override
  Future<bool> isGuest() async {
    return await authService.isGuest();
  }

  @override
  Future<bool> isSignedIn() async {
    return await authService.isSignedIn();
  }

  /// If fails, throws [Failure]
  @override
  Future<void> registerWithEmailpassword({
    @required String email,
    @required String password,
  }) async {
    try {
      await authService.registerWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  @override
  Future<void> signInWithEmailpassword({
    @required String email,
    @required String password,
  }) async {
    try {
      await authService.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  /// has 4 responsibilities 1.signInWithGoogle 2. isNewUser 2.createProfile 3.attachInUsers
  /// for old users checks 5. employerOrWorker()
  @override
  Future<String> signInWithGoogle() async {
    try {
      AuthResult authResult = await authService.signInWithGoogle();
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        WorkerModel workerModel = WorkerModel(
          email: authResult.user.email,
          name: authResult.user.displayName,
        );
        await workerService.createAnWorker(workerModel);
        await workerService.attachInUsers();
        return AppStringConstants.workerType;
      } else {
        return await usersService.employerOrWorker();
      }
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  /// has 4 responsibilities 1.signInWithGoogle 2. isNewUser 2.createProfile 3.attachInUsers
  /// for old users checks 5. employerOrWorker()
  @override
  Future<String> signInWithFacebook() async {
    try {
      AuthResult authResult = await authService.signInWithFacebook();
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        WorkerModel workerModel = WorkerModel(
          email: authResult.user.email,
          name: authResult.user.displayName,
        );
        await workerService.createAnWorker(workerModel);
        await workerService.attachInUsers();
        return AppStringConstants.workerType;
      } else {
        return await usersService.employerOrWorker();
      }
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  @override
  Future<void> signOut() async {
    await authService.signOut();
  }

  @override
  Future<String> getUserId() async {
    var id = await authService.getFirebaseUserId();
    return id;
  }

  // 1. sign in as guest
  /// If fails, throws [Failure]
  Future<void> signInAsGuest() async {
    try {
      await authService.signInAsGuest();
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 2. link with email
  /// If fails, throws [Failure]
  Future<void> linkWithEmail({
    @required String email,
    @required String password,
  }) async {
    try {
      await authService.linkAccountWithEmail(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 3. link with google
  // has 3 responsibilities 1.linkWithGoogle 2.createProfile 3.attachInUsers
  /// If fails, throws [Failure]
  Future<void> linkWithGoogle() async {
    try {
      AuthResult authResult = await authService.linkAccountWithGoogle();
      WorkerModel workerModel = WorkerModel(
        email: authResult.user.email,
        name: authResult.user.displayName,
      );
      await workerService.createAnWorker(workerModel);
      await workerService.attachInUsers();
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // link with facebook
  Future<void> linkWithFacebook() async {
    try {
      AuthResult authResult = await authService.linkAccountWithFacebook();
      WorkerModel workerModel = WorkerModel(
        email: authResult.user.email,
        name: authResult.user.displayName,
      );
      await workerService.createAnWorker(workerModel);
      await workerService.attachInUsers();
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  // 4. add a worker profile
  Future<void> addWorker(WorkerModel workerModel) async {
    await workerService.createAnWorker(workerModel);
  }

  // 5. attach worker in users collection
  Future<void> addInUsers() async {
    await workerService.attachInUsers();
  }

  // 6. read worker profile
  Stream<WorkerModel> getWorkerProfile(String userId) {
    return workerService.readWorkerProfilestream(userId);
  }

  // 7. update worker profile
  Future<void> updateWorkerProfile(WorkerModel workerModel) async {
    await workerService.updateProfile(workerModel);
  }

  // 8. upload profile photo and get download link
  Future<String> uploadProfilePhoto(File image) async {
    return await usersService.uploadProfileImageToDatabase(image);
  }
  
}
