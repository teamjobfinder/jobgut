import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

class EmployerRepository {
  var employersCollection = Firestore.instance.collection("employers");
  var workersCollection = Firestore.instance.collection("workers");

  final GoogleSignIn googleSignIn = GoogleSignIn();

  FirebaseAuth firebaseAuth;

  EmployerRepository() {
    firebaseAuth = FirebaseAuth.instance;
  }

  // 1.
  Future<FirebaseUser> signUpEmployerWithEmailPass(
      String email, String pass) async {
    try {
      var authResult = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: pass,
      );
      return authResult.user;
    } on PlatformException catch (_) {
      String authError = "";
      throw Exception(authError);
    }
  }

  // 2. sign in with email and password
  Future<FirebaseUser> signInEmployer(String email, String password) async {
    try {
      var authresult = await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return authresult.user;
    } on PlatformException catch (_) {
      String authError = "";
      throw Exception(authError);
    }
  }

  // 3. sign out
  Future<void> signOutEmployer() async {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
  }

  // 4. check signIn
  Future<bool> isEmployerSignedIn() async {
    var currentUser = await firebaseAuth.currentUser();
    return currentUser != null;
  }

  // 5. get current user
  Future<FirebaseUser> getCurrentEmployerId() async {
    return await FirebaseAuth.instance.currentUser();
  }

  // 6. add an employer to employers collection user to users collection
  Future<void> addAnEmployer(EmployerModel employerModel) async {
    FirebaseUser user = await getCurrentEmployerId();
    await Firestore.instance.collection("employers").document(user.uid).setData(
      {"Profile": employerModel.toMap()},
    );
    await Firestore.instance
        .collection("users")
        .document(user.uid)
        .setData({"type": "Employer"});
  }

  // 7.
  Future<CollectionReference> getPostsRef() async {
    FirebaseUser user = await getCurrentEmployerId();
    var ref = Firestore.instance
        .collection("employers")
        .document(user.uid)
        .collection("posts");
    return ref;
  }

  // 8.
  Future<void> postAJob(JobPostModel jobPostModel) async {
    await Firestore.instance.runTransaction((transaction) async {
      await getPostsRef().then((postsRef) {
        postsRef.add(jobPostModel.toMap());
      }).catchError((error) {
        print("Error while saving posts : ${error.toString()}");
      });
    });
  }

  Stream<List<JobPostModel>> getAllJobPosts(String userId) {
    CollectionReference postsCollection =
        employersCollection.document(userId).collection("posts");
    return postsCollection.snapshots().map((snapshot) {
      return snapshot.documents
          .map((doc) => JobPostModel.fromSnapshot(doc))
          .toList();
    });
  }

  Future<EmployerModel> getCurrentEmployer() async {
    var user = await FirebaseAuth.instance.currentUser();
    var employerSnap = await employersCollection.document(user.uid).get();
    var map = Map<String, dynamic>.from(employerSnap["Profile"]);
    var employerModel = EmployerModel.fromMap(map);
    return employerModel;
  }

  // 10. check the mobile verification of employer
  Future<bool> isEmployerVerified() async {
    var user = await FirebaseAuth.instance.currentUser();
    var employerSnap = await employersCollection.document(user.uid).get();
    var map = Map<String, dynamic>.from(employerSnap["Profile"]);
    var employerModel = EmployerModel.fromMap(map);
    return employerModel.isVerified;
  }

  // 11.
  Future<void> deleteAPost(DocumentReference documentReference) async {
    await Firestore.instance.runTransaction((transaction) async {
      transaction.delete(documentReference).then((_) {
        print("Successfullydeleted");
      }).catchError((e) {
        print("Error while deleting ${e.toString()}");
      });
    });
  }

  // 12.
  Future<void> updateAJobPost(
      DocumentReference documentReference, JobPostModel jobPostModel) async {
    await Firestore.instance.runTransaction((transaction) async {
      transaction.update(documentReference, jobPostModel.toMap()).then((_) {
        print("Successfully updated");
      }).catchError((e) {
        print("Error while updating : ${e.toString()}");
      });
    });
  }

  // 13. update employer profile
  Future<void> updateEmployerProfile(EmployerModel employerModel) async {
    var user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance.collection("employers").document(user.uid).setData(
      {"Profile": employerModel.toMap()},
    ).then((_) {
      print("Profile update successful");
    }).catchError((e) {
      print("Error while updating profile : ${e.toString()}");
    });
  }

  // 14. get list of applications
  Future<List<WorkerModel>> getListOfApplications(
      List<String> workerIds) async {
    List<WorkerModel> list = List<WorkerModel>();

    for (var wid in workerIds) {
      DocumentSnapshot documentSnapshot =
          await workersCollection.document(wid).get();
      var map = Map<String, dynamic>.from(documentSnapshot["Profile"]);
      var workerModel = WorkerModel.fromMap(map);
      print("now : ${workerModel.email}");
      list.add(workerModel);
    }

    return list;
  }

  // 15. upload job image
  Future<String> uploadJobImage(File image) async {
    var user = await FirebaseAuth.instance.currentUser();
    var uid = user.uid;
    Uuid uuid = Uuid();
    var imageId = uuid.v1();
    StorageReference reference =
        FirebaseStorage.instance.ref().child("images/jobImages/$uid/$imageId");
    StorageUploadTask uploadTask = reference.putFile(image);
    var downLoadUri = await (await uploadTask.onComplete).ref.getDownloadURL();
    return downLoadUri.toString();
  }
}
