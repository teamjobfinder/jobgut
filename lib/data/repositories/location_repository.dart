import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/data/services/location_service_two.dart';

class LocationRepository {
  LocationServiceTwo locationService = LocationServiceTwo();

  Future<TheLocation> getCurrentLocation() async {
    return await locationService.readCurrentLocation();
  }

  Future<Placemark> searchLocation(
      String text, GoogleMapController googleMapController) async {
    return await locationService.searchALocation(text, googleMapController);
  }

  /// checks device gps status
  /// returns true if GPS on, else for
  Stream<bool> checkGps() {
    return locationService.gpsStatusStream();
  }
}
