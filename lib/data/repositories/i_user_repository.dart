import 'package:meta/meta.dart';

abstract class IUserRepository {
  Future<void> registerWithEmailpassword({
    @required String email,
    @required String password,
  });
  Future<void> signInWithEmailpassword({
    @required String email,
    @required String password,
  });

  /// Returns user type
  Future<String> signInWithGoogle();
  Future<bool> isSignedIn();
  Future<void> signOut();
  Future<bool> isGuest();

  /// Returns User Type
  Future<String> signInWithFacebook();
  Future<String> getUserId();
}
