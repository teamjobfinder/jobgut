import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/i_user_repository.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/employer_service.dart';
import 'package:jobs/data/services/location_service_two.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/utils/failure.dart';
import 'package:meta/meta.dart';

/// Potential errors are handled
class EmployersRepository implements IUserRepository {
  static const String TAG = "EMPLOYER_REPOSITORY";

  AuthService authService = AuthService();
  EmployerService employerService = EmployerService();
  LocationServiceTwo locationService = LocationServiceTwo();
  UsersService usersService = UsersService();

  @override
  Future<bool> isGuest() async {
    return await authService.isGuest();
  }

  @override
  Future<bool> isSignedIn() async {
    return await authService.isSignedIn();
  }

  /// If fails, throws [Failure]
  @override
  Future<void> registerWithEmailpassword({
    @required String email,
    @required String password,
  }) async {
    try {
      await authService.registerWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      // String authError = Helper.convertErrorCodesToMessage(e.code);
      // print("$TAG registerWithEmailpassword error : $authError");
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  @override
  Future<void> signInWithEmailpassword({
    @required String email,
    @required String password,
  }) async {
    try {
      await authService.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  /// has 4 responsibilities 1.signInWithGoogle 2. isNewUser 3.createProfile 4.attachInUsers
  /// for old users checks 5. employerOrWorker()
  /// returns user type
  @override
  Future<String> signInWithGoogle() async {
    try {
      AuthResult authResult = await authService.signInWithGoogle();
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        EmployerModel employerModel = EmployerModel(
          email: authResult.user.email,
          name: authResult.user.displayName,
          isVerified: false,
        );
        await employerService.createProfile(employerModel);
        await employerService.attachInUsers();
        return AppStringConstants.employerType;
      } else {
        return await usersService.employerOrWorker();
      }
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  /// If fails, throws [Failure]
  /// has 4 responsibilities 1.signInWithFacebook 2. isNewUser 3.createProfile 4.attachInUsers
  /// for old users checks 5. employerOrWorker()
  /// returns user type
  @override
  Future<String> signInWithFacebook() async {
    try {
      AuthResult authResult = await authService.signInWithFacebook();
      bool isNewUser = authService.isNewUser(authResult);
      if (isNewUser) {
        EmployerModel employerModel = EmployerModel(
          email: authResult.user.email,
          name: authResult.user.displayName,
          isVerified: false,
        );
        await employerService.createProfile(employerModel);
        await employerService.attachInUsers();
        return AppStringConstants.employerType;
      } else {
        return await usersService.employerOrWorker();
      }
    } on PlatformException catch (e) {
      throw Exception(e.message);
    } on SocketException catch (_) {
      throw Exception(AppStrings.noInternetConnection);
    }
  }

  @override
  Future<void> signOut() async {
    await authService.signOut();
  }

  @override
  Future<String> getUserId() async {
    String id = await authService.getFirebaseUserId();
    return id;
  }

  // 1. link employer with phone number
  Future<void> linkWithPhone(String phoneNumber) async {
    await authService.linkCredentialWithPhone(phoneNumber);
  }

  // 2. link employer with phone using OTP
  Future<void> linkWithPhoneUsingOTP(
      String smsCode, String verId, String phoneNumber) async {
    await authService.linkCredentialWithOTP(smsCode, verId, phoneNumber);
  }

  // 3. upload profile photo and get download url
  Future<String> uploadProfilePhoto(File image) async {
    return await usersService.uploadProfileImageToDatabase(image);
  }

  // 4. create profile
  Future<void> addEmployer(EmployerModel employerModel) async {
    await employerService.createProfile(employerModel);
  }

  // 5. get profile as stream
  Stream<EmployerModel> getEmployer(String userId) {
    return employerService.readProfileSTream(userId);
  }

  // get profile as future
  Future<EmployerModel> getEmployerAsFuture() async {
    return await employerService.readEmployerProfile();
  }

  // get employer of a post
  Future<EmployerModel> getEmployerOfAPost(DocumentSnapshot documentSnapshot) async {
    return await employerService.readEmployerOfAPost(documentSnapshot);
  }

  // 6. update profile
  Future<void> updateEmployer(EmployerModel employerModel) async {
    await employerService.updateProfile(employerModel);
  }

  // 7. upload job photo and get download url
  Future<String> uploadJobPhoto(File image) async {
    return await employerService.createJobImage(image);
  }

  // 8. get list of applicants
  Future<List<WorkerModel>> getApplicants(List<String> workerIds) async {
    return await employerService.readListOfApplicants(workerIds);
  }

  Stream<List<String>> getListOfApplicantsId(
      DocumentReference documentReference) {
    return employerService.readStreamOfApplicantsId(documentReference);
  }

  // 9. attach in users collection
  Future<void> addInUsers() async {
    await employerService.attachInUsers();
  }

}
