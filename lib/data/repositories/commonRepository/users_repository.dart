import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/res/user_type.dart';

class UsersRepository {
  final GoogleSignIn googleSignIn = GoogleSignIn();

  // check user type. e.g employer/ worker
  Future<String> employerOrWorker() async {
    var user = await FirebaseAuth.instance.currentUser();
    DocumentSnapshot ds =
        await Firestore.instance.collection("users").document(user.uid).get();
    return ds['type'];
  }

  // upload profile image
  Future<String> uploadProfileImageToDatabase(File image) async {
    var user = await FirebaseAuth.instance.currentUser();
    var uid = user.uid;
    StorageReference reference =
        FirebaseStorage.instance.ref().child("images/profilePics/$uid/");
    StorageUploadTask uploadTask = reference.putFile(image);
    var downLoadUri = await (await uploadTask.onComplete).ref.getDownloadURL();
    return downLoadUri.toString();
  }

  // add an employer to workers collection
  Future<void> addGoogleSignedWorker(WorkerModel workerModel) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance.collection("workers").document(user.uid).setData(
      {"Profile": workerModel.toMap()},
    );
    await Firestore.instance
        .collection("users")
        .document(user.uid)
        .setData({"type": "Worker"});
  }

  // add an employer to employers collection user to users collection
  Future<void> addGoogleSignedEmployer(EmployerModel employerModel) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance.collection("employers").document(user.uid).setData(
      {"Profile": employerModel.toMap()},
    );
    await Firestore.instance
        .collection("users")
        .document(user.uid)
        .setData({"type": "Employer"});
  }

  // google sign in
  Future<String> signInUserWithGoogle({@required bool isWorker}) async {
    // 1.
    String userType = "";

    // 2
    try {
      print("G started gSignIN");
      final GoogleSignInAccount googleUser = await googleSignIn.signIn();
      print("G after getting google user ${googleUser.email}");
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      print("G after authing google user ${googleAuth.accessToken}");
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await FirebaseAuth.instance
          .signInWithCredential(credential)
          .then((authresult) async {
        print("google sign in success");

        // 3.
        if (authresult.additionalUserInfo.isNewUser) {
          if (isWorker) {
            WorkerModel workerModel = WorkerModel(
              email: authresult.user.email,
              name: authresult.user.displayName,
            );
            addGoogleSignedWorker(workerModel);
            userType = UserType.worker;
          } else {
            EmployerModel employerModel = EmployerModel(
              email: authresult.user.email,
              name: authresult.user.displayName,
              isVerified: false,
            );
            addGoogleSignedEmployer(employerModel);
            userType = UserType.employer;
          }
        } else {
          userType = await employerOrWorker();
        }
      }).catchError((e) {
        print("Google sign in error : ${e.toString()}");
        Fluttertoast.showToast(
          msg: "Google sign in error : ${e.toString()}",
        );
      });
    } catch (e) {
      Fluttertoast.showToast(
        msg: "error : ${e.toString()}",
      );
    }

    return userType;
  }
}
