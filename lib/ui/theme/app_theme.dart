import 'package:flutter/material.dart';

final lightTheme = ThemeData(
  primaryColor: Colors.blue,
  appBarTheme: AppBarTheme(
    color: Colors.white,
    elevation: 0.3,
    iconTheme: IconThemeData(
      color: Colors.grey,
    ),
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Colors.red),
      headline6: TextStyle(
        color: Colors.blue,
        fontFamily: "RussoOne",
        fontSize: 25.0,
        letterSpacing: 1.3,
      ),
    ),
  ),
  tabBarTheme: TabBarTheme(
    labelPadding: EdgeInsets.symmetric(vertical: 10.0),
    labelColor: Colors.blue,
    labelStyle: TextStyle(
      color: Colors.blue,
      fontSize: 15.0,
      fontWeight: FontWeight.bold,
    ),
    unselectedLabelColor: Colors.grey,
    unselectedLabelStyle: TextStyle(
      color: Colors.grey,
      fontSize: 15.0,
    ),
  ),
);

final darkTheme = ThemeData(
  brightness: Brightness.dark,
  appBarTheme: AppBarTheme(
    elevation: 0.3,
    iconTheme: IconThemeData(
      color: Colors.grey,
    ),
    textTheme: TextTheme(
      bodyText1: TextStyle(color: Colors.red),
      headline6: TextStyle(
        color: Colors.blue,
        fontFamily: "RussoOne",
        fontSize: 25.0,
        letterSpacing: 1.3,
      ),
    ),
  ),
  tabBarTheme: TabBarTheme(
    labelPadding: EdgeInsets.symmetric(vertical: 10.0),
    labelColor: Colors.blue,
    labelStyle: TextStyle(
      color: Colors.blue,
      fontSize: 15.0,
      fontWeight: FontWeight.bold,
    ),
    unselectedLabelColor: Colors.grey,
    unselectedLabelStyle: TextStyle(
      color: Colors.grey,
      fontSize: 15.0,
    ),
  ),
);
