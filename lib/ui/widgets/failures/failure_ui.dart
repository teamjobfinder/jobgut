import 'package:flutter/material.dart';

class FailureUi extends StatelessWidget {
  final String message;

  const FailureUi({Key key, this.message = "Something Went Wrong"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(10.0),
      child: Text(
        message,
        style: const TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
          fontSize: 18.0,
        ),
      ),
    );
  }
}
