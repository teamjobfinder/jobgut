import 'package:flutter/material.dart';

class Bubble extends StatelessWidget {

  final double size ;
  final Color color;

  const Bubble({
    this.size = 50.0,
    this.color = Colors.white38,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
