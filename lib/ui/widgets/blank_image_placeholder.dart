import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

/**
 * const checked
 * theme checked
 */
class BlankImagePlaceHolder extends StatelessWidget {
  String text;
  double width, height;
  Function onClick;

  BlankImagePlaceHolder({
    @required this.text,
    @required this.width,
    @required this.height,
    this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        child: Container(
          height: height,
          width: width,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.grey[300],
          ),
          child: Text(
            text,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        onTap: onClick,
      ),
    );
  }
}
