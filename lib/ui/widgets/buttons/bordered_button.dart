import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class BorderedButton extends StatelessWidget {
  final Widget child;
  final double width;
  final double height;
  final Function onPressed;
  final bool isRounded;
  final Color buttonColor;
  final Color borderColor;

  const BorderedButton({
    Key key,
    @required this.child,
    this.width = double.infinity,
    this.height = 50.0,
    this.onPressed,
    this.isRounded = true,
    @required this.buttonColor,
    @required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      width: width,
      height: height,
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor,
          width: 2.0
        ),
        borderRadius: isRounded ? BorderRadius.circular(30.0) : null,
        color: buttonColor
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onPressed,
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}