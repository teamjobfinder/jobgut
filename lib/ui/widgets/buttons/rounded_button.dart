import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Function onPressed;
  final double height, width;

  /// default height 20 and width 100
  const RoundedButton({
    Key key,
    @required this.text,
    this.textColor,
    this.onPressed,
    this.height = 20.0,
    this.width = 100.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 10.0,
      color: Colors.white,
      onPressed: onPressed,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
      padding: const EdgeInsets.all(0.0),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        width: width,
        height: height,
        alignment: Alignment.center,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: textColor,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
