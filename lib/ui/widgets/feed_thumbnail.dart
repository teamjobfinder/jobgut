import 'package:flutter/material.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * 1. theme checked
 * 2. const checked
 */
class FeedThumbnail extends StatelessWidget {
  final JobPostModel jobPostModel;
  final double subBoxRadius = 10.0;

  const FeedThumbnail({@required this.jobPostModel});

  @override
  Widget build(BuildContext context) {
    var cardWidth = MediaQuery.of(context).size.width - 15.0;
    return Card(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // 1st child : salary, type
            Row(
              children: <Widget>[
                // salary range
                Container(
                  width: 150.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(subBoxRadius),
                  ),
                  child: Text(
                    Helper.isNullOrEmpty(jobPostModel?.salaryRange)
                        ? ""
                        : jobPostModel?.salaryRange,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                // salary type
                Container(
                  margin: const EdgeInsets.only(left: 10.0),
                  width: 90.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius: BorderRadius.circular(subBoxRadius),
                  ),
                  child: Text(
                    Helper.isNullOrEmpty(jobPostModel?.salaryType)
                        ? ""
                        : jobPostModel?.salaryType,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Spacer(),
              ],
            ),
            // date
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                  "Posted on : ${Helper.formatDateTime(jobPostModel.date)}"),
            ),
            // employer name and logo
            Container(
              child: Row(
                children: <Widget>[
                  buildPicHolder(
                    url: jobPostModel?.companyLogoUrl,
                    radius: 60.0,
                    isWorker: true,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10.0),
                    width: cardWidth * 0.7,
                    child: Text(
                      jobPostModel?.companyName.toUpperCase(),
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17.0,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
            // image
            Helper.isNullOrEmpty(jobPostModel.jobImageUrl) != true
                ? Container(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    alignment: Alignment.center,
                    child: Image.network(
                      jobPostModel?.jobImageUrl,
                      width: cardWidth,
                      height: 200.0,
                      fit: BoxFit.cover,
                    ),
                  )
                : const SizedBox(height: 0.0),
            // title
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                jobPostModel?.title.toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  color: Colors.blue,
                ),
              ),
            ),
            // description
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                jobPostModel.jobDescription.length > 70
                    ? "${jobPostModel?.jobDescription.substring(0, 69)} ..."
                    : jobPostModel.jobDescription,
                style: const TextStyle(fontSize: 17.0),
              ),
            ),
            // last child
            Row(
              children: <Widget>[
                // location
                Row(
                  children: <Widget>[
                    const Icon(Icons.place),
                    Container(
                      width: cardWidth * 0.6,
                      child: Text(
                        "${jobPostModel?.locality}, ${jobPostModel?.administrativeArea} , ${jobPostModel?.country}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: const TextStyle(fontSize: 11.0),
                      ),
                    ),
                  ],
                ),
                // job type
                Container(
                  margin: const EdgeInsets.only(left: 10.0),
                  width: 90.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                      color: setColor(jobPostModel.jobType),
                      borderRadius: BorderRadius.circular(subBoxRadius)),
                  child: Text(
                    jobPostModel?.jobType,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Color setColor(String jobType) {
    if (jobType == AppStrings.fullTime) {
      return AppColors.fullTimeJob;
    } else if (jobType == AppStrings.partTime) {
      return AppColors.partTimeJob;
    }
    return AppColors.freelanceJob;
  }
}
