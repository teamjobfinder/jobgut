import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class CenterError extends StatelessWidget {
  final String message;

  const CenterError({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        message,
        style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
