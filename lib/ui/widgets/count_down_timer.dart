import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

class CountDownTimer extends StatelessWidget {

  int seconds;

  CountDownTimer({@required this.seconds});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: SlideCountdownClock(
        
        duration: Duration(seconds: seconds),
        slideDirection: SlideDirection.Up,
        separator: "-",
        textStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
        separatorTextStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.blue,
        ),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.blue, shape: BoxShape.circle),
      ),
    );
  }
}
