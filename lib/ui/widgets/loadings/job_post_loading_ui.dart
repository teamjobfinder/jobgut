import 'package:flutter/material.dart';
import 'package:jobs/ui/widgets/loadings/feed_loading.dart';

class JobPostLoadingUi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FeedLoading(),
    );
  }
}