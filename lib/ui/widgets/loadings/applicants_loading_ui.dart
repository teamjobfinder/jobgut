import 'package:flutter/material.dart';
import 'package:jobs/ui/widgets/buttons/rounded_button.dart';
import 'package:shimmer/shimmer.dart';

class ApplicantsLoadingUi extends StatelessWidget {

  const ApplicantsLoadingUi();

  @override
  Widget build(BuildContext context) {
    double scrWidth, scrHeight;
    scrWidth = MediaQuery.of(context).size.width;
    scrHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: 10,
          itemBuilder: (context, pos) {
            return Shimmer.fromColors(
              highlightColor: Colors.white,
              baseColor: Colors.grey[300],
              child: _loadingLayout(scrWidth, scrHeight),
            );
          },
        ),
        Positioned(
          bottom: 10.0,
          child: Container(
            width: scrWidth,
            alignment: Alignment.center,
            child: RoundedButton(
              height: 50.0,
              width: scrWidth * 0.4,
              text: "Loading...",
              textColor: Colors.grey[400],
            ),
          ),
        ),
      ],
    );
  }

  Widget _loadingLayout(double scrWidth, double scrHeight) {
    return Container(
      margin: const EdgeInsets.all(3.0),
      width: scrWidth,
      height: scrHeight * 0.3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // 1st child
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.all(10.0),
                width: 40.0,
                height: 40.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey,
                ),
              ),
              Container(
                height: 35.0,
                width: 200.0,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 227, 224, 224),
                  borderRadius: BorderRadius.circular(80.0),
                ),
              ),
            ],
          ),
          // 2nd child
          Container(
            margin: const EdgeInsets.all(10.0),
            height: 20.0,
            width: 300.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 227, 224, 224),
              borderRadius: BorderRadius.circular(80.0),
            ),
          ),
          // 3rd child
          Container(
            margin: const EdgeInsets.all(10.0),
            height: 20.0,
            width: 200.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 227, 224, 224),
              borderRadius: BorderRadius.circular(80.0),
            ),
          ),
        ],
      ),
    );
  }
}
