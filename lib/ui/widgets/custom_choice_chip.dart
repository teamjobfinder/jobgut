import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

class CustomChoiceChip extends StatelessWidget {
  final String defTitle;
  final String title;
  final IconData iconData;
  final ValueChanged<bool> onSelected;
  final bool isLarge;

  const CustomChoiceChip({
    Key key,
    @required this.defTitle,
    @required this.title,
    @required this.iconData,
    @required this.onSelected,
    this.isLarge = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _chipSize = isLarge
        ? Helper.screenWidthPortion(context, time: 0.3)
        : Helper.screenWidthPortion(context, time: 0.2);
    return Container(
      width: _chipSize,
      height: _chipSize,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: ChoiceChip(
        label: Container(
          padding: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: FaIcon(
                    iconData,
                    size: _chipSize * 0.3,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 3.0),
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17.0,
                  ),
                ),
              ),
            ],
          ),
        ),
        padding: EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(),
        selectedColor: Colors.green,
        selected: defTitle == title,
        onSelected: onSelected,
      ),
    );
  }
}
