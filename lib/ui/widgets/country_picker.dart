import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/// used for picking country code
class CountryPicker extends StatelessWidget {
  String initialCountryCode;
  Function(CountryCode country) onChanged;

  CountryPicker({@required this.initialCountryCode, @required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return CountryCodePicker(
      initialSelection:
          Helper.isNullOrEmpty(initialCountryCode) ? "+82" : initialCountryCode,
      favorite: ['+82'],
      showCountryOnly: false,
      showOnlyCountryWhenClosed: false,
      alignLeft: false,
      onChanged: (c) {
        return onChanged(c);
      },
    );
  }
}
