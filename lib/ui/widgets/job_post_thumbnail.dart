import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/ui/pages/employer/edit_job_post_page.dart';
import 'package:jobs/ui/pages/employer/postForm/form_part_one.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * const checked
 */
class JobPostThumbnail extends StatelessWidget {
  JobPostModel jobPostModel;
  DocumentReference reference;
  double subBoxRadius = 10.0;
  Function onDelete;
  Function onShare;

  JobPostThumbnail({
    @required this.jobPostModel,
    @required this.reference,
    @required this.onDelete,
    @required this.onShare,
  });

  @override
  Widget build(BuildContext context) {
    var cardWidth = MediaQuery.of(context).size.width - 15.0;

    return Card(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // 1st child : salary, type
            Row(
              children: <Widget>[
                // salary range
                Container(
                  width: 150.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(subBoxRadius),
                  ),
                  child: Text(
                    jobPostModel.salaryRange,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                // salary type
                Container(
                  margin: const EdgeInsets.only(left: 10.0),
                  width: 90.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius: BorderRadius.circular(subBoxRadius),
                  ),
                  child: Text(
                    jobPostModel.salaryType,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Spacer(),
                popupMenuButton(context),
              ],
            ),
            // date
            Container(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text(
                  "Posted on : ${Helper.formatDateTime(jobPostModel.date)}"),
            ),
            // image
            Helper.isNullOrEmpty(jobPostModel.jobImageUrl) != true
                ? Container(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    width: cardWidth,
                    height: 200.0,
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(3.0),
                      color: const Color(0xff7c94b6),
                      image: new DecorationImage(
                        image: new NetworkImage(
                          jobPostModel.jobImageUrl,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                : Container(),
            // title
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                jobPostModel.title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                  color: Colors.blue,
                ),
              ),
            ),
            // description
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                jobPostModel.jobDescription.length > 70
                    ? "${jobPostModel.jobDescription.substring(0, 69)} ..."
                    : jobPostModel.jobDescription,
                style: const TextStyle(fontSize: 17.0),
              ),
            ),
            // last child
            Row(
              children: <Widget>[
                // location
                Row(
                  children: <Widget>[
                    const Icon(Icons.place),
                    Container(
                      width: cardWidth * 0.6,
                      child: Text(
                        "${jobPostModel.locality}, ${jobPostModel.administrativeArea} , ${jobPostModel.country}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: const TextStyle(fontSize: 11.0),
                      ),
                    ),
                  ],
                ),
                // job type
                Container(
                  margin: const EdgeInsets.only(left: 10.0),
                  width: 90.0,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    color: setColor(jobPostModel.jobType),
                    borderRadius: BorderRadius.circular(subBoxRadius),
                  ),
                  child: Text(
                    jobPostModel.jobType,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Color setColor(String jobType) {
    if (jobType == AppStrings.fullTime) {
      return AppColors.fullTimeJob;
    } else if (jobType == AppStrings.partTime) {
      return AppColors.partTimeJob;
    }
    return AppColors.freelanceJob;
  }

  Widget popupMenuButton(BuildContext ctx) {
    return PopupMenuButton<String>(
      onSelected: onChoices,
      itemBuilder: (ctx) {
        return AppStrings.ovMenuChoices.map((choice) {
          return PopupMenuItem(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
    );
  }

  void onChoices(String choice) {
    if (choice == AppStrings.ovMenuDelete) {
      onDelete();
    } else if (choice == AppStrings.ovMenuEdit) {
      Get.to(
        FormPartOne(
          documentReference: reference,
          jobPostModel: jobPostModel,
          isUpdating: true,
        ),
      );
    } else if (choice == AppStrings.ovMenuShare) {
      onShare();
      // String textToShare =
      //     "${AppConfig.appName}\n ${jobPostModel.title} - ${jobPostModel.jobDescription}\n Company - ${jobPostModel.companyName}";
      // String finalText =
      //     "$textToShare\n Download app from Google Play Store : ${AppStrings.goolePlayAppLink}";
      // Share.share(finalText);
    }
  }
}
