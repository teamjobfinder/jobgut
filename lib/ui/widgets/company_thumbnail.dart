import 'dart:math';
import 'package:flutter/material.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:meta/meta.dart';

class CompanyThumbnail extends StatelessWidget {
  String city, country, companyName;
  int posts;

  var colorIndex = Random().nextInt(AppColors.randomColors.length);

  CompanyThumbnail({
    this.city,
    this.country,
    @required this.companyName,
    this.posts,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.0),
      width: 150.0,
      decoration: BoxDecoration(
        color: AppColors.randomColors[colorIndex],
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            child: Text(
              "$city, $country",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Spacer(),
          Container(
            child: Text(
              companyName,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 20.0),
            child: Text(
              "$posts posts",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
