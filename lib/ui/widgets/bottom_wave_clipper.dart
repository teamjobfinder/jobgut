import 'package:flutter/material.dart';

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0.0, size.height - 20);

    var fcp = new Offset(size.width / 4, size.height); // first control point
    var fep = new Offset(size.width / 2.25, size.height - 30.0); // first end point
    path.quadraticBezierTo(fcp.dx, fcp.dy, fep.dx, fep.dy);

    var scp = Offset(size.width - (size.width / 3.25), size.height - 65); // second control point
    var sep = Offset(size.width, size.height - 40); // first end point
    path.quadraticBezierTo(scp.dx, scp.dy, sep.dx, sep.dy);

    path.lineTo(size.width, size.height - 20);

    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }
}