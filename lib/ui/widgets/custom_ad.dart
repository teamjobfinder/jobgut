// import 'package:flutter/material.dart';
// import 'package:flutter_native_admob/flutter_native_admob.dart';
// import 'package:flutter_native_admob/native_admob_controller.dart';
// import 'package:jobs/res/strings/ad_strings.dart';
// import 'package:meta/meta.dart';

// class CustomAd extends StatelessWidget {
//   double adHeight;
//   NativeAdmobController nativeAdmobController;

//   CustomAd({@required this.adHeight, this.nativeAdmobController});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       height: adHeight,
//       padding: EdgeInsets.all(10),
//       child: NativeAdmob(
//         adUnitID: AdStrings.admobNativeAdId,
//         controller: nativeAdmobController,
//         loading: const SizedBox(height: 0.0),
//       ),
//     );
//   }
// }
