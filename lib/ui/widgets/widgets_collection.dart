import 'package:flutter/material.dart';
import 'package:jobs/res/app_assets.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:meta/meta.dart';

// circular profile pic
Widget buildPicHolder(
    {@required String url, @required double radius,@required bool isWorker}) {
  return ClipOval(
    child: url != null
        ? Image.network(
            url,
            width: radius,
            height: radius,
            fit: BoxFit.cover,
          )
        : Image.asset(
            isWorker
                ? AppAssets.workerPlaceHolderImage
                : AppAssets.employerPlaceHolderImage,
            height: radius,
            width: radius,
            fit: BoxFit.cover,
          ),
  );
}

Widget buildLoadingIndicator() {
  return const Center(
    child: const CircularProgressIndicator(),
  );
}

Widget buildErrorUi(String message) {
  return Center(
    child: Text(
      message,
      style: const TextStyle(
        color: Colors.red,
      ),
    ),
  );
}

Widget buildHeaderTextUi(String text) {
  return Align(
    alignment: Alignment.centerLeft,
    child: Container(
      padding: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
      alignment: Alignment.centerLeft,
      width: 120.0,
      margin: EdgeInsets.only(bottom: 5.0),
      color: Color(0xffc5cad3),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 15.0,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    ),
  );
}

final kInnerDecoration = BoxDecoration(
  color: Colors.white,
  border: Border.all(color: Colors.white),
  borderRadius: BorderRadius.circular(10),
);

final kGradientBoxDecoration = BoxDecoration(
  gradient: AppColors.blueWhiteGradient,
  borderRadius: BorderRadius.circular(10),
);

Widget buildTextField(
  TextEditingController controller,
  String text, {
  int minLines,
  int maxLines,
  @required bool isValidationRequired,
}) {
  return Container(
    padding: const EdgeInsets.all(2.0),
    decoration: kGradientBoxDecoration,
    margin: EdgeInsets.only(bottom: 20.0),
    child: Container(
      decoration: kInnerDecoration,
      child: TextFormField(
        autofocus: false,
        controller: controller,
        decoration: textFieldDecoration(text),
        style: TextStyle(color: Colors.black),
        minLines: minLines,
        maxLines: maxLines,
        validator: isValidationRequired
            ? (_) {
                if (controller.text.isEmpty) {
                  return "This field is required";
                }
              }
            : null,
      ),
    ),
  );
}

// decoration for textfields
InputDecoration textFieldDecoration(String hintText) {
  return InputDecoration(
    contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
    labelText: hintText,
    hintText: hintText,
    hintStyle: TextStyle(color: Colors.black),
    border: InputBorder.none,
  );
}


