import 'package:flutter/material.dart';
import 'package:jobs/notifiers/theme_notifier.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/employer/phone_verify_page.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:jobs/utils/helper.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

/**
 * const checked
 */
class EmployerDrawer extends StatefulWidget {
  const EmployerDrawer();

  @override
  _EmployerDrawerState createState() => _EmployerDrawerState();
}

class _EmployerDrawerState extends State<EmployerDrawer> {
  final String _TAG = "_EmployerDrawerState";
  var scrHeight, scrWidth;

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    scrHeight = MediaQuery.of(context).size.height;
    scrWidth = MediaQuery.of(context).size.width;
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return SizedBox(
      width: 300,
      height: double.infinity,
      child: Material(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
          child: Theme(
            data: ThemeData(brightness: Brightness.dark),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  height: scrHeight * 0.3,
                  alignment: Alignment.center,
                  child: RichText(
                    text: const TextSpan(
                        text: "${AppConfig.appName}\n",
                        style: const TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          letterSpacing: 2.5,
                          fontFamily: "RussoOne",
                        ),
                        children: <TextSpan>[
                          const TextSpan(
                            text: AppConfig.appVersion,
                            style: const TextStyle(fontSize: 17),
                          ),
                        ]),
                  ),
                ),

                // drawer items

                // phone verify
                const Divider(height: 2.0, color: Colors.white70),
                ListTile(
                  leading: const Icon(Icons.phone_android),
                  title: const Text('Verify Phone Number'),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const PhoneVerifyPage()));
                  },
                ),
                const Divider(height: 2.0, color: Colors.white70),
                // share app
                ListTile(
                  leading: const Icon(Icons.share),
                  title: const Text("Share App"),
                  onTap: () {
                    String title = "Download ${AppConfig.appName}";
                    String text = "$title\n ${AppConfig.playStoreLink}";
                    Share.share(text);
                  },
                ),
                const Divider(height: 2.0, color: Colors.white70),
                // rate app
                ListTile(
                  leading: const Icon(Icons.star_border),
                  title: const Text("Rate This App"),
                  onTap: () async {
                    String url = AppConfig.playStoreLink;
                    if (await canLaunch(url)) {
                      await launch(url);
                    }
                  },
                ),
                const Divider(height: 2.0, color: Colors.white70),
                // switch theme
                Consumer<ThemeNotifier>(
                  builder: (context, themeNotifier, child) {
                    return SwitchListTile(
                      secondary: const Icon(Icons.lightbulb_outline),
                      title: Text("Dark Mode"),
                      onChanged: (val) {
                        themeNotifier.switchTheme();
                      },
                      value: themeNotifier.getTheme,
                    );
                  },
                ),
                const Divider(height: 2.0, color: Colors.white70),
                // about app
                ListTile(
                  leading: const Icon(Icons.info),
                  title: const Text("About"),
                  onTap: () {
                    showAboutDialog(
                      context: context,
                      applicationName: AppStrings.appName,
                      applicationVersion: "Version : ${AppConfig.appVersion}",
                      children: [
                        Text(
                          "Simple App For Searching Jobs and For Hiring Employee",
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
