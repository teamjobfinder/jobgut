import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/employerBloc/employerProfilePageBloc/employer_profile_page_bloc.dart';
import 'package:jobs/blocs/employerBloc/employerProfilePageBloc/employer_profile_page_event.dart';
import 'package:jobs/blocs/employerBloc/employerProfilePageBloc/employer_profile_page_state.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/employer/edit_employer_profile.dart';
import 'package:jobs/ui/temp/job_map.dart';
import 'package:jobs/ui/widgets/empty_appbar.dart';
import 'package:jobs/ui/widgets/glowing_stars.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/dialog_helper.dart';
import 'package:jobs/utils/helper.dart';
import 'package:url_launcher/url_launcher.dart';

/**
 * const checked
 * theme checked
 */

class EmployerProfilePageParent extends StatelessWidget {
  const EmployerProfilePageParent();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EmployerProfilePageBloc>(
      create: (context) => EmployerProfilePageBloc(),
      child: const EmployerProfilePage(),
    );
  }
}

class EmployerProfilePage extends StatefulWidget {
  const EmployerProfilePage();

  @override
  _EmployerProfilePageState createState() => _EmployerProfilePageState();
}

class _EmployerProfilePageState extends State<EmployerProfilePage> {
  final String _TAG = "_EmployerProfilePageState";

  var scrHeight, scrWidth;
  Color secondaryColor = Colors.white70;
  EmployerProfilePageBloc employerProfilePageBloc;
  EmployerRepository employerRepository = EmployerRepository();

  @override
  void initState() {
    super.initState();
    employerProfilePageBloc = BlocProvider.of<EmployerProfilePageBloc>(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.blue, //or set color with: Color(0xFF0000FF)
    ));
    employerProfilePageBloc.add(GetEmployerProfile());
  }

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    scrWidth = MediaQuery.of(context).size.width;
    scrHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: EmptyAppBar(),
      body: BlocListener<EmployerProfilePageBloc, EmployerProfilePageState>(
        listener: (context, state) {
          if (state is EmployerLoggedOut) {
            Get.to(EmployeeEmployerPage());
          }
        },
        child: BlocBuilder<EmployerProfilePageBloc, EmployerProfilePageState>(
          builder: (context, state) {
            if (state is EmployerProfilePageInitial) {
              return buildLoadingIndicator();
            } else if (state is EmployerProfilePageLoading) {
              return buildLoadingIndicator();
            } else if (state is EmployerProfilePageLoaded) {
              return _EmployerProfileUi(
                scrHeight: scrHeight,
                scrWidth: scrWidth,
                model: state.employerModel,
                employerRepository: employerRepository,
              );
            } else if (state is EmployerProfilePageError) {
              return buildErrorUi(state.message);
            } else if (state is EmployerLoggedOut) {
              return const SizedBox(height: 0.0);
            }
          },
        ),
      ),
    );
  }
}

class _EmployerProfileUi extends StatelessWidget {
  final EmployerModel model;
  final double scrHeight;
  final double scrWidth;
  final EmployerRepository employerRepository;

  const _EmployerProfileUi({
    Key key,
    @required this.model,
    @required this.scrHeight,
    @required this.scrWidth,
    @required this.employerRepository,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        // header
        Container(
          height: scrHeight * 0.45,
          child: Stack(
            children: <Widget>[
              // header bg
              Container(
                height: scrHeight * 0.35,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [Colors.blue[800], Colors.blue[400]],
                  ),
                ),
              ),
              // name
              Positioned(
                top: scrHeight / 13.0,
                right: 0.0,
                left: 0.0,
                child: Align(
                  child: Text(
                    model.name,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                    ),
                  ),
                ),
              ),
              // email
              Positioned(
                top: scrHeight / 8.5,
                right: 0.0,
                left: 0.0,
                child: Align(
                  child: Text(
                    model.email == null ? "" : model.email,
                    style: TextStyle(
                      color: Colors.white70,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ),
              // location
              Positioned(
                top: scrHeight / 6.5,
                right: 0.0,
                left: 0.0,
                child: Align(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.place, color: Colors.white70),
                      InkWell(
                        onTap: Helper.isNullOrEmpty(model.latitude)
                            ? () {}
                            : () {
                                Get.to(JobMap(
                                  employerModel: model,
                                ));
                              },
                        child: Container(
                          width: scrWidth * 0.7,
                          child: Text(
                            model.location != null ? model.location : "N/A",
                            style: TextStyle(
                              color: Colors.white70,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // logo
              Positioned(
                right: 0.0,
                left: 0.0,
                top: scrHeight * 0.23,
                child: Align(
                  child: AvatarGlow(
                    endRadius: 80.0,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          width: 2.0,
                        ),
                        shape: BoxShape.circle,
                      ),
                      child: buildPicHolder(
                        url: model.logoUrl,
                        radius: 100.0,
                        isWorker: false,
                      ),
                    ),
                  ),
                ),
              ),
              // logout
              Positioned(
                right: 10.0,
                top: 5.0,
                child: InkWell(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white70,
                    ),
                    padding: const EdgeInsets.symmetric(
                      vertical: 7.0,
                      horizontal: 5.0,
                    ),
                    child: const Text(
                      "LOG OUT",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  onTap: () {
                    DialogHelper.showClassicDialog(
                      context,
                      "Want to Log Out?",
                      "You Will be Logged Out",
                      onCancel: () {
                        Navigator.of(context).pop();
                      },
                      onOk: () {
                        BlocProvider.of<EmployerProfilePageBloc>(context)
                            .add(SignOutEmployer());
                      },
                    );
                  },
                ),
              ),
              // edit
              Positioned(
                left: 10.0,
                top: 5.0,
                child: InkWell(
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 3.0, horizontal: 5.0),
                    color: Colors.white70,
                    child: const Icon(Icons.edit, color: Colors.black),
                  ),
                  onTap: () {
                    navigateToEditPage(context, model);
                  },
                ),
              ),
              // stars
              const Positioned(
                bottom: 120.0,
                right: 40.0,
                child: const GlowingStars(),
              ),
              // stars
              const Positioned(
                top: 30.0,
                left: 20.0,
                child: const GlowingStars(),
              ),
            ],
          ),
        ),
        // body
        model.phoneNumber != null
            ? Container(
                padding:
                    const EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: const Icon(
                        Icons.phone,
                        color: Colors.blue,
                      ),
                    ),
                    InkWell(
                      child: Text(
                        model.phoneNumber.toString(),
                        style: TextStyle(
                          color: Colors.blue[300],
                          fontSize: 16.0,
                        ),
                      ),
                      onTap: () async {
                        if (Helper.isNullOrEmpty(
                            model.phoneNumber.toString())) {
                          return;
                        }
                        var number = model.phoneNumber.toString();
                        var url = "tel:$number";
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          print("Could not launch $url");
                        }
                      },
                    ),
                  ],
                ),
              )
            : const SizedBox(height: 0.0),
        Container(
          padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
          child: Text(
            "About ${model.name}",
            style: const TextStyle(fontSize: 18.0),
          ),
        ),
        const Divider(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(model.description != null ? model.description : "N/A"),
        ),
        // website url
        model.websiteUrl != null
            ? Container(
                padding: const EdgeInsets.only(
                    top: 15.0, left: 10.0, right: 10.0, bottom: 20.0),
                child: Row(
                  children: <Widget>[
                    const Icon(
                      Icons.laptop_chromebook,
                      color: Colors.blue,
                    ),
                    const SizedBox(width: 10.0),
                    InkWell(
                      onTap: () async {
                        String link = "https://${model.websiteUrl}";
                        if (await canLaunch(link)) {
                          await launch(link);
                        } else {
                          print("cant launch $link");
                        }
                      },
                      child: Text(
                        model.websiteUrl,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : const SizedBox(height: 0.0),
      ],
    );
  }

  void navigateToEditPage(BuildContext context, EmployerModel model) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EditEmployerProfilePage(employerModel: model);
    }));
  }

  void navigateToHome(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return const EmployeeEmployerPage();
      }),
    );
  }
}
