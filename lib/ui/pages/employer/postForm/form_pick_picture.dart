import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/ui/widgets/buttons/bordered_button.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:meta/meta.dart';

class FormPickPicture extends StatefulWidget {
  final JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  const FormPickPicture({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPickPictureState createState() => _FormPickPictureState();
}

class _FormPickPictureState extends State<FormPickPicture> {
  final String _TAG = "_FormPartFourState";
  double scrWidth, scrHeight;

  double scrPadding = 10.0;

  File _jobPostImage;
  String _jobImageUrl;
  EmployerRepository employerRepository = EmployerRepository();
  EmployerModel employerModel;
  ProgressDialog _progressDialog;

  @override
  void initState() {
    super.initState();
    _progressDialog = ProgressDialog(context);
    _getCurrentEmployer();
  }

  @override
  Widget build(BuildContext context) {
    scrWidth = Helper.getScreenWidth(context);
    scrHeight = Helper.getScreenHeight(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            // bg image
            Container(
              width: scrWidth,
              height: scrHeight,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_image.png"),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            // picture
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: scrPadding,
                vertical: 20.0,
              ),
              height: scrHeight,
              width: scrWidth,
              child: Column(
                children: <Widget>[
                  // text
                  Container(
                    padding: EdgeInsets.only(top: 40.0, bottom: 20.0),
                    alignment: Alignment.center,
                    child: Text(
                      "Almost Done!",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 50.0,
                      ),
                      textScaleFactor: 1.0,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  // add picture
                  Container(
                    color: Colors.white,
                    height: scrHeight * 0.5,
                    width: scrWidth - 30.0,
                    child: _jobPostImage != null
                        ? _buildChangePictureUI()
                        : _buildPickPictureUI(),
                  ),
                  Spacer(),
                  // button
                  _buildButtons(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onAddPhoto() async {
    var image = await Helper.pickImageFromGallery();
    setState(() {
      _jobPostImage = image;
    });
    print("image pickedd");
  }

  Widget _buildPickPictureUI() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: 15.0),
          alignment: Alignment.center,
          child: FaIcon(
            FontAwesomeIcons.peopleArrows,
            color: Colors.blue,
            size: 80.0,
          ),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              _onAddPhoto();
            },
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "+ ADD PICTURE",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
        ),
        Text(
          "Optional Picture Related To The Job",
          style: TextStyle(
            color: Colors.grey,
          ),
        ),
      ],
    );
  }

  Widget _buildChangePictureUI() {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          width: scrWidth,
          height: scrHeight * 0.5,
          child: Image.file(
            _jobPostImage,
            fit: BoxFit.cover,
          ),
          foregroundDecoration: BoxDecoration(
            color: Colors.blue[600].withOpacity(0.1),
          ),
        ),
        Positioned(
          bottom: 30.0,
          right: 20.0,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                _onAddPhoto();
              },
              child: CircleAvatar(
                radius: 30.0,
                backgroundColor: Colors.white,
                child: Icon(Icons.add, color: Colors.blue),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildButtons(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // back btn
        Flexible(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(bottom: 5.0),
            alignment: Alignment.center,
            child: BorderedButton(
              borderColor: Colors.blue,
              buttonColor: Colors.white,
              child: Text(
                "BACK",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
        // continue button
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: GradientButton(
              child: Text(
                "POST",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context),
              gradient: AppColors.blueWhiteGradient,
              onPressed: () {
                _onPostJob();
              },
            ),
          ),
        ),
      ],
    );
  }

  void _onPostJob() async {
    _progressDialog.show();

    var isConnected = await ConnectivityService().hasConnection();
    if (isConnected) {
      _jobImageUrl = _jobPostImage != null
          ? await employerRepository.uploadJobImage(_jobPostImage)
          : "";
      _manipulateJobPostModel();
      Helper.logPrint(
          _TAG, "before posting... : ${widget.jobPostModel.latitude}");

      if (widget.isUpdating) {
        // edit post
        await employerRepository
            .updateAJobPost(widget.documentReference, widget.jobPostModel)
            .then((_) {
          _progressDialog.hide();
          Fluttertoast.showToast(
            msg: "successfully updated the job post",
            backgroundColor: Colors.blue,
          );
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => EmployerHomePage()),
              (Route<dynamic> route) => false);
        }).catchError((e) {
          _progressDialog.hide();
          Fluttertoast.showToast(
            msg: "Error ${e.toString()}",
            backgroundColor: Colors.red,
          );
        });
      } else {
        // new post
        await employerRepository.postAJob(widget.jobPostModel).then((_) {
          _progressDialog.hide();
          Fluttertoast.showToast(
            msg: "successfully posted a job",
            backgroundColor: Colors.blue,
          );
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => EmployerHomePage()),
              (Route<dynamic> route) => false);
        }).catchError((e) {
          _progressDialog.hide();
          Fluttertoast.showToast(
            msg: "Error ${e.toString()}",
            backgroundColor: Colors.red,
          );
        });
      }

      Helper.logPrint(
          _TAG, "after posting... : ${widget.jobPostModel.latitude}");
    } else {
      _progressDialog.hide();
      Fluttertoast.showToast(
        msg: "Unable to Post. Check Network Connection",
      );
    }
    _progressDialog.hide();
  }

  void _getCurrentEmployer() async {
    employerModel = await employerRepository.getCurrentEmployer();
  }

  void _manipulateJobPostModel() {
    widget.jobPostModel?.companyName = employerModel.name;
    widget.jobPostModel?.companyLogoUrl = employerModel.logoUrl;
    widget.jobPostModel?.nationality = employerModel.nationality;
    widget.jobPostModel?.isValid = true;
    widget.jobPostModel?.jobImageUrl = _jobImageUrl;
  }
}
