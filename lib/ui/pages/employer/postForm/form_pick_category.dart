import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/strings/job_categories.dart';
import 'package:jobs/ui/pages/employer/postForm/form_pick_picture.dart';
import 'package:jobs/ui/widgets/buttons/bordered_button.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/ui/widgets/custom_choice_chip.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

class FormPickCategory extends StatefulWidget {
  final JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  const FormPickCategory({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPickCategoryState createState() => _FormPickCategoryState();
}

class _FormPickCategoryState extends State<FormPickCategory> {
  // default job category
  String _jobCategory;

  @override
  void initState() {
    super.initState();
    _jobCategory = Helper.isNullOrEmpty(widget.jobPostModel?.jobCategory)
        ? "Designer"
        : widget.jobPostModel?.jobCategory;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Helper.handleJobPostCloseDialog(context);
          },
        ),
        title: Text("New Post"),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: Helper.screenHeightPortion(context, time: 0.7),
            child: _buildCategoriesChip(true),
          ),
          Spacer(),
          _buildButtons(),
        ],
      ),
    );
  }

  Widget _buildCategoriesChip(bool isLarge) {
    return GridView.count(
      crossAxisCount: 3,
      children: <Widget>[
        CustomChoiceChip(
          title: JobCategories.designer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.borderStyle,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.designer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.uiux,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.uikit,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.uiux;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.engineer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.stackExchange,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.engineer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.doctor,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.hospital,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.doctor;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.chef,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.cookie,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.chef;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.cleaning,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.handsWash,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.cleaning;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.warehouse,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.warehouse,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.warehouse;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.official,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.suitcase,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.official;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.salesAndMarketing,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.salesforce,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.salesAndMarketing;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.driverAndCourier,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.truck,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.driverAndCourier;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.construction,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.building,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.construction;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.teacher,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.chalkboardTeacher,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.teacher;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.lawyer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.male,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.lawyer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.photographer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.camera,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.photographer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.receptionist,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.female,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.receptionist;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.customerSupport,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.phoneAlt,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.customerSupport;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.eventManagement,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.moneyBillWave,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.eventManagement;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.waiter,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.restaurant,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.waiter;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.bartender,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.glasses,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.bartender;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.kitchen_helper,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.kitchen,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.kitchen_helper;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.salesman,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.mail_outline,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.salesman;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.cosmetics,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.party_mode,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.cosmetics;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.hair_designer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.party_mode,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.hair_designer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.barista,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.beer,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.barista;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.language_tutor,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.language,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.language_tutor;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.others,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.devices_other,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.others;
            });
          },
        ),
      ],
    );
  }

  // for buttons
  Widget _buildButtons() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // back btn
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: BorderedButton(
                child: Text(
                  "BACK",
                  style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
                height: 50.0,
                width: Helper.getScreenWidth(context) - 200,
                borderColor: Colors.blue,
                buttonColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ),
          // continue button
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GradientButton(
                child: Text(
                  "CONTINUE",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
                height: 50.0,
                width: Helper.getScreenWidth(context) - 200,
                gradient: AppColors.blueWhiteGradient,
                onPressed: () {
                  _onNavigation();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onNavigation() {
    widget.jobPostModel?.jobCategory = _jobCategory;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => FormPickPicture(
          jobPostModel: widget.jobPostModel,
          documentReference: widget.documentReference,
          isUpdating: widget.isUpdating,
        ),
      ),
    );
  }
}