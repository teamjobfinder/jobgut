import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:intl/intl.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/keys.dart';
import 'package:jobs/ui/pages/employer/postForm/form_pick_category.dart';
import 'package:jobs/ui/temp/temp_map.dart';
import 'package:jobs/ui/widgets/buttons/bordered_button.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/utils/helper.dart';
import 'package:jobs/utils/location_service.dart';
import 'package:meta/meta.dart';

/**
 * 1. date
 * 2. deadline
 * 3. address
 * 4. joblocation
 */
class FormPartFour extends StatefulWidget {
  final JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  const FormPartFour({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPartFourState createState() => _FormPartFourState();
}

class _FormPartFourState extends State<FormPartFour> {
  final String _TAG = "_FormPartFourState";
  LocationService _locationService = LocationService();

  DateTime now = DateTime.now();
  // deaflut deadline
  DateTime _deadLine;
  String _address;
  // location; //
  TheLocation _jobLocation;
  String _country,
      _administrativeArea,
      _subAdministrativeArea,
      _name,
      _locality,
      _subLocality,
      _thoroughfare;
  double _latitude, _longitude;

  @override
  void initState() {
    super.initState();
    _initDefaultValues();
  }

  void _initDefaultValues() {
    _deadLine = Helper.isNullOrEmpty(widget.jobPostModel?.deadline)
        ? DateTime.now()
        : DateTime.parse(widget.jobPostModel.deadline);
    _address = Helper.isNullOrEmpty(widget.jobPostModel?.address)
        ? ""
        : widget.jobPostModel.address;
    // loction
    _country = widget.jobPostModel?.country;
    _administrativeArea = widget.jobPostModel?.administrativeArea;
    _subAdministrativeArea = widget.jobPostModel?.subAdministrativeArea;
    _name = widget.jobPostModel?.name;
    _locality = widget.jobPostModel?.locality;
    _subLocality = widget.jobPostModel?.subLocality;
    _thoroughfare = widget.jobPostModel?.thoroughfare;
    _latitude = widget.jobPostModel?.latitude;
    _longitude = widget.jobPostModel?.longitude;
  }

  double _scrPadding = 10.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Helper.handleJobPostCloseDialog(context);
          },
        ),
        title: Text("New Post"),
        centerTitle: true,
      ),
      body: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        padding: EdgeInsets.symmetric(
          horizontal: _scrPadding,
          vertical: _scrPadding,
        ),
        child: Column(
          children: <Widget>[
            // contents
            ListView(
              physics: ScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              children: <Widget>[
                _buildMapPicker(),
                _buildAddressPicker(),
                _buildCalendarUi(),
              ],
            ),
            Spacer(),
            _buildButtons(),
          ],
        ),
      ),
    );
  }

  // location picker
  Widget _buildAddressPicker() {
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      height: 50.0,
      width: MediaQuery.of(context).size.width - 30.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.blue[400],
          ],
        ),
      ),
      child: MaterialButton(
        onPressed: () async {
          print("place");
          Prediction prediction = await PlacesAutocomplete.show(
            context: context,
            apiKey: Apikeys.geoApiKey,
            onError: (e) {},
            mode: Mode.fullscreen,
            language: "en",
            components: [Component(Component.country, "kr")],
          );
          setState(() {
            _address = prediction?.description;
          });
        },
        child: Text(
          Helper.isNullOrEmpty(_address) ? "Set Address" : _address,
          overflow: TextOverflow.ellipsis,
          maxLines: 3,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _buildMapPicker() {
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      height: 50.0,
      width: MediaQuery.of(context).size.width - 30.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.blue[400],
          ],
        ),
      ),
      child: InkWell(
        onTap: () async {
          var gpsStatus = await _locationService.isGpsOn();
          if (gpsStatus) {
            _jobLocation = await Navigator.push(context,
                MaterialPageRoute(builder: (context) {
              return JobMaps(fromWorkerContext: false);
            }));
            Helper.logPrint(_TAG, "setting... : ${_jobLocation.toString()}");
            setState(() {});
          } else {
            Fluttertoast.showToast(
              msg: "Need to enable GPS to set Job Location",
            );
          }
        },
        child: Text(
          _jobLocation?.country == null
              ? "Set Job Location From Map"
              : "${_jobLocation.name}, ${_jobLocation.subLocality}, ${_jobLocation.country}",
          overflow: TextOverflow.ellipsis,
          maxLines: 3,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _buildCalendarUi() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: Calendar(
        showCalendarPickerIcon: true,
        showTodayAction: true,
        onDateSelected: (dateTime) {
          setState(() {
            _deadLine = dateTime;
          });
        },
      ),
    );
  }

  // for buttons
  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // back btn
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: BorderedButton(
              child: Text(
                "BACK",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              borderColor: Colors.blue,
              buttonColor: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
        // continue button
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: GradientButton(
              child: Text(
                "CONTINUE",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              gradient: AppColors.blueWhiteGradient,
              onPressed: () {
                _onNavigation();
              },
            ),
          ),
        ),
      ],
    );
  }

  void _onNavigation() {
    widget.jobPostModel?.address = _address;
    widget.jobPostModel?.date = DateFormat('yyyy-MM-dd').format(now);
    widget.jobPostModel?.deadline = DateFormat('yyyy-MM-dd').format(_deadLine);
    // location
    widget.jobPostModel?.country = _jobLocation?.country;
    widget.jobPostModel?.administrativeArea = _jobLocation?.administrativeArea;
    widget.jobPostModel?.subAdministrativeArea =
        _jobLocation?.subAdministrativeArea;
    widget.jobPostModel?.name = _jobLocation?.name;
    widget.jobPostModel?.locality = _jobLocation?.locality;
    widget.jobPostModel?.subLocality = _jobLocation?.subLocality;
    widget.jobPostModel?.thoroughfare = _jobLocation?.thoroughfare;
    widget.jobPostModel?.latitude = _jobLocation?.latitude;
    widget.jobPostModel?.longitude = _jobLocation?.longitude;

    Helper.logPrint(_TAG, "model... : ${widget.jobPostModel.toString()}");
    Helper.logPrint(_TAG, "lat... : ${widget.jobPostModel.latitude}");

    print("date : ${widget.jobPostModel?.date}");
    print("deadline : ${widget.jobPostModel?.deadline}");

    Navigator.of(context).push(
      (MaterialPageRoute(
        builder: (context) => FormPickCategory(
          jobPostModel: widget.jobPostModel,
          documentReference: widget.documentReference,
          isUpdating: widget.isUpdating,
        ),
      )),
    );
  }
}
