import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/language_codes.dart';
import 'package:jobs/res/strings/salary_type.dart';
import 'package:jobs/ui/pages/employer/postForm/form_part_three.dart';
import 'package:jobs/ui/widgets/buttons/bordered_button.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/ui/widgets/custom_choice_chip.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * 1. salary range
 * 2. salary type
 * 3. job language
 */
class FormPartTwo extends StatefulWidget {
  final JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  const FormPartTwo({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPartTwoState createState() => _FormPartTwoState();
}

class _FormPartTwoState extends State<FormPartTwo> {
  // default salarytype
  String _salaryType;
  // default job language
  String _jobLanguage;
  TextEditingController _salaryRangeContrlr = TextEditingController();

  @override
  void initState() {
    super.initState();
    _salaryRangeContrlr.text = widget.jobPostModel?.salaryRange;
    _initDefaultValues();
  }

  @override
  void dispose() { 
    _salaryRangeContrlr.dispose();
    super.dispose();
  }

  void _initDefaultValues() {
    _salaryType = Helper.isNullOrEmpty(widget.jobPostModel?.salaryType)
        ? "HOURLY"
        : widget.jobPostModel.salaryType;
    _jobLanguage = Helper.isNullOrEmpty(widget.jobPostModel?.language)
        ? "Korean"
        : widget.jobPostModel.language;
  }

  var _scrPadding = 10.0;

  Widget _buildTextFieldItem(
    String title,
    TextEditingController controller,
    String hint, {
    int minLines,
    int maxLines,
    @required bool isValidationRequired,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _scrPadding),
      child: buildTextField(
        controller,
        hint,
        minLines: minLines,
        maxLines: maxLines,
        isValidationRequired: isValidationRequired,
      ),
    );
  }

  TextStyle chipTextstyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 17.0,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Helper.handleJobPostCloseDialog(context);
          },
        ),
        title: Text("New Post"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: Helper.getScreenWidth(context),
          height: Helper.getScreenHeight(context),
          padding: EdgeInsets.symmetric(
              horizontal: _scrPadding, vertical: _scrPadding),
          child: Column(
            children: <Widget>[
              _buildLanguageDropdown(),
              _buildTextFieldItem(
                "Salary Range",
                _salaryRangeContrlr,
                "Salary range (Optional)",
                isValidationRequired: false,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: _scrPadding),
                child: buildHeaderTextUi("SALARY TYPE"),
              ),
              _buildSalaryTypeChips(),
              Expanded(
                child: _buildButtons(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // salary type chips
  Widget _buildSalaryTypeChips() {
    return Wrap(
      children: <Widget>[
        CustomChoiceChip(
          title: SalaryType.hourly,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.hourglass,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.hourly;
            });
          },
        ),
        CustomChoiceChip(
          title: SalaryType.daily,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.calendarDay,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.daily;
            });
          },
        ),
        CustomChoiceChip(
          title: SalaryType.weekly,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.calendarWeek,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.weekly;
            });
          },
        ),
        CustomChoiceChip(
          title: SalaryType.monthly,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.monument,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.monthly;
            });
          },
        ),
        CustomChoiceChip(
          title: SalaryType.yearly,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.moneyCheck,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.yearly;
            });
          },
        ),
        CustomChoiceChip(
          title: SalaryType.fixed,
          defTitle: _salaryType,
          iconData: FontAwesomeIcons.fileExcel,
          onSelected: (_) {
            setState(() {
              _salaryType = SalaryType.fixed;
            });
          },
        ),
      ],
    );
  }

  // language dropdown
  Widget _buildLanguageDropdown() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          buildHeaderTextUi("Job Language"),
          DropdownButton(
            items: LanguageCodes.jobLanguages.map((language) {
              return DropdownMenuItem(
                value: language,
                child: Text(language),
              );
            }).toList(),
            onChanged: (changedLanguage) async {
              setState(() {
                this._jobLanguage = changedLanguage;
              });
            },
            value: _jobLanguage,
          ),
        ],
      ),
    );
  }

  // for buttons
  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // back btn
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: BorderedButton(
              borderColor: Colors.blue,
              buttonColor: Colors.white,
              child: Text(
                "BACK",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
        // continue button
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: GradientButton(
              child: Text(
                "CONTINUE",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              gradient: AppColors.blueWhiteGradient,
              onPressed: () {
                _onNavigation();
              },
            ),
          ),
        ),
      ],
    );
  }

  void _onNavigation() {
    widget.jobPostModel?.salaryRange = _salaryRangeContrlr.text;
    widget.jobPostModel?.salaryType = _salaryType;
    widget.jobPostModel?.language = _jobLanguage;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => FormPartThree(
          jobPostModel: widget.jobPostModel,
          documentReference: widget.documentReference,
          isUpdating: widget.isUpdating,
        ),
      ),
    );
  }
}
