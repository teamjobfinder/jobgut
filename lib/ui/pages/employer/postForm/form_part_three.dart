import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/job_experiences.dart';
import 'package:jobs/res/strings/gender_string.dart';
import 'package:jobs/res/strings/job_type.dart';
import 'package:jobs/ui/pages/employer/postForm/form_part_four.dart';
import 'package:jobs/ui/widgets/buttons/bordered_button.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/ui/widgets/custom_choice_chip.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * 1. Job Type 
 * 2. gender
 * 3. experience
 * 4. vacancies
 */
class FormPartThree extends StatefulWidget {
  final JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  const FormPartThree({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPartThreeState createState() => _FormPartThreeState();
}

class _FormPartThreeState extends State<FormPartThree> {
  // default job type
  String _jobType;
  // default preferred gender
  String _preferredGender;
  // default experience
  String _jobExperience;
  TextEditingController _vacanciesCntrlr = TextEditingController();
  var _scrPadding = 10.0;

  @override
  void initState() {
    super.initState();
    _vacanciesCntrlr.text = widget.jobPostModel?.vacancies.toString();
    _initDefaultValues();
  }

  @override
  void dispose() {
    _vacanciesCntrlr.dispose();
    super.dispose();
  }

  void _initDefaultValues() {
    _jobType = Helper.isNullOrEmpty(widget.jobPostModel?.jobType)
        ? "Full Time"
        : widget.jobPostModel.jobType;
    _preferredGender = Helper.isNullOrEmpty(widget.jobPostModel?.preferredGender)
        ? "MALE"
        : widget.jobPostModel.preferredGender;
    _jobExperience = Helper.isNullOrEmpty(widget.jobPostModel?.experience)
        ? "1 Year"
        : widget.jobPostModel.experience;
  }

  Widget _buildTextFieldItem(
    String title,
    TextEditingController controller,
    String hint, {
    int minLines,
    int maxLines,
    @required bool isValidationRequired,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _scrPadding),
      child: buildTextField(
        controller,
        hint,
        minLines: minLines,
        maxLines: maxLines,
        isValidationRequired: isValidationRequired,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Helper.handleJobPostCloseDialog(context);
          },
        ),
        title: Text("New Post"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: Helper.getScreenWidth(context),
          height: Helper.getScreenHeight(context),
          padding: EdgeInsets.symmetric(
              horizontal: _scrPadding, vertical: _scrPadding),
          child: Column(
            children: <Widget>[
              _buildTextFieldItem(
                "Vacancies",
                _vacanciesCntrlr,
                "No of vacancies (Optional)",
                isValidationRequired: false,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: _scrPadding),
                child: buildHeaderTextUi("JOB  TYPE"),
              ),
              _buildJobTypeChips(true),
              Container(
                padding: EdgeInsetsDirectional.only(
                    start: _scrPadding, end: _scrPadding, top: _scrPadding),
                child: buildHeaderTextUi("PREFERRED GENDER"),
              ),
              _buildPreferredGenderChips(true),
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: _scrPadding, vertical: _scrPadding),
                child: _buildExperienceUi(),
              ),
              _buildButtons(),
            ],
          ),
        ),
      ),
    );
  }

  // job type
  Widget _buildJobTypeChips(bool isLarge) {
    return Wrap(
      children: <Widget>[
        CustomChoiceChip(
          defTitle: _jobType,
          title: JobType.partTime,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.batteryQuarter,
          onSelected: (_) {
            setState(() {
              _jobType = JobType.partTime;
            });
          },
        ),
        CustomChoiceChip(
          defTitle: _jobType,
          title: JobType.fullTime,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.batteryFull,
          onSelected: (_) {
            setState(() {
              _jobType = JobType.fullTime;
            });
          },
        ),
        CustomChoiceChip(
          defTitle: _jobType,
          title: JobType.freelancer,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.kiwiBird,
          onSelected: (_) {
            setState(() {
              _jobType = JobType.freelancer;
            });
          },
        ),
      ],
    );
  }

  // preferred gender
  Widget _buildPreferredGenderChips(bool isLarge) {
    return Wrap(
      children: <Widget>[
        CustomChoiceChip(
          defTitle: _preferredGender,
          title: GenderString.male,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.male,
          onSelected: (_) {
            setState(() {
              _preferredGender = GenderString.male;
            });
          },
        ),
        CustomChoiceChip(
          defTitle: _preferredGender,
          title: GenderString.female,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.female,
          onSelected: (_) {
            setState(() {
              _preferredGender = GenderString.female;
            });
          },
        ),
        CustomChoiceChip(
          defTitle: _preferredGender,
          title: GenderString.any,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.baby,
          onSelected: (_) {
            setState(() {
              _preferredGender = GenderString.any;
            });
          },
        ),
      ],
    );
  }

  // experience dropdown
  Widget _buildExperienceUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buildHeaderTextUi("EXPERIENCE"),
        DropdownButton(
          items: JobExperience.experiences.map((experience) {
            return DropdownMenuItem(
              value: experience,
              child: Text(experience),
            );
          }).toList(),
          onChanged: (changedExperience) async {
            setState(() {
              this._jobExperience = changedExperience;
            });
          },
          value: _jobExperience,
        ),
      ],
    );
  }

  // for buttons
  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // back btn
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: BorderedButton(
              borderColor: Colors.blue,
              buttonColor: Colors.white,
              child: Text(
                "BACK",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
        // continue button
        Flexible(
          flex: 1,
          child: Container(
            alignment: Alignment.center,
            child: GradientButton(
              child: Text(
                "CONTINUE",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
              height: 50.0,
              width: Helper.getScreenWidth(context) - 200,
              gradient: AppColors.blueWhiteGradient,
              onPressed: () {
                _onNavigation();
              },
            ),
          ),
        ),
      ],
    );
  }

  void _onNavigation() {
    widget.jobPostModel?.vacancies = int.tryParse(_vacanciesCntrlr.text) ?? 1;
    widget.jobPostModel?.jobType = _jobType;
    widget.jobPostModel?.preferredGender = _preferredGender;
    widget.jobPostModel?.experience = _jobExperience;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => FormPartFour(
          jobPostModel: widget.jobPostModel,
          documentReference: widget.documentReference,
          isUpdating: widget.isUpdating,
        ),
      ),
    );
  }
}
