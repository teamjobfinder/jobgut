import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/ui/pages/employer/postForm/form_part_two.dart';
import 'package:jobs/ui/widgets/buttons/gradient_button.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:meta/meta.dart';

/**
 * 1. title
 * 2. description
 * 3. position
 * 4. schedule
 */
class FormPartOne extends StatefulWidget {
  JobPostModel jobPostModel;
  final DocumentReference documentReference;
  final bool isUpdating;

  /// Model & Reference needed for editing
  FormPartOne({
    Key key,
    this.jobPostModel,
    this.documentReference,
    @required this.isUpdating,
  }) : super(key: key);

  @override
  _FormPartOneState createState() => _FormPartOneState();
}

class _FormPartOneState extends State<FormPartOne> {
  var _partOneFormKey = GlobalKey<FormState>();
  double _scrPadding = 10.0;
  TextEditingController _titleCntrlr;
  TextEditingController _descriptionCntrlr;
  TextEditingController _positionCntrlr;
  TextEditingController _scheduleCntrlr;
  List<TimelineModel> items;
  Color _iconBg = Colors.blue;
  Icon _icon = Icon(Icons.blur_circular, color: Colors.white);

  @override
  void initState() {
    super.initState();
    _initEditiingControllers();
    _initControllersText();
    _initItems();
  }

  void _initEditiingControllers() {
    _titleCntrlr = TextEditingController();
    _descriptionCntrlr = TextEditingController();
    _positionCntrlr = TextEditingController();
    _scheduleCntrlr = TextEditingController();
  }

  void _initControllersText() {
    _titleCntrlr.text = widget.jobPostModel?.title;
    _descriptionCntrlr.text = widget.jobPostModel?.jobDescription;
    _positionCntrlr.text = widget.jobPostModel?.jobPosition;
    _scheduleCntrlr.text = widget.jobPostModel?.schedule;
  }

  // bool _checkNullOrEmpty()

  void _initItems() {
    items = [
      TimelineModel(
        _buildTextFieldItem("Title", _titleCntrlr, "Job Title (required)",
            isValidationRequired: true),
        position: TimelineItemPosition.left,
        iconBackground: _iconBg,
        icon: _icon,
      ),
      TimelineModel(
        _buildTextFieldItem(
          "Description",
          _descriptionCntrlr,
          "Job Description (required)",
          minLines: 10,
          maxLines: 20,
          isValidationRequired: true,
        ),
        position: TimelineItemPosition.left,
        iconBackground: _iconBg,
        icon: _icon,
      ),
      TimelineModel(
        _buildTextFieldItem(
          "Position",
          _positionCntrlr,
          "Job Position",
          isValidationRequired: false,
        ),
        position: TimelineItemPosition.left,
        iconBackground: _iconBg,
        icon: _icon,
      ),
      TimelineModel(
        _buildTextFieldItem(
          "Schedule",
          _scheduleCntrlr,
          "Job Schedule",
          isValidationRequired: false,
        ),
        position: TimelineItemPosition.left,
        iconBackground: _iconBg,
        icon: _icon,
      ),
      TimelineModel(
        Container(height: 100.0),
        icon: null,
        iconBackground: null,
      )
    ];
  }

  Widget _buildTextFieldItem(
    String title,
    TextEditingController controller,
    String hint, {
    int minLines,
    int maxLines,
    @required bool isValidationRequired,
  }) {
    return buildTextField(
      controller,
      hint,
      minLines: minLines,
      maxLines: maxLines,
      isValidationRequired: isValidationRequired,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Helper.handleJobPostCloseDialog(context);
          },
        ),
        title: Text("New Post"),
        centerTitle: true,
      ),
      body: Form(
        key: _partOneFormKey,
        child: Container(
          width: Helper.getScreenWidth(context),
          height: Helper.getScreenHeight(context),
          padding: EdgeInsets.symmetric(
              horizontal: _scrPadding, vertical: _scrPadding),
          child: Column(
            children: <Widget>[
              // textfields
              _buildBody(),
              // button
              _buildButton(),
            ],
          ),
        ),
      ),
    );
  }

  // for body
  Widget _buildBody() {
    return Expanded(
      child: Timeline(
        children: items,
        position: TimelinePosition.Left,
      ),
    );
  }

  // for buttons
  Widget _buildButton() {
    return Container(
      alignment: Alignment.center,
      child: GradientButton(
        child: Text(
          "CONTINUE",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
        height: 50.0,
        width: Helper.getScreenWidth(context) - 200,
        gradient: AppColors.blueWhiteGradient,
        onPressed: () {
          if (_partOneFormKey.currentState.validate()) {
            _onNavigation();
          }
        },
      ),
    );
  }

  void _onNavigation() {
    if (widget.jobPostModel == null) {
      widget.jobPostModel = JobPostModel(
        title: _titleCntrlr.text,
        jobDescription: _descriptionCntrlr.text,
        jobPosition: _positionCntrlr.text,
        schedule: _positionCntrlr.text,
      );
    } else {
      widget.jobPostModel.title = _titleCntrlr.text;
      widget.jobPostModel.jobDescription = _descriptionCntrlr.text;
      widget.jobPostModel.jobPosition = _positionCntrlr.text;
      widget.jobPostModel.schedule = _scheduleCntrlr.text;
    }
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => FormPartTwo(
          jobPostModel: widget.jobPostModel,
          documentReference: widget.documentReference,
          isUpdating: widget.isUpdating,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _titleCntrlr.dispose();
    _descriptionCntrlr.dispose();
    _positionCntrlr.dispose();
    _scheduleCntrlr.dispose();
    super.dispose();
  }
}
