import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/employer/active_jobs_page.dart';
import 'package:jobs/ui/pages/employer/employer_drawer.dart';
import 'package:jobs/ui/widgets/flip_drawer.dart';
import 'package:jobs/utils/helper.dart';

/**
 * const checked
 * theme checked
 */
// additional widget for using custom drawer [FlipDrawer]
class EmployerJobsPageDrawer extends StatelessWidget {
  final String _TAG = "EmployerJobsPageDrawer";
  const EmployerJobsPageDrawer();

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    return const FlipDrawer(
      child: const EmployerJobsPage(),
      drawer: const EmployerDrawer(),
    );
  }
}

class EmployerJobsPage extends StatelessWidget {

  const EmployerJobsPage();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          // insides are controlled from drawer
        ),
        body: const ActiveJobsPageParent(),
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime currentBackPressedTime;
    DateTime now = DateTime.now();
    if (currentBackPressedTime == null ||
        now.difference(currentBackPressedTime) > Duration(seconds: 2)) {
      currentBackPressedTime = now;
      Fluttertoast.showToast(msg: "Press again to exit");
      return Future.value(false);
    }
    return Future.value(true);
  }
}