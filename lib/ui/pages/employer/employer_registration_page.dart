import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/blocs/regBloc/employerRegBloc/employer_reg_bloc.dart';
import 'package:jobs/blocs/regBloc/employerRegBloc/employer_reg_event.dart';
import 'package:jobs/blocs/regBloc/employerRegBloc/employer_reg_state.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/common/login_page.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/ui/pages/worker/worker_home.dart';
import 'package:jobs/ui/widgets/bubble.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:progress_dialog/progress_dialog.dart';

/**
 * const checked
 */
class EmployerRegistrationPageParent extends StatelessWidget {
  final EmployersRepository employersRepository = EmployersRepository();

  EmployerRegistrationPageParent();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EmployerRegBloc(
        employersRepository: employersRepository,
      ),
      child: const EmployerRegistrationPage(),
    );
  }
}

class EmployerRegistrationPage extends StatefulWidget {

  const EmployerRegistrationPage();

  @override
  _EmployerRegistrationPageState createState() =>
      _EmployerRegistrationPageState();
}

class _EmployerRegistrationPageState extends State<EmployerRegistrationPage> {

  final String _TAG = "_EmployerRegistrationPageState";

  TextEditingController nameCntrlr = TextEditingController();
  TextEditingController emailCntrlr = TextEditingController();
  TextEditingController passCntrlr = TextEditingController();

  ProgressDialog progressDialog;
  UsersRepository usersRepository = UsersRepository();
  EmployerRegBloc employerRegBloc;
  var employerSignUpFormKey = GlobalKey<FormState>();

  double scrWidth, scrHeight;

  @override
  Widget build(BuildContext context) {

    Helper.logPrint(_TAG, AppStrings.logBuildFunction);

    scrHeight = MediaQuery.of(context).size.height;
    scrWidth = MediaQuery.of(context).size.width;
    progressDialog = ProgressDialog(context);
    employerRegBloc = BlocProvider.of<EmployerRegBloc>(context);
    return WillPopScope(
      onWillPop: () async => await Future.value(false),
      child: Scaffold(
        body: Form(
          key: employerSignUpFormKey,
          child: SingleChildScrollView(
            child: Container(
              height: scrHeight,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.blue[800], Colors.blue[400]],
                ),
              ),
              child: Stack(
                children: <Widget>[
                  // For Bloc
                  BlocListener<EmployerRegBloc, EmployerRegState>(
                    listener: (context, state) {
                      if (state is EmployerRegSuccess) {
                        progressDialog.hide();
                        navigateToEmployerHomePage(context);
                      } else if (state is EmployerRegFailure) {
                        progressDialog.hide();
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text(state.message),
                        ));
                      } else if (state is ThirdPartySignInEmployer) {
                        progressDialog.hide();
                        navigateToEmployerHomePage(context);
                      } else if (state is ThirdPartySignInWorker) {
                        progressDialog.hide();
                        navigateToWorkerHomePage(context);
                      }
                    },
                    child: BlocBuilder<EmployerRegBloc, EmployerRegState>(
                      builder: (context, state) {
                        if (state is EmployerRegInitial) {
                          return buildInitialUi();
                        } else if (state is EmployerRegLoading) {
                          return buildLoading();
                        } else if (state is EmployerRegSuccess) {
                          progressDialog.hide();
                          return Container();
                        } else if (state is EmployerRegFailure) {
                          progressDialog.hide();
                          return Container();
                        } else if (state is ThirdPartySignInEmployer) {
                          progressDialog.hide();
                          return Container();
                        } else if (state is ThirdPartySignInWorker) {
                          progressDialog.hide();
                          return Container();
                        }
                      },
                    ),
                  ),
                  // email sign up part
                  buildEmailSignUpPart(scrHeight, scrWidth),
                  // text
                  Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 100.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: const Text(
                        "Or Sign In With",
                        style: const TextStyle(color: Colors.white),
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                    ),
                  ),
                  // google sign in button
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildGoogleSignInUi(),
                        buildFacebookSignInUi(),
                      ],
                    ),
                  ),
                  // followings are bubbles
                  const Positioned(
                    right: -100.0,
                    top: -70.0,
                    child: const Bubble(
                      size: 250.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    bottom: 100.0,
                    child: const Bubble(
                      size: 50.0,
                    ),
                  ),
                  const Positioned(
                    right: 30.0,
                    bottom: 60.0,
                    child: const Bubble(
                      size: 30.0,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  // google sign in event added here
  Widget buildGoogleSignInUi() {
    return Align(
      child: GoogleSignInButton(
        text: "Google",
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();
          if (isConnected) {
            employerRegBloc.add(GoogleSignInBtnPressed());
          } else {
            Fluttertoast.showToast(msg: "Check Internet Connection");
          }
        },
        darkMode: false,
        borderRadius: 50.0,
      ),
    );
  }

  // facebook sign in event added here
  Widget buildFacebookSignInUi() {
    return Align(
      child: FacebookSignInButton(
        text: "Facebook",
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();
          if (isConnected) {
            employerRegBloc.add(FacebookSignInBtnPressed());
          } else {
            Fluttertoast.showToast(msg: "Check Internet Connection");
          }
        },
        borderRadius: 50.0,
      ),
    );
  }

  // email sign up event added here
  Widget buildEmailSignUpPart(double scrHeight, double scrWidth) {
    return Container(
      margin: const EdgeInsets.only(top: 40.0),
      child: Column(
        children: <Widget>[
          // header text
          Container(
            padding: const EdgeInsets.only(
                top: 30.0, bottom: 25.0, left: 10.0, right: 10.0),
            child: const Text(
              "Sign Up & Start Hiring Workers",
              style: const TextStyle(
                fontSize: 20.0,
                color: Colors.white,
                fontFamily: "RussoOne",
              ),
            ),
          ),
          // name textfield
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Write Your/Organization's Name Please";
                  }
                },
                style: const TextStyle(color: Colors.white),
                controller: nameCntrlr,
                decoration: InputDecoration(
                  errorStyle: const TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "Your/Organization's Name",
                  hintStyle: const TextStyle(color: Colors.white),
                  border: const OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: const Icon(Icons.info, color: Colors.white),
                ),
                keyboardType: TextInputType.text,
              ),
            ),
          ),
          // email textfield
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Enter a valid email address";
                  }
                },
                style: const TextStyle(color: Colors.white),
                controller: emailCntrlr,
                decoration: InputDecoration(
                  errorStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "E-mail",
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                  border: const OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: const Icon(Icons.email, color: Colors.white),
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
          ),
          // password textfield
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Password can't be empty";
                  }
                  if (value.length < 6) {
                    return "Password length should be at least 6";
                  }
                },
                style: const TextStyle(color: Colors.white),
                controller: passCntrlr,
                decoration: InputDecoration(
                  errorStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "Password",
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                  border: const OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: const Icon(Icons.lock, color: Colors.white),
                ),
                obscureText: true,
              ),
            ),
          ),
          // sign up btn, EmailSignUpEvent added here
          MaterialButton(
            minWidth: scrWidth * 0.8,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80.0)),
            child: Text(
              "SIGN UP NOW",
              style: TextStyle(
                color: Colors.blue[300],
                fontFamily: "RussoOne",
              ),
            ),
            onPressed: () async {
              if (employerSignUpFormKey.currentState.validate()) {
                var model = EmployerModel(
                  email: emailCntrlr.text,
                  name: nameCntrlr.text,
                  isVerified: false,
                );

                bool isConnected = await ConnectivityService().hasConnection();

                if (isConnected) {
                  progressDialog.show();
                  employerRegBloc.add(EmployerRegBtnPressed(
                    email: emailCntrlr.text,
                    password: passCntrlr.text,
                    employerModel: model,
                  ));
                } else {
                  progressDialog.hide();
                  Fluttertoast.showToast(
                    msg: "Check Internet Connection",
                  );
                }
              }
            },
            color: Colors.white,
          ),
          // login page and home page navigation
          Material(
            color: Colors.transparent,
            child: Container(
              padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
                bottom: 10.0,
                top: 30.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    "Already registered?   ",
                    style: const TextStyle(color: Colors.white),
                  ),
                  InkWell(
                    child: Container(
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(),
                      child: const Text(
                        "Login",
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    onTap: () {
                      navigateToLoginPage(context);
                    },
                  ),
                  const SizedBox(width: 10.0),
                  const Text("Or   ", style: const TextStyle(color: Colors.white)),
                  // go home
                  InkWell(
                    onTap: () {
                      navigateToAppHome(context);
                    },
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: const Text(
                        "GO HOME",
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildInitialUi() {
    return const SizedBox(height: 0.0);
  }

  Widget buildLoading() {
    return const Center(
      child: const CircularProgressIndicator(),
    );
  }

  Widget buildFailureUi(String message) {
    return Text(message);
  }

  void navigateToEmployerHomePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return const EmployerHomePage();
    }));
  }

  void navigateToWorkerHomePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerHome();
    }));
  }

  void navigateToLoginPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return LoginPageParent(loginWorker: false);
    }));
  }

  void navigateToAppHome(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return const EmployeeEmployerPage();
    }));
  }

  @override
  void dispose() {
    nameCntrlr.dispose();
    emailCntrlr.dispose();
    passCntrlr.dispose();
    super.dispose();
  }
}
