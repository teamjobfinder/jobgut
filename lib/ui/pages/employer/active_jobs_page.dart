import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/employerBloc/activeJobsBloc/active_jobs_bloc.dart';
import 'package:jobs/blocs/employerBloc/activeJobsBloc/active_jobs_event.dart';
import 'package:jobs/blocs/employerBloc/activeJobsBloc/active_jobs_state.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/data/services/dynamic_url_service.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:jobs/ui/pages/employer/postForm/form_part_one.dart';
import 'package:jobs/ui/widgets/buttons/rounded_button.dart';
import 'package:jobs/ui/widgets/failures/failure_ui.dart';
import 'package:jobs/ui/widgets/job_post_thumbnail.dart';
import 'package:jobs/ui/widgets/loadings/feed_loading.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';
import 'package:share/share.dart';

// employer side
/**
 * const checked
 * theme checked
 */

class ActiveJobsPageParent extends StatelessWidget {
  const ActiveJobsPageParent();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ActiveJobsBloc>(
      create: (context) => ActiveJobsBloc(),
      child: ActiveJobsPage(),
    );
  }
}

class ActiveJobsPage extends StatefulWidget {
  const ActiveJobsPage();

  @override
  _ActiveJobsPageState createState() => _ActiveJobsPageState();
}

class _ActiveJobsPageState extends State<ActiveJobsPage> {
  final String _TAG = "_ActiveJobsPageState";

  double scrHeight, scrWidth;
  EmployerRepository employerRepository = EmployerRepository();
  ActiveJobsBloc _activeJobsBloc;

  @override
  void initState() {
    super.initState();
    _activeJobsBloc = BlocProvider.of<ActiveJobsBloc>(context);
    _activeJobsBloc.add(FetchActiveJobs());
  }

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    scrHeight = MediaQuery.of(context).size.height;
    scrWidth = MediaQuery.of(context).size.width;
    return Container(
      width: scrWidth,
      height: scrHeight,
      // color: AppColors.bgColor,
      child: Stack(
        children: <Widget>[
          BlocListener<ActiveJobsBloc, ActiveJobsState>(
            listener: (context, state) {
              if (state is JobPostSuccessFullyDeleted) {
                _showSnackBar(context, "Post Deleted");
              } else if (state is JobPostDeleteFailure) {
                _showSnackBar(context, "Failed To Delete Post");
              }
            },
            child: BlocBuilder<ActiveJobsBloc, ActiveJobsState>(
              builder: (context, state) {
                if (state is ActiveJobsLoading) {
                  return const FeedLoading();
                } else if (state is ActiveJobsLoaded) {
                  return _PostsLoadedui(posts: state.activeJobs);
                } else if (state is NoActiveJobs) {
                  return const _NoPostsUi();
                } else if (state is ActiveJobsLoadFailure) {
                  return FailureUi(message: state.message);
                } else if (state is JobPostSuccessFullyDeleted) {
                  return const SizedBox(height: 0.0);
                } else if (state is JobPostDeleteFailure) {
                  return const SizedBox(height: 0.0);
                }
              },
            ),
          ),
          Positioned(
            bottom: 10.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              alignment: Alignment.center,
              child: RoundedButton(
                height: 50.0,
                width: scrWidth * 0.4,
                text: "POST JOB",
                onPressed: () async {
                  bool isVerified =
                      await employerRepository.isEmployerVerified();

                  if (!isVerified) {
                    Fluttertoast.showToast(
                        msg: "Verify Your Phone Number First",
                        toastLength: Toast.LENGTH_LONG);
                    return;
                  }
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => FormPartOne(isUpdating: false),
                    ),
                  );
                },
                textColor: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  }
}

class _NoPostsUi extends StatelessWidget {
  const _NoPostsUi();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      height: 300.0,
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
      ),
      child: const Center(
        child: const Text(
          "You have not posted any job yet!",
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class _PostsLoadedui extends StatefulWidget {
  final List<JobPostDocumentModel> posts;

  const _PostsLoadedui({
    Key key,
    @required this.posts,
  }) : super(key: key);

  @override
  _PostsLoadeduiState createState() => _PostsLoadeduiState();
}

class _PostsLoadeduiState extends State<_PostsLoadedui> {
  double _adHeight = 0.0;
  // StreamSubscription _subscription;
  // final _nativeAdController = NativeAdmobController();
  DynamicUrlService _dynamicUrlService = DynamicUrlService();

  // void _onStateChanged(AdLoadState state) {
  //   switch (state) {
  //     case AdLoadState.loading:
  //       setState(() {
  //         _adHeight = 0;
  //       });
  //       break;

  //     case AdLoadState.loadCompleted:
  //       setState(() {
  //         _adHeight = 330;
  //       });
  //       break;

  //     default:
  //       break;
  //   }
  // }

  @override
  void initState() {
    // _subscription = _nativeAdController.stateChanged.listen(_onStateChanged);
    super.initState();
  }

  @override
  void dispose() {
    // _subscription.cancel();
    // _nativeAdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(bottom: 100.0, top: 10.0),
      itemCount: widget.posts.length,
      itemBuilder: (context, position) {
        JobPostModel jobPostModel = widget.posts[position].jobPostModel;
        DocumentReference documentReference =
            widget.posts[position].documentReference;
        DocumentSnapshot documentSnapshot =
            widget.posts[position].documentSnapshot;
        return InkWell(
          onTap: () {
            Get.to(PostDetailsPageParent(
              isWorker: false,
              jobPostModel: jobPostModel,
              documentReference: documentReference,
              documentSnapshot: documentSnapshot,
            ));
          },
          child: JobPostThumbnail(
            jobPostModel: jobPostModel,
            reference: documentReference,
            onDelete: () {
              BlocProvider.of<ActiveJobsBloc>(context)
                  .add(DeleteJobPost(documentReference: documentReference));
            },
            onShare: () async {
              String link = await _dynamicUrlService.createPostLink(
                  jobPostModel, documentReference);
              Share.share(link);
            },
          ),
        );
      },
      separatorBuilder: (context, position) {
        // show ad
        if (position == 2) {
          // return CustomAd(
          //   nativeAdmobController: _nativeAdController,
          //   adHeight: _adHeight,
          // );
          return const SizedBox(height: 0.0);
        } else {
          return const SizedBox(height: 0.0);
        }
      },
    );
  }
}
