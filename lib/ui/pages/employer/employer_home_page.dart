import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/employer/employer_jobs_page.dart';
import 'package:jobs/ui/pages/employer/employer_profile_page.dart';
import 'package:jobs/utils/helper.dart';

/**
 * Const checked
 * theme checked
 */
class EmployerHomePage extends StatefulWidget {

  const EmployerHomePage();

  @override
  _EmployerHomePageState createState() => _EmployerHomePageState();
}

class _EmployerHomePageState extends State<EmployerHomePage> {

  final String _TAG = "_EmployerHomePageState";

  int current_tab = 0;
  DateTime currentBackPressTime;

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              current_tab = index;
            });
          },
          currentIndex: current_tab,
          items: [
            BottomNavigationBarItem(
              icon: const Icon(Icons.devices),
              title: const Text("My Jobs"),
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              title: const Text("Profile"),
            ),
          ],
        ),
        body: IndexedStack(
          children: <Widget>[
            const EmployerJobsPageDrawer(),
            const EmployerProfilePageParent(),
          ],
          index: current_tab,
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Press again to exit");
      print("SINGLE PRESSED");
      return Future.value(false);
    }
    print("DOUBLE PRESSED");
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    // return Future.value(true);
  }
}