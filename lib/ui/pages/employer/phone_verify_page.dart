import 'package:country_code_picker/country_code.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/widgets/bubble.dart';
import 'package:jobs/ui/widgets/count_down_timer.dart';
import 'package:jobs/ui/widgets/country_picker.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:progress_dialog/progress_dialog.dart';

class PhoneVerifyPage extends StatefulWidget {
  const PhoneVerifyPage();

  @override
  _PhoneVerifyPageState createState() => _PhoneVerifyPageState();
}

class _PhoneVerifyPageState extends State<PhoneVerifyPage> {
  final String _TAG = "_PhoneVerifyPageState";

  var scrWidth, scrHeight;
  EmployerRepository employerRepository = EmployerRepository();

  CountryCode country = CountryCode(
    code: "KR",
    dialCode: "+82",
    flagUri: "flags/kr.png",
    name: "대한민국",
  );
  TextEditingController phoneCntrlr = TextEditingController();
  TextEditingController otpCntrlr = TextEditingController();

  EmployerModel employerModel;
  String verificationId, smsCode;
  bool codeSent = false;
  ProgressDialog progressDialog;

  @override
  void dispose() {
    phoneCntrlr.dispose();
    otpCntrlr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);

    progressDialog = ProgressDialog(context);
    scrWidth = MediaQuery.of(context).size.width;
    scrHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Verify Phone Number",
          style: TextStyle(
              fontSize: 20.0,
              color: Colors.blue[400],
              letterSpacing: 2,
              fontFamily: "RussoOne"),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.grey),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Material(
            child: FutureBuilder(
              future: employerRepository.isEmployerVerified(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return buildErrorUi("Error : ${snapshot.error.toString()}");
                }
                if (snapshot.hasData) {
                  bool isVerified = snapshot.data;
                  if (isVerified) {
                    return buildVerifiedUserUi();
                  } else {
                    return FutureBuilder(
                      future: employerRepository.getCurrentEmployer(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return buildErrorUi(
                              "Error : ${snapshot.error.toString()}");
                        }
                        if (snapshot.hasData) {
                          employerModel = snapshot.data;
                          print("Employer : ${employerModel.name}");
                          return buildBody();
                        } else {
                          return buildLoadingIndicator();
                        }
                      },
                    );
                  }
                } else {
                  return buildLoadingIndicator();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            buildBackGround(),
            Positioned(
              right: 20.0,
              top: 20.0,
              child: Bubble(
                size: 80.0,
                color: Colors.blue[600],
              ),
            ),
            const Positioned(
              right: -40.0,
              top: -40.0,
              child: const Bubble(
                size: 160.0,
                color: Colors.white12,
              ),
            ),
            buildText(),
          ],
        ),
        // phone number section
        codeSent == false ? buildPhoneNumberSection() : Container(),
        // otp section
        codeSent == true ? buildOtpSection() : Container(),
      ],
    );
  }

  Widget buildPhoneNumberSection() {
    return Column(
      children: <Widget>[
        buildTitleText("Enter Your Phone Number"),
        // number picker
        Container(
          padding: const EdgeInsets.only(left: 10.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CountryPicker(
                initialCountryCode: country.dialCode,
                onChanged: (c) {
                  setState(() {
                    country = c;
                  });
                },
              ),
              buildTextField(
                  phoneCntrlr, "Enter A Valid Phone Number", "Phone"),
            ],
          ),
        ),
        const SizedBox(height: 10.0),
        // button
        MaterialButton(
          color: Colors.blue[400],
          child:
              const Text("Submit", style: const TextStyle(color: Colors.white)),
          onPressed: () async {
            if (phoneCntrlr.text.isEmpty) {
              Fluttertoast.showToast(msg: "Enter A Valid Phone Number");
              return;
            }
            var number = country.dialCode + phoneCntrlr.text;
            verifyPhone(number);
          },
        ),
      ],
    );
  }

  Widget buildTextField(
      TextEditingController controller, String hintText, String type) {
    return Container(
      width: scrWidth * 0.7,
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Material(
        elevation: 6.0,
        child: TextFormField(
          onChanged: (value) {
            if (type == "OTP") {
              this.smsCode = value;
            }
          },
          controller: controller,
          style: TextStyle(color: Colors.blue[400]),
          decoration: InputDecoration(
            errorStyle: TextStyle(color: Colors.red),
            filled: true,
            hintText: hintText,
            hintStyle: TextStyle(color: Colors.grey),
            border: InputBorder.none,
            prefixIcon: Icon(Icons.phone, color: Colors.blue[400]),
          ),
          keyboardType: TextInputType.phone,
        ),
      ),
    );
  }

  Widget buildBackGround() {
    return Container(
      width: scrWidth,
      height: scrHeight * 0.3,
      margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: [Colors.blue[800], Colors.blue[400]],
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
    );
  }

  Widget buildText() {
    return Positioned(
      bottom: 90.0,
      left: 30.0,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppStrings.appName,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 25.0,
                fontFamily: "RussoOne",
                letterSpacing: 2.5,
              ),
            ),
            const Text(
              "Simple Job Searching App",
              style: const TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontFamily: "RussoOne",
                letterSpacing: 2,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTitleText(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Text(
        text,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17.0,
        ),
      ),
      alignment: Alignment.centerLeft,
    );
  }

  Widget buildOtpSection() {
    var number = country.dialCode + phoneCntrlr.text;
    return Column(
      children: <Widget>[
        buildTimer(),
        const Text(
          "OTP code is sent to your phone number\n Code will be expired in 100 seconds. \n Not received code? Restart your phone & try again",
        ),
        // buildTextField(otpCntrlr, "Enter The OTP Code", "OTP"),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 5.0),
          child: PinCodeTextField(
            length: 6,
            obsecureText: false,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.box,
              borderRadius: BorderRadius.circular(5),
              fieldHeight: 50,
              fieldWidth: 40,
              activeFillColor: Colors.white,
            ),
            animationDuration: Duration(milliseconds: 300),
            backgroundColor: Colors.blue.shade50,
            enableActiveFill: true,
            controller: otpCntrlr,
            onCompleted: (v) {
              print(" OTP Completed");
            },
            onChanged: (value) {
              print(value);
              setState(() {
                this.smsCode = value;
              });
            },
          ),
        ),
        MaterialButton(
          color: Colors.blue[400],
          child:
              const Text("Submit", style: const TextStyle(color: Colors.white)),
          onPressed: () {
            verifyWithOTP(smsCode, verificationId, number);
          },
        ),
      ],
    );
  }

  TextStyle textStyle(double fontSize, String fontfamily) {
    return TextStyle(
      color: Colors.white,
      fontSize: fontSize,
      fontFamily: fontfamily,
      letterSpacing: 2.5,
    );
  }

  Widget buildVerifiedUserUi() {
    return Container(
      height: scrHeight,
      width: scrWidth,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: [Colors.blue[800], Colors.blue[400]],
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: scrHeight * 0.3, bottom: 10.0),
            child: Text(
              "You Are A Verified User!",
              style: textStyle(20.0, "RussoOne"),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Go Ahead",
              style: textStyle(17.0, "RussoOne"),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "And",
              style: textStyle(17.0, "RussoOne"),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Post Jobs!",
              style: textStyle(27.0, "RussoOne"),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> verifyPhone(phoneNumber) async {
    progressDialog.show();

    final PhoneVerificationCompleted verified =
        (AuthCredential credential) async {
      progressDialog.dismiss();
      print("CallBack : verified");
      FirebaseUser user = await FirebaseAuth.instance.currentUser();
      await user.linkWithCredential(credential).then((_) async {
        print("User Successfully Linked");
        employerModel.isVerified = true;
        employerModel.phoneNumber = phoneNumber;
        await employerRepository.updateEmployerProfile(employerModel);
        Fluttertoast.showToast(
          msg: "Successfully Verified!",
          backgroundColor: Colors.green,
        );
        setState(() {});
      }).catchError((e) {
        print("Linking Error : ${e.toString()}");
        Fluttertoast.showToast(
          msg: "Error : ${e.toString()}",
          backgroundColor: Colors.red,
        );
      });
    };

    final PhoneVerificationFailed verificationfailed =
        (AuthException authException) {
      print('${authException.message.toString()}');
      print("CallBack : failed");
      Fluttertoast.showToast(
        msg: "Error : ${authException.message.toString()}",
        backgroundColor: Colors.red,
      );
      progressDialog.dismiss();
    };

    final PhoneCodeSent smsSent = (String verificationId, [int forceResend]) {
      print("CallBack : smssent");
      progressDialog.dismiss();
      this.verificationId = verificationId;
      setState(() {
        this.codeSent = true;
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      print("CallBack : timeout");
      progressDialog.dismiss();
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      timeout: const Duration(seconds: 100),
      verificationCompleted: verified,
      verificationFailed: verificationfailed,
      codeSent: smsSent,
      codeAutoRetrievalTimeout: autoTimeout,
    );
  }

  verifyWithOTP(smsCode, verId, phoneNumber) async {
    progressDialog.show();
    var user = await FirebaseAuth.instance.currentUser();

    AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);

    user.linkWithCredential(credential).then((_) async {
      employerModel.isVerified = true;
      employerModel.phoneNumber = phoneNumber;
      await employerRepository.updateEmployerProfile(employerModel);
      progressDialog.dismiss();
      print("Otp verification successful for $phoneNumber");
      Fluttertoast.showToast(msg: "Successfully Verified With OTP!");
      setState(() {});
    }).catchError((e) {
      print("Otp verification error : ${e.toString()}");
      progressDialog.dismiss();
      Fluttertoast.showToast(
        msg: "OTP verification error : ${e.toString()}",
      );
    });
  }

  Widget buildTimer() {
    return CountDownTimer(seconds: 100);
  }
}
