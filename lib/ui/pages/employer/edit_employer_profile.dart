import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/app_assets.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/keys.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';
import 'package:progress_dialog/progress_dialog.dart';

/**
 * setState pattern used
 * const checked
 * theme checked
 */
class EditEmployerProfilePage extends StatefulWidget {
  EmployerModel employerModel;

  EditEmployerProfilePage({@required this.employerModel});

  @override
  _EditEmployerProfilePageState createState() =>
      _EditEmployerProfilePageState();
}

class _EditEmployerProfilePageState extends State<EditEmployerProfilePage> {
  Color textBoxColor = AppColors.bgColor;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController websiteController = TextEditingController();

  String location;
  String nationality;
  Country country;
  double _latitude, _longitude;

  var editEmployerProfileFormKey = GlobalKey<FormState>();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Apikeys.geoApiKey);

  EmployerRepository employerRepository = EmployerRepository();
  UsersRepository usersRepository = UsersRepository();

  File _profileImage;
  ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    nameController.text = widget.employerModel.name;
    emailController.text = widget.employerModel.email;
    descriptionController.text = widget.employerModel.description;
    websiteController.text = widget.employerModel.websiteUrl;
    nationality = widget.employerModel.nationality;
    location = widget.employerModel.location;
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Profile"),
        iconTheme: const IconThemeData(color: Colors.grey),
        centerTitle: true,
      ),
      body: Form(
        key: editEmployerProfileFormKey,
        child: Material(
          child: ListView(
            padding: const EdgeInsets.all(10.0),
            children: <Widget>[
              // pp
              Align(
                alignment: Alignment.center,
                child: Stack(
                  children: <Widget>[
                    Container(
                      child: ClipOval(
                        child: _profileImage != null
                            ? Image.file(
                                _profileImage,
                                width: 210.0,
                                height: 210.0,
                                fit: BoxFit.fill,
                              )
                            : Image.asset(
                                AppAssets.employerPlaceHolderImage,
                                height: 210.0,
                                width: 210.0,
                                fit: BoxFit.cover,
                              ),
                      ),
                    ),
                    Positioned(
                      bottom: 20.0,
                      right: 20.0,
                      child: IconButton(
                        icon: const Icon(
                          Icons.camera_alt,
                          size: 50.0,
                          color: Colors.blue,
                        ),
                        onPressed: () async {
                          var image = await Helper.pickImageFromGallery();
                          setState(() {
                            _profileImage = image;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
              // name
              buildHeader("Company Name"),
              Container(
                // color: textBoxColor,
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write the name of compnay or employer";
                    }
                  },
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Comany or Employer Name",
                  ),
                ),
              ),
              // About
              buildHeader("About Your Company"),
              buildTextField(
                  descriptionController, "About Employer or Company"),
              // email
              buildHeader("Company Email"),
              Container(
                // color: textBoxColor,
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write the email of compnay or employer";
                    }
                  },
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Comany or Employer Email",
                  ),
                ),
              ),
              // location
              buildHeader("Location"),
              buildLocationPicker(),
              // nationality
              buildHeader("Nationality"),
              Container(
                // color: textBoxColor,
                child: CountryPicker(
                  onChanged: (c) {
                    setState(() {
                      country = c;
                    });
                  },
                  showCurrency: false,
                  showCurrencyISO: false,
                  showDialingCode: false,
                  showFlag: true,
                  showName: true,
                  selectedCountry: country,
                ),
              ),
              buildHeader("Website"),
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                // color: textBoxColor,
                child: TextFormField(
                  controller: websiteController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Comany or Employer Website",
                  ),
                ),
              ),
              // button
              InkWell(
                onTap: () async {
                  if (editEmployerProfileFormKey.currentState.validate()) {
                    progressDialog.show();
                    var currentEmployer =
                        await employerRepository.getCurrentEmployer();
                    String logoUrl = _profileImage != null
                        ? await usersRepository
                            .uploadProfileImageToDatabase(_profileImage)
                        : currentEmployer.logoUrl;

                    widget.employerModel.logoUrl = logoUrl;
                    widget.employerModel.name = nameController.text;
                    widget.employerModel.email = emailController.text;
                    widget.employerModel.location = location.toString();
                    widget.employerModel.latitude = _latitude;
                    widget.employerModel.longitude = _longitude;
                    widget.employerModel.description =
                        descriptionController.text;
                    widget.employerModel.websiteUrl = websiteController.text;
                    widget.employerModel.nationality =
                        Helper.isNullOrEmpty(country)
                            ? nationality
                            : country.name;

                    bool isConnected =
                        await ConnectivityService().hasConnection();

                    if (isConnected) {
                      await employerRepository
                          .updateEmployerProfile(widget.employerModel)
                          .then((_) {
                        resetTextControllers();
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Successfully Updated",
                          backgroundColor: Colors.blue,
                        );
                        Navigator.pop(context);
                      }).catchError((e) {
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Error : ${e.toString()}",
                          backgroundColor: Colors.red,
                        );
                      });
                    } else {
                      progressDialog.hide();
                      Fluttertoast.showToast(
                        msg: "Failed. Check Network Connection",
                      );
                    }
                  }
                },
                child: Container(
                  height: 60.0,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.blue,
                  ),
                  child: const Text(
                    "Update Profile",
                    style: TextStyle(
                      // color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLocationPicker() {
    return Container(
      margin: const EdgeInsets.only(bottom: 15.0),
      height: 50.0,
      width: MediaQuery.of(context).size.width - 30.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.blue[400],
          ],
        ),
      ),
      child: MaterialButton(
        onPressed: () async {
          Prediction prediction = await PlacesAutocomplete.show(
            context: context,
            apiKey: Apikeys.geoApiKey,
            onError: (e) {},
            mode: Mode.fullscreen,
            language: "en",
            components: [Component(Component.country, "kr")],
          );
          setState(() {
            location = prediction?.description;
            setLatLang(prediction);
          });
        },
        child: Text(
          Helper.isNullOrEmpty(location) ? "Set Location" : location,
          overflow: TextOverflow.ellipsis,
          maxLines: 3,
          style: const TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Future<void> setLatLang(Prediction prediction) async {
    if (prediction != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(prediction.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      _latitude = lat;
      _longitude = lng;
    }
  }

  Widget buildHeader(String text) {
    return Container(
      margin:const EdgeInsets.only(top: 5.0),
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget buildTextField(TextEditingController controller, String text) {
    return Container(
      // color: textBoxColor,
      child: TextFormField(
        controller: controller,
        decoration:  InputDecoration(
          border: const OutlineInputBorder(),
          hintText: text,
        ),
        minLines: 10,
        maxLines: 20,
      ),
    );
  }

  void resetTextControllers() {
    nameController.text = "";
    emailController.text = "";
    descriptionController.text = "";
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    descriptionController.dispose();
    super.dispose();
  }
}