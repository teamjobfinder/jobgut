import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/strings/job_categories.dart';
import 'package:jobs/ui/temp/temp_map.dart';
import 'package:jobs/ui/widgets/blank_image_placeholder.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:jobs/utils/location_service.dart';
import 'package:meta/meta.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:progress_dialog/progress_dialog.dart';

/**
 * const checked
 * theme checked
 */
class EditJobPostPage extends StatefulWidget {
  JobPostModel jobPostModel;
  DocumentReference documentReference;

  EditJobPostPage({
    @required this.jobPostModel,
    @required this.documentReference,
  });

  @override
  _EditJobPostPageState createState() => _EditJobPostPageState();
}

class _EditJobPostPageState extends State<EditJobPostPage> {
  // default salarytype
  String salaryType;
  // default job category
  String jobCategory;
  // default job type
  String jobType;

  Color textBoxColor = AppColors.textBoxColor;
  var editJobPostormKey = GlobalKey<FormState>();

  DateTime now = DateTime.now();
  // deaflut deadline
  DateTime deadLine = DateTime.now();

  TextEditingController titleCntrlr = TextEditingController();
  TextEditingController descCntrlr = TextEditingController();
  TextEditingController jobPosCntrlr = TextEditingController();
  TextEditingController salaryRangeCntrlr = TextEditingController();
  TextEditingController vacanciesCntrlr = TextEditingController();
  TextEditingController refCntrlr = TextEditingController();
  TextEditingController experienceCntrlr = TextEditingController();
  TextEditingController jobTypeCntrlr = TextEditingController();
  TextEditingController scheduleCntrlr = TextEditingController();
  TextEditingController addressCntrlr = TextEditingController();

  EmployerRepository employerRepository = EmployerRepository();
  EmployerModel employerModel;
  TheLocation jobLocation = TheLocation();
  LocationService locationService = LocationService();

  ProgressDialog progressDialog;
  File _jobPostImage;
  double scrWidth;

  @override
  void initState() {
    super.initState();
    salaryType = widget.jobPostModel.salaryType;
    jobCategory = widget.jobPostModel.jobCategory;
    jobType = widget.jobPostModel.jobType;

    titleCntrlr.text = widget.jobPostModel.title;
    descCntrlr.text = widget.jobPostModel.jobDescription;
    jobPosCntrlr.text = widget.jobPostModel.jobPosition;
    salaryRangeCntrlr.text = widget.jobPostModel.salaryRange;
    vacanciesCntrlr.text = widget.jobPostModel.vacancies.toString();
    experienceCntrlr.text = widget.jobPostModel.experience;
    jobTypeCntrlr.text = widget.jobPostModel.jobType;
    scheduleCntrlr.text = widget.jobPostModel.schedule;
    addressCntrlr.text = widget.jobPostModel.address;
    getCurrentEmployer();
  }

  @override
  void dispose() {
    titleCntrlr.dispose();
    descCntrlr.dispose();
    jobPosCntrlr.dispose();
    salaryRangeCntrlr.dispose();
    vacanciesCntrlr.dispose();
    refCntrlr.dispose();
    experienceCntrlr.dispose();
    jobTypeCntrlr.dispose();
    scheduleCntrlr.dispose();
    addressCntrlr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    scrWidth = MediaQuery.of(context).size.width;
    progressDialog = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        title: const Text("Edit Job Post"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.camera_alt,
              color: Colors.blue,
            ),
            onPressed: () async {
              var image = await Helper.pickImageFromGallery();
              setState(() {
                _jobPostImage = image;
              });
            },
          ),
        ],
      ),
      body: Form(
        key: editJobPostormKey,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              // job post image
              Align(
                alignment: Alignment.center,
                child: Stack(
                  children: <Widget>[
                    // job image
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(top: 30.0, bottom: 10.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30.0),
                        child: Helper.isNullOrEmpty(
                                    widget.jobPostModel.jobImageUrl) !=
                                true
                            ? Image.network(widget.jobPostModel.jobImageUrl)
                            : _jobPostImage != null
                                ? Image.file(
                                    _jobPostImage,
                                    width: scrWidth,
                                    height: 200.0,
                                    fit: BoxFit.cover,
                                  )
                                : BlankImagePlaceHolder(
                                    text: "Add An Image Related To Job",
                                    height: 100.0,
                                    width: scrWidth,
                                    onClick: () async {
                                      print("cam p");
                                      var image =
                                          await Helper.pickImageFromGallery();
                                      setState(() {
                                        _jobPostImage = image;
                                      });
                                    },
                                  ),
                      ),
                    ),
                  ],
                ),
              ),
              // title
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Job Title",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write a job title";
                    }
                  },
                  controller: titleCntrlr,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Job Title",
                  ),
                ),
              ),
              // description
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Job Description",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write a job description";
                    }
                  },
                  controller: descCntrlr,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Job Description",
                  ),
                  minLines: 10,
                  maxLines: 20,
                ),
              ),
              // position
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Job Position",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextField(
                  controller: jobPosCntrlr,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Job Position",
                  ),
                ),
              ),
              // salary
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Salary Range",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextField(
                  controller: salaryRangeCntrlr,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Salary Range",
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  choiceChip("Daily"),
                  choiceChip("Weekly"),
                  choiceChip("Monthly"),
                  choiceChip("Yearly"),
                ],
              ),
              // job schedule
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Job Schedule",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextField(
                  controller: scheduleCntrlr,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Job Schedule",
                  ),
                ),
              ),
              // job category
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Job Category",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Wrap(
                children: <Widget>[
                  checkbox(JobCategories.designer),
                  checkbox(JobCategories.uiux),
                  checkbox(JobCategories.engineer),
                  checkbox(JobCategories.doctor),
                  checkbox(JobCategories.chef),
                  checkbox(JobCategories.cleaning),
                  checkbox(JobCategories.warehouse),
                  checkbox(JobCategories.official),
                  checkbox(JobCategories.salesAndMarketing),
                  checkbox(JobCategories.driverAndCourier),
                  checkbox(JobCategories.construction),
                  checkbox(JobCategories.teacher),
                  checkbox(JobCategories.lawyer),
                  checkbox(JobCategories.photographer),
                  checkbox(JobCategories.receptionist),
                  checkbox(JobCategories.customerSupport),
                  checkbox(JobCategories.eventManagement),
                  checkbox(JobCategories.waiter),
                  checkbox(JobCategories.others),
                ],
              ),
              // job type
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Job Type",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Wrap(
                children: <Widget>[
                  jobTypeheckbox(AppStrings.fullTime),
                  const SizedBox(width: 20.0),
                  jobTypeheckbox(AppStrings.partTime),
                  const SizedBox(width: 20.0),
                  jobTypeheckbox(AppStrings.freelancer),
                ],
              ),
              // vacancies
              Container(
                padding: const EdgeInsets.only(right: 10.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    const Text(
                      "Vacancies : ",
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      width: 140.0,
                      height: 30.0,
                      child: TextField(
                        controller: vacanciesCntrlr,
                        decoration: const InputDecoration(
                          border: const OutlineInputBorder(),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              ),
              // experience
              Container(
                padding: const EdgeInsets.only(right: 10.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    const Text(
                      "Experience : ",
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      width: 140.0,
                      height: 30.0,
                      child: TextField(
                        controller: experienceCntrlr,
                        decoration: const InputDecoration(
                          border: const OutlineInputBorder(),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              ),
              // location
              // TODO will be a map later
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Job Location",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 50.0,
                width: MediaQuery.of(context).size.width - 30.0,
                alignment: Alignment.center,
                // color: textBoxColor,
                child: InkWell(
                  onTap: () async {
                    var gpsStatus = await locationService.isGpsOn();
                    if (gpsStatus) {
                      jobLocation = await Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return JobMaps(fromWorkerContext: false);
                      }));
                    } else {
                      Fluttertoast.showToast(
                        msg: "Need to enable GPS to set Job Location",
                      );
                    }
                  },
                  child: Text(
                    jobLocation?.country == null
                        ? "Set Job Location From Map"
                        : "${jobLocation.name}, ${jobLocation.subLocality}, ${jobLocation.country}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                ),
              ),
              // ref
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 20.0),
                child: const Text(
                  "Reference",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,,
                child: TextField(
                  controller: refCntrlr,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText:
                        "Optional reference to easily identify your posts",
                  ),
                ),
              ),
              // office address
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 20.0),
                child: const Text(
                  "Office Address",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // color: textBoxColor,
                child: TextField(
                  controller: addressCntrlr,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Address of your office",
                  ),
                ),
              ),
              // deadline
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 20.0),
                child: const Text(
                  "Deadline",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Calendar(
                  showCalendarPickerIcon: true,
                  showTodayAction: true,
                  onDateSelected: (dateTime) {
                    setState(() {
                      deadLine = dateTime;
                    });
                  },
                ),
              ),
              InkWell(
                onTap: () async {
                  if (editJobPostormKey.currentState.validate()) {
                    progressDialog.show();

                    bool isConnected =
                        await ConnectivityService().hasConnection();

                    if (isConnected) {
                      // getting image url
                      String postImageUrl = _jobPostImage != null
                          ? await employerRepository
                              .uploadJobImage(_jobPostImage)
                          : widget.jobPostModel.jobImageUrl;

                      if (employerModel != null) {
                        final _timeStamp = FieldValue.serverTimestamp();
                        JobPostModel jobPostModel = JobPostModel(
                          companyName: employerModel.name,
                          companyLogoUrl: employerModel.logoUrl,
                          jobDescription: descCntrlr.text,
                          jobPosition: jobPosCntrlr.text,
                          salaryRange: salaryRangeCntrlr.text,
                          title: titleCntrlr.text,
                          jobCategory: jobCategory,
                          salaryType: salaryType,
                          isValid: true,
                          experience: experienceCntrlr.text,
                          jobType: jobType,
                          schedule: scheduleCntrlr.text,
                          address: addressCntrlr.text,
                          vacancies: int.tryParse(vacanciesCntrlr.text) ?? 1,
                          date: DateFormat('dd-MM-yyyy').format(now),
                          deadline: DateFormat('dd-MM-yyyy').format(deadLine),
                          // location
                          country: jobLocation.country,
                          locality: jobLocation.locality,
                          subLocality: jobLocation.subLocality,
                          name: jobLocation.name, // area name actually,
                          latitude: jobLocation.latitude,
                          longitude: jobLocation.longitude,
                          administrativeArea: jobLocation.administrativeArea,
                          subAdministrativeArea:
                              jobLocation.subAdministrativeArea,
                          thoroughfare: jobLocation.thoroughfare,
                          // image
                          jobImageUrl: postImageUrl,
                        );
                        await employerRepository
                            .updateAJobPost(
                                widget.documentReference, jobPostModel)
                            .then((_) {
                          progressDialog.hide();
                          Fluttertoast.showToast(msg: "Updated successfully");
                          navigateBack(context);
                        }).catchError((e) {
                          progressDialog.hide();
                          Fluttertoast.showToast(
                              msg: "Error while updating : ${e.toString()}");
                        });
                      } else {
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Failed to Update",
                        );
                      }
                    } else {
                      progressDialog.hide();
                      Fluttertoast.showToast(
                        msg: "Failed. Check Network Connection",
                      );
                    }
                  }
                },
                child: Container(
                  height: 60.0,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.blue,
                  ),
                  child: const Text(
                    "Update Job",
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void resetTextControllers() {
    titleCntrlr.text = "";
    descCntrlr.text = "";
    jobPosCntrlr.text = "";
    salaryRangeCntrlr.text = "";
    vacanciesCntrlr.text = "";
    refCntrlr.text = "";
    experienceCntrlr.text = "";
    jobTypeCntrlr.text = "";
    scheduleCntrlr.text = "";
    addressCntrlr.text = "";
  }

  Container choiceChip(String title) {
    return Container(
      padding: const EdgeInsets.all(5.0),
      child: ChoiceChip(
        label: Text(title),
        selected: salaryType == title,
        shape: RoundedRectangleBorder(),
        onSelected: (selected) {
          setState(() {
            salaryType = title;
          });
        },
      ),
    );
  }

  Widget checkbox(String title) {
    return Container(
      padding: const EdgeInsets.only(right: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title),
          Checkbox(
            value: jobCategory == title,
            onChanged: (bool value) {
              /// manage the state of each value
              setState(() {
                switch (title) {
                  case "Engineer":
                    jobCategory = title;
                    break;
                  case "Doctor":
                    jobCategory = title;
                    break;
                  case "Technology":
                    jobCategory = title;
                    break;
                  case "Driver":
                    jobCategory = title;
                    break;
                  case "Others":
                    jobCategory = title;
                    break;
                }
              });
            },
          )
        ],
      ),
    );
  }

  // jobTypeCheckBox
  Widget jobTypeheckbox(String title) {
    return Container(
      padding: const EdgeInsets.only(right: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title),
          Checkbox(
            value: jobType == title,
            onChanged: (bool value) {
              /// manage the state of each value
              setState(() {
                switch (title) {
                  case "Full Time":
                    jobType = title;
                    break;
                  case "Part Time":
                    jobType = title;
                    break;
                }
              });
            },
          )
        ],
      ),
    );
  }

  void getCurrentEmployer() async {
    employerModel = await employerRepository.getCurrentEmployer();
  }

  void navigateBack(BuildContext context) {
    Navigator.pop(context);
  }
}
