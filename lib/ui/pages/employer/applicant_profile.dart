import 'package:flutter/material.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * const checked
 * theme checked
 */
class ApplicantProfile extends StatelessWidget {
  WorkerModel workerModel;

  String notProvidedString = "Not Provided";

  ApplicantProfile({@required this.workerModel});

  @override
  Widget build(BuildContext context) {
    var scrWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Applicant's Profile"),
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.grey),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        width: scrWidth,
        child: ListView(
          children: <Widget>[
            // 1st section pp, name, profession
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: <Widget>[
                  // pp
                  Container(
                    margin: const EdgeInsets.only(right: 20.0),
                    height: 70,
                    width: 70,
                    child: buildPicHolder(
                      url: workerModel.profilPicUrl,
                      radius: 70.0,
                      isWorker: false,
                    ),
                  ),
                  // name and profession
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      // name
                      Container(
                        width: (scrWidth / 3) * 2,
                        child: Text(
                          Helper.isNullOrEmpty(workerModel.name)
                              ? "N/A"
                              : workerModel.name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.0,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                      // profession
                      Container(
                        width: (scrWidth / 3) * 2,
                        child: Text(
                          workerModel.profession != null
                              ? workerModel.profession
                              : "Not Specified",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // 2nd section phone
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  // icon
                  const Icon(
                    Icons.phone,
                    size: 30.0,
                  ),
                  const SizedBox(width: 45.0),
                  // text
                  Container(
                    width: (scrWidth / 3) * 2,
                    child: Text(
                      workerModel.phoneNumber != null
                          ? workerModel.phoneNumber.toString()
                          : "Not Provided",
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
            ),
            // 3rd section email
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  // icon
                  const Icon(
                    Icons.email,
                    size: 30.0,
                  ),
                  const SizedBox(width: 45.0),
                  // text
                  Text(
                    workerModel.email,
                    style: const TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            const Divider(height: 40.0),
            // 4th section about
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  // icon
                  const Icon(
                    Icons.info,
                    size: 30.0,
                  ),
                  const SizedBox(width: 45.0),
                  Column(
                    children: <Widget>[
                      // title
                      Container(
                        width: (scrWidth / 3) * 2,
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: const Text(
                          "About",
                          style: const TextStyle(fontSize: 18.0),
                        ),
                      ),
                      // about text
                      Container(
                        width: (scrWidth / 3) * 2,
                        child: Text(
                          workerModel.about != null
                              ? workerModel.about
                              : notProvidedString,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 20,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(height: 40.0),
            // 5th section location
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  // icon
                  const Icon(
                    Icons.place,
                    size: 30.0,
                  ),
                  const SizedBox(width: 45.0),
                  Column(
                    children: <Widget>[
                      // title
                      Container(
                        width: (scrWidth / 3) * 2,
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: const Text(
                          "Location",
                          style: const TextStyle(fontSize: 18.0),
                        ),
                      ),
                      // about text
                      Container(
                        width: (scrWidth / 3) * 2,
                        child: Text(
                          workerModel.location != null
                              ? workerModel.location
                              : notProvidedString,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(height: 40.0),
            // 6th section skills
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  // icon
                  const Icon(
                    Icons.work,
                    size: 30.0,
                  ),
                  const SizedBox(width: 45.0),
                  Column(
                    children: <Widget>[
                      // title
                      Container(
                        width: (scrWidth / 3) * 2,
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: const Text(
                          "Skills",
                          style: const TextStyle(fontSize: 18.0),
                        ),
                      ),
                      // about text
                      Container(
                        width: (scrWidth / 3) * 2,
                        child: Text(
                          workerModel.skills != null
                              ? workerModel.skills
                              : notProvidedString,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 10,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}