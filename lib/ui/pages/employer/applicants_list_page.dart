import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/employerBloc/applicantsBloc/applicants_bloc.dart';
import 'package:jobs/blocs/employerBloc/applicantsBloc/applicants_event.dart';
import 'package:jobs/blocs/employerBloc/applicantsBloc/applicants_state.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/ui/pages/employer/applicant_profile.dart';
import 'package:jobs/ui/widgets/failures/failure_ui.dart';
import 'package:jobs/ui/widgets/loadings/applicants_loading_ui.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

/**
 * const checked
 * theme checked
 */

class ApplicantsListPageParent extends StatelessWidget {
  final DocumentReference documentReference;

  ApplicantsListPageParent({@required this.documentReference});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ApplicantsBloc>(
      create: (context) => ApplicantsBloc(),
      child: ApplicantsListPage(
        documentReference: documentReference,
      ),
    );
  }
}

class ApplicantsListPage extends StatefulWidget {
  final DocumentReference documentReference;

  ApplicantsListPage({@required this.documentReference});

  @override
  _ApplicantsListPageState createState() => _ApplicantsListPageState();
}

class _ApplicantsListPageState extends State<ApplicantsListPage> {
  final String _TAG = "_ApplicantsListPageState";
  ApplicantsBloc _applicantsBloc;

  @override
  void initState() {
    super.initState();
    _applicantsBloc = BlocProvider.of<ApplicantsBloc>(context);
    _applicantsBloc
        .add(FetchApplicantsId(documentReference: widget.documentReference));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Applicants"),
      ),
      body: BlocBuilder<ApplicantsBloc, ApplicantsState>(
        builder: (context, state) {
          if (state is ApplicantsInitial) {
            Helper.logPrint(_TAG, state.toString());
            return const SizedBox(height: 0.0);
          } else if (state is ApplicantsLoading) {
            Helper.logPrint(_TAG, state.toString());
            return const ApplicantsLoadingUi();
          } else if (state is ApplicantsLoaded) {
            Helper.logPrint(_TAG, state.toString());
            return _ApplicantsLoadedUi(
              applicants: state.applicants,
            );
          } else if (state is ApplicantsLoadFailure) {
            Helper.logPrint(_TAG, state.toString());
            return FailureUi(message: state.message);
          }
        },
      ),
    );
  }
}

class _ApplicantsLoadedUi extends StatelessWidget {
  final List<WorkerModel> applicants;

  _ApplicantsLoadedUi({Key key, @required this.applicants});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: applicants.length,
      itemBuilder: (context, position) {
        WorkerModel model = applicants[position];
        return Material(
          child: InkWell(
            onTap: () {
              Get.to(ApplicantProfile(workerModel: model));
            },
            child: ListTile(
              leading: CircleAvatar(
                // backgroundColor: Colors.grey[300],
                child: buildPicHolder(
                  isWorker: true,
                  radius: 50.0,
                  url: model.profilPicUrl,
                ),
              ),
              title: Text(model.name == null ? "N/A" : model.name),
              subtitle: Text(model.email),
            ),
          ),
        );
      },
    );
  }
}

// class ApplicantsListPage extends StatefulWidget {
//   List<String> applicantsIds;

//   ApplicantsListPage({@required this.applicantsIds});

//   @override
//   _ApplicantsListPageState createState() => _ApplicantsListPageState();
// }

// class _ApplicantsListPageState extends State<ApplicantsListPage> {
//   EmployerRepository employerRepository;

//   _ApplicantsListPageState() {
//     employerRepository = EmployerRepository();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Applicants"),
//         centerTitle: true,
//         iconTheme: const IconThemeData(color: Colors.grey),
//       ),
//       body: FutureBuilder(
//         future: employerRepository.getListOfApplications(widget.applicantsIds),
//         builder: (context, snapshot) {
//           if (snapshot.hasData) {
// return ListView.builder(
//   itemCount: snapshot.data.length,
//   itemBuilder: (context, position) {
//     WorkerModel model = snapshot.data[position];
//     return Material(
//       child: InkWell(
//         onTap: () {
//           navigateToApplicantProfilePage(context, model);
//         },
//         child: ListTile(
//           leading: CircleAvatar(
//             // backgroundColor: Colors.grey[300],
//             child: buildPicHolder(
//               isWorker: false,
//               radius: 50.0,
//               url: model.profilPicUrl,
//             ),
//           ),
//           title: Text(model.name == null ? "N/A" : model.name),
//           subtitle: Text(model.email),
//         ),
//       ),
//     );
//   },
// );
//           } else {
//             return buildLoadingUi();
//           }
//         },
//       ),
//     );
//   }

//   Widget buildLoadingUi() {
//     return const Center(
//       child: const Text("Loading ... ..."),
//     );
//   }

//   void navigateToApplicantProfilePage(
//       BuildContext context, WorkerModel workerModel) {
//     Navigator.of(context).push(MaterialPageRoute(builder: (context) {
//       return ApplicantProfile(workerModel: workerModel);
//     }));
//   }
// }
