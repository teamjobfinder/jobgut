import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/data/repositories/employerRepository/employer_repository.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/job_experiences.dart';
import 'package:jobs/res/language_codes.dart';
import 'package:jobs/res/strings/ad_strings.dart';
import 'package:jobs/res/strings/job_categories.dart';
import 'package:jobs/ui/temp/temp_map.dart';
import 'package:jobs/ui/widgets/blank_image_placeholder.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:jobs/utils/location_service.dart';
import 'package:progress_dialog/progress_dialog.dart';

// TODO Bloc later
class PostJobPageParent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PostJobPage();
  }
}

class PostJobPage extends StatefulWidget {
  @override
  _PostJobPageState createState() => _PostJobPageState();
}

class _PostJobPageState extends State<PostJobPage> {
  // default salarytype
  String salaryType = "Monthly";
  // default job category
  String jobCategory = "Others";
  // default job type
  String jobType = "Full Time";
  // default preferred Gender
  String preferredGender = "Male";
  // default job language
  String jobLanguage = "English";
  // default experience
  String jobExperience = "1 Year";
  File _jobPostImage;

  var formKey = GlobalKey<FormState>();
  LocationService locationService;

  TextEditingController titleCntrlr = TextEditingController();
  TextEditingController descCntrlr = TextEditingController();
  TextEditingController jobPosCntrlr = TextEditingController();
  TextEditingController salaryRangeCntrlr = TextEditingController();
  TextEditingController vacanciesCntrlr = TextEditingController();
  TextEditingController refCntrlr = TextEditingController();
  TextEditingController experienceCntrlr = TextEditingController();
  TextEditingController jobTypeCntrlr = TextEditingController();
  TextEditingController scheduleCntrlr = TextEditingController();
  TextEditingController addressCntrlr = TextEditingController();

  DateTime now = DateTime.now();
  // deaflut deadline
  DateTime deadLine = DateTime.now();

  EmployerRepository employerRepository;
  EmployerModel employerModel;

  ProgressDialog progressDialog;

  TheLocation jobLocation = TheLocation();

  // for ad
  // AdService _adService = AdService();
  // InterstitialAd _interstitialAd;

  _PostJobPageState() {
    employerRepository = EmployerRepository();
    locationService = LocationService();
  }

  @override
  void initState() {
    super.initState();
    // FirebaseAdMob.instance.initialize(appId: AdStrings.admobAppId);

    progressDialog = ProgressDialog(context);
    getCurrentEmployer();
    // _interstitialAd = _adService.createInterstitialAd()..load();
  }

  @override
  void dispose() {
    titleCntrlr.dispose();
    descCntrlr..dispose();
    jobPosCntrlr.dispose();
    salaryRangeCntrlr.dispose();
    vacanciesCntrlr.dispose();
    refCntrlr.dispose();
    experienceCntrlr.dispose();
    scheduleCntrlr.dispose();
    jobTypeCntrlr.dispose();
    addressCntrlr.dispose();
    // _interstitialAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.blue[300],
        title: Text(
          "Post Job",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.camera_alt,
              color: Colors.white,
            ),
            onPressed: () async {
              print("cam p");
              var image = await Helper.pickImageFromGallery();
              setState(() {
                _jobPostImage = image;
              });
              print("image pickedd");
            },
          ),
        ],
      ),
      body: Form(
        key: formKey,
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: SingleChildScrollView(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Stack(
                    children: <Widget>[
                      // job image
                      Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30.0),
                          child: _jobPostImage != null
                              ? Image.file(
                                  _jobPostImage,
                                  width: Helper.getScreenWidth(context),
                                  height: 200.0,
                                  fit: BoxFit.cover,
                                )
                              : BlankImagePlaceHolder(
                                  text: "Add An Image Related To Job",
                                  height: 100.0,
                                  width: Helper.getScreenWidth(context),
                                  onClick: () async {
                                    print("cam p");
                                    var image =
                                        await Helper.pickImageFromGallery();
                                    setState(() {
                                      _jobPostImage = image;
                                    });
                                  },
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                // title
                headerTextUi("Job Title"),
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: TextFormField(
                    validator: (string) {
                      if (string.isEmpty) {
                        return "Write a job title";
                      }
                    },
                    style: TextStyle(color: Colors.white),
                    controller: titleCntrlr,
                    decoration: textFieldDecoration("Job Title"),
                  ),
                ),
                headerTextUi("Job Description"),
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: TextFormField(
                    validator: (string) {
                      if (string.isEmpty) {
                        return "Write a job description";
                      }
                    },
                    style: TextStyle(color: Colors.white),
                    controller: descCntrlr,
                    decoration: textFieldDecoration("Job Description"),
                    minLines: 10,
                    maxLines: 20,
                  ),
                ),
                headerTextUi("Job Position"),
                buildTextField(jobPosCntrlr, "Job Position"),
                headerTextUi("Salary Range"),
                buildTextField(salaryRangeCntrlr, "Salary Range"),
                Wrap(
                  children: <Widget>[
                    choiceChip("Hourly"),
                    choiceChip("Daily"),
                    choiceChip("Weekly"),
                    choiceChip("Monthly"),
                    choiceChip("Yearly"),
                  ],
                ),
                buildLanguageUi(),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: headerTextUi("Job Schedule"),
                ),
                buildTextField(scheduleCntrlr, "Job Schedule"),
                // job category
                Container(
                  padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Text(
                    "Job Category",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                // job categories
                Container(
                  child: Wrap(
                    children: <Widget>[
                      checkbox(JobCategories.designer),
                      checkbox(JobCategories.uiux),
                      checkbox(JobCategories.engineer),
                      checkbox(JobCategories.doctor),
                      checkbox(JobCategories.chef),
                      checkbox(JobCategories.cleaning),
                      checkbox(JobCategories.warehouse),
                      checkbox(JobCategories.official),
                      checkbox(JobCategories.salesAndMarketing),
                      checkbox(JobCategories.driverAndCourier),
                      checkbox(JobCategories.construction),
                      checkbox(JobCategories.teacher),
                      checkbox(JobCategories.lawyer),
                      checkbox(JobCategories.photographer),
                      checkbox(JobCategories.receptionist),
                      checkbox(JobCategories.customerSupport),
                      checkbox(JobCategories.eventManagement),
                      checkbox(JobCategories.waiter),
                      checkbox(JobCategories.bartender),
                      checkbox(JobCategories.kitchen_helper),
                      checkbox(JobCategories.salesman),
                      checkbox(JobCategories.cosmetics),
                      checkbox(JobCategories.hair_designer),
                      checkbox(JobCategories.barista),
                      checkbox(JobCategories.language_tutor),
                      checkbox(JobCategories.others),
                    ],
                  ),
                ),
                Divider(
                  height: 20.0,
                  thickness: 1.0,
                ),
                // job type
                Container(
                  padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Text(
                    "Job Type",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  child: Wrap(
                    children: <Widget>[
                      jobTypeheckbox(AppStrings.fullTime),
                      SizedBox(width: 20.0),
                      jobTypeheckbox(AppStrings.partTime),
                      SizedBox(width: 20.0),
                      jobTypeheckbox(AppStrings.freelancer),
                    ],
                  ),
                ),
                // preferred gender
                Container(
                  padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Text(
                    "Preferred Gender",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  child: Wrap(
                    children: <Widget>[
                      preferredGenderCheckBox(AppStrings.male),
                      SizedBox(width: 20.0),
                      preferredGenderCheckBox(AppStrings.female),
                      SizedBox(width: 20.0),
                      preferredGenderCheckBox(AppStrings.any),
                    ],
                  ),
                ),
                // vacancies
                headerTextUi("Vacancies"),
                buildTextField(vacanciesCntrlr, "Vacancies"),
                // experience
                buildExperienceUi(),
                // location
                Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    "Job Location",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  height: 50.0,
                  width: MediaQuery.of(context).size.width - 30.0,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.blue,
                        Colors.blue[400],
                      ],
                    ),
                  ),
                  child: InkWell(
                    onTap: () async {
                      var gpsStatus = await locationService.isGpsOn();
                      if (gpsStatus) {
                        jobLocation = await Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return JobMaps(fromWorkerContext: false);
                        }));
                      } else {
                        Fluttertoast.showToast(
                          msg: "Need to enable GPS to set Job Location",
                        );
                      }
                    },
                    child: Text(
                      jobLocation?.country == null
                          ? "Set Job Location From Map"
                          : "${jobLocation.name}, ${jobLocation.subLocality}, ${jobLocation.country}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                headerTextUi("Refrence"),
                buildTextField(refCntrlr, "Reference"),
                headerTextUi("Office Address"),
                buildTextField(addressCntrlr, "Office Address"),
                // deadline
                Container(
                  padding: EdgeInsets.only(bottom: 10.0, top: 20.0),
                  child: Text(
                    "Deadline",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Calendar(
                    showCalendarPickerIcon: true,
                    showTodayAction: true,
                    onDateSelected: (dateTime) {
                      setState(() {
                        deadLine = dateTime;
                      });
                    },
                  ),
                ),
                InkWell(
                  onTap: () async {
                    bool isVerified =
                        await employerRepository.isEmployerVerified();

                    if (!isVerified) {
                      Fluttertoast.showToast(
                          msg: "Verify Your Phone Number First",
                          toastLength: Toast.LENGTH_LONG);
                      resetTextControllers();
                      return;
                    }

                    if (formKey.currentState.validate()) {
                      print("PG : Inside form");
                      progressDialog.show();

                      var isConnected =
                          await ConnectivityService().hasConnection();

                      if (isConnected) {
                        String jobImageUrl = _jobPostImage != null
                            ? await employerRepository
                                .uploadJobImage(_jobPostImage)
                            : "";

                        JobPostModel jobPostModel = JobPostModel(
                          companyName: employerModel.name,
                          companyLogoUrl: employerModel.logoUrl,
                          jobDescription: descCntrlr.text,
                          jobPosition: jobPosCntrlr.text,
                          salaryRange: salaryRangeCntrlr.text,
                          title: titleCntrlr.text,
                          jobCategory: jobCategory,
                          salaryType: salaryType,
                          isValid: true,
                          experience: jobLanguage,
                          jobType: jobType,
                          schedule: scheduleCntrlr.text,
                          address: addressCntrlr.text,
                          vacancies: int.tryParse(vacanciesCntrlr.text) ?? 1,
                          date: DateFormat('dd-MM-yyyy').format(now),
                          deadline: DateFormat('dd-MM-yyyy').format(deadLine),
                          // location
                          country: jobLocation.country,
                          locality: jobLocation.locality,
                          subLocality: jobLocation.subLocality,
                          name: jobLocation.name, // area name actually,
                          latitude: jobLocation.latitude,
                          longitude: jobLocation.longitude,
                          administrativeArea: jobLocation.administrativeArea,
                          subAdministrativeArea:
                              jobLocation.subAdministrativeArea,
                          thoroughfare: jobLocation.thoroughfare,
                          // job image
                          jobImageUrl: jobImageUrl,
                          language: jobLanguage,
                          preferredGender: preferredGender,
                          nationality: employerModel.nationality,
                        );
                        print("PG : Connected");
                        await employerRepository
                            .postAJob(jobPostModel)
                            .then((_) {
                          resetTextControllers();
                          print("PG : suceess");
                          progressDialog.hide();
                          Fluttertoast.showToast(
                            msg: "successfully posted a job",
                            backgroundColor: Colors.blue,
                          );
                          // _interstitialAd?.show();
                          navigateBack(context);
                        }).catchError((e) {
                          print("PG : error");
                          print("Error while updating ${e.toString()}");
                          progressDialog.hide();
                          Fluttertoast.showToast(
                            msg: "Error ${e.toString()}",
                            backgroundColor: Colors.red,
                          );
                        });
                      } else {
                        print("PG : Not Connected");
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Unable to Post. Check Network Connection",
                        );
                      }
                      progressDialog.hide();
                    }
                  },
                  child: Container(
                    height: 60.0,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.blue,
                    ),
                    child: Text(
                      "Post Job",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getCurrentEmployer() async {
    print("CAlled");
    employerModel = await employerRepository.getCurrentEmployer();
    print("EMPPPPPP : ${employerModel.name}");
  }

  void resetTextControllers() {
    titleCntrlr.text = "";
    descCntrlr.text = "";
    jobPosCntrlr.text = "";
    salaryRangeCntrlr.text = "";
    vacanciesCntrlr.text = "";
    refCntrlr.text = "";
    experienceCntrlr.text = "";
    jobTypeCntrlr.text = "";
    scheduleCntrlr.text = "";
    addressCntrlr.text = "";
  }

  // decoration for textfields
  InputDecoration textFieldDecoration(String hintText) {
    return InputDecoration(
      filled: true,
      fillColor: Colors.blue[400],
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.white70),
      border: OutlineInputBorder(),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
      ),
    );
  }

  Widget headerTextUi(String text) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
        alignment: Alignment.centerLeft,
        width: 120.0,
        margin: EdgeInsets.only(bottom: 5.0),
        color: Color(0xffc5cad3),
        child: Text(
          text,
          style: TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget buildTextField(TextEditingController controller, String text) {
    return Container(
      margin: EdgeInsets.only(bottom: 15.0),
      child: TextField(
        controller: controller,
        decoration: textFieldDecoration(text),
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Container choiceChip(String title) {
    return Container(
      color: Colors.red,
      padding: EdgeInsets.all(5.0),
      child: ChoiceChip(
        // label: Text(title),
        label: Container(
          width: Helper.screenWidthPortion(context, time: 0.2),
          height: Helper.screenWidthPortion(context, time: 0.3),
          child: Column(
            children: <Widget>[
              // image
              Container(
                alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundColor: Colors.green,
                ),
              ),
              // text
              Text(title)
            ],
          ),
        ),
        selected: salaryType == title,
        shape: RoundedRectangleBorder(),
        onSelected: (selected) {
          setState(() {
            salaryType = title;
          });
        },
      ),
    );
  }

  Widget checkbox(String title) {
    return Container(
      padding: EdgeInsets.only(right: 5.0),
      margin: EdgeInsets.only(right: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title),
          Checkbox(
            value: jobCategory == title,
            onChanged: (bool value) {
              /// manage the state of each value
              setState(() {
                switch (title) {
                  case JobCategories.designer:
                    jobCategory = title;
                    break;
                  case JobCategories.uiux:
                    jobCategory = title;
                    break;
                  case JobCategories.engineer:
                    jobCategory = title;
                    break;
                  case JobCategories.doctor:
                    jobCategory = title;
                    break;
                  case JobCategories.chef:
                    jobCategory = title;
                    break;
                  case JobCategories.cleaning:
                    jobCategory = title;
                    break;
                  case JobCategories.warehouse:
                    jobCategory = title;
                    break;
                  case JobCategories.official:
                    jobCategory = title;
                    break;
                  case JobCategories.salesAndMarketing:
                    jobCategory = title;
                    break;
                  case JobCategories.driverAndCourier:
                    jobCategory = title;
                    break;
                  case JobCategories.construction:
                    jobCategory = title;
                    break;
                  case JobCategories.teacher:
                    jobCategory = title;
                    break;
                  case JobCategories.lawyer:
                    jobCategory = title;
                    break;
                  case JobCategories.photographer:
                    jobCategory = title;
                    break;
                  case JobCategories.receptionist:
                    jobCategory = title;
                    break;
                  case JobCategories.customerSupport:
                    jobCategory = title;
                    break;
                  case JobCategories.eventManagement:
                    jobCategory = title;
                    break;
                  case JobCategories.waiter:
                    jobCategory = title;
                    break;
                  case JobCategories.bartender:
                    jobCategory = title;
                    break;
                  case JobCategories.kitchen_helper:
                    jobCategory = title;
                    break;
                  case JobCategories.salesman:
                    jobCategory = title;
                    break;
                  case JobCategories.cosmetics:
                    jobCategory = title;
                    break;
                  case JobCategories.hair_designer:
                    jobCategory = title;
                    break;
                  case JobCategories.barista:
                    jobCategory = title;
                    break;
                  case JobCategories.language_tutor:
                    jobCategory = title;
                    break;
                  case JobCategories.others:
                    jobCategory = title;
                    break;
                }
              });
            },
          )
        ],
      ),
    );
  }

  // jobTypeCheckBox
  Widget jobTypeheckbox(String title) {
    return Container(
      padding: EdgeInsets.only(right: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title),
          Checkbox(
            value: jobType == title,
            onChanged: (_) {
              /// manage the state of each value
              setState(() {
                switch (title) {
                  case AppStrings.fullTime:
                    jobType = title;
                    break;
                  case AppStrings.partTime:
                    jobType = title;
                    break;
                  case AppStrings.freelancer:
                    jobType = title;
                    break;
                }
              });
            },
          )
        ],
      ),
    );
  }

  // preferredGender checkbox
  Widget preferredGenderCheckBox(String gender) {
    return Container(
      padding: EdgeInsets.only(right: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(gender),
          Checkbox(
            value: preferredGender == gender,
            onChanged: (_) {
              /// manage the state of each value
              setState(() {
                switch (gender) {
                  case AppStrings.male:
                    preferredGender = gender;
                    break;
                  case AppStrings.female:
                    preferredGender = gender;
                    break;
                  case AppStrings.any:
                    preferredGender = gender;
                    break;
                }
              });
            },
          )
        ],
      ),
    );
  }

  void navigateBack(BuildContext context) {
    Navigator.pop(context);
  }

  Widget buildLanguageUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        headerTextUi("Job Language"),
        DropdownButton(
          items: LanguageCodes.jobLanguages.map((language) {
            return DropdownMenuItem(
              value: language,
              child: Text(language),
            );
          }).toList(),
          onChanged: (changedLanguage) async {
            setState(() {
              this.jobLanguage = changedLanguage;
            });
          },
          value: jobLanguage,
        ),
      ],
    );
  }

  Widget buildExperienceUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        headerTextUi("Experience"),
        DropdownButton(
          items: JobExperience.experiences.map((experience) {
            return DropdownMenuItem(
              value: experience,
              child: Text(experience),
            );
          }).toList(),
          onChanged: (changedExperience) async {
            setState(() {
              this.jobExperience = changedExperience;
            });
          },
          value: jobExperience,
        ),
      ],
    );
  }
}
