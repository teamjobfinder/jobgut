import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/strings/app_string_constants.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:jobs/ui/widgets/feed_thumbnail.dart';
import 'package:meta/meta.dart';

class NewSearchPage extends StatelessWidget {
  final String nationality;
  final String language;

  const NewSearchPage(
      {Key key, @required this.nationality, @required this.language})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Jobs"),
        centerTitle: true,
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .collectionGroup("posts")
            .where(AppStringConstants.nationality, isEqualTo: nationality)
            .where(AppStringConstants.language, isEqualTo: language)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return _buildErrorui(snapshot.error.toString());
          }
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, position) {
                DocumentSnapshot docSnap = snapshot.data.documents[position];
                DocumentReference docRef = docSnap.reference;
                JobPostModel model = JobPostModel.fromSnapshot(
                    snapshot.data.documents[position]);
                JobPostDocumentModel jobPostDocumentModel =
                    JobPostDocumentModel(
                  documentReference: docRef,
                  documentSnapshot: docSnap,
                  jobPostModel: model,
                );
                return InkWell(
                  onTap: () {
                    Get.to(PostDetailsPageParent(
                      isWorker: true,
                      jobPostModel: model,
                      documentReference: jobPostDocumentModel.documentReference,
                      documentSnapshot: docSnap,
                    ));
                  },
                  child: FeedThumbnail(
                    jobPostModel: model,
                  ),
                );
              },
            );
          } else {
            return Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(
                  "No job found for the applied filters : $nationality nationality and $language language"),
            );
          }
        },
      ),
    );
  }

  Widget _buildLoadingUi() {
    return const Center(
      child: const Text("Loading. Please Wait ... ..."),
    );
  }

  Widget _buildErrorui(String message) {
    return Center(
      child: Text(
        "Error : $message",
        style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
