import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:meta/meta.dart';

class LocationSearchResultPage extends StatefulWidget {
  String location;

  LocationSearchResultPage({@required this.location});

  @override
  _LocationSearchResultPageState createState() =>
      _LocationSearchResultPageState();
}

// TODO
// See if it can be replace by readJobsInALocation() in worker_service.dart
class _LocationSearchResultPageState extends State<LocationSearchResultPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: StreamBuilder(
          stream: Firestore.instance
              .collectionGroup("posts")
              .orderBy(AppStrings.workerLocation)
              .where(AppStrings.workerLocation,
                  isGreaterThanOrEqualTo: widget.location)
              .where(AppStrings.workerLocation,
                  isLessThanOrEqualTo: widget.location + "z")
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              print("ERROR : ${snapshot.error}");
            }
            if (snapshot.hasData) {
              print("DATA : Loaded");
              print("LOC : ${snapshot.data.documents.length}");
            } else {
              print("LAOding");
            }
            return Container();
          },
        ),
      ),
    );
  }
}
