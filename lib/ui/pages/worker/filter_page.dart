import 'package:flutter/material.dart';
import 'package:jobs/ui/pages/worker/filterJob/category_type_filter_page.dart';
import 'package:jobs/ui/pages/worker/filterJob/search_job_page.dart';

class FilterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,         
          title: const Text("Filters"),
          centerTitle: true,
          bottom: TabBar(
            tabs: <Widget>[
              const Text(
                "Category & Type",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textScaleFactor: 1.0,
              ),
              const Text(
                "Nationality",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textScaleFactor: 1.0,
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            CategoryTypeFilterPage(),
            SearchJobPageParent(),
          ],
        ),
      ),
    );
  }
}
