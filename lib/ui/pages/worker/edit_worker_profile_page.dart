import 'dart:io';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/res/colors/app_colors.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/widgets/country_picker.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class EditWorkerProfilePage extends StatefulWidget {
  WorkerModel workerModel;

  EditWorkerProfilePage({@required this.workerModel});

  @override
  _EditWorkerProfilePageState createState() => _EditWorkerProfilePageState();
}

class _EditWorkerProfilePageState extends State<EditWorkerProfilePage> {
  Color textBoxColor = AppColors.textBoxColor;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController aboutController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController professionController = TextEditingController();
  TextEditingController skillsController = TextEditingController();

  WorkerRepository workerRepository = WorkerRepository();
  UsersRepository usersRepository = UsersRepository();

  // for phone number
  CountryCode country = CountryCode(
    code: "KR",
    dialCode: "+82",
    flagUri: "flags/kr.png",
    name: "대한민국",
  );

  var editWorkerProfileFormKey = GlobalKey<FormState>();
  // default gender
  String gender = "Male";

  File _profileImage;
  ProgressDialog progressDialog;

  Future pickImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _profileImage = image;
    });
  }

  @override
  void initState() {
    super.initState();
    nameController.text = widget.workerModel.name;
    emailController.text = widget.workerModel.email;
    phoneController.text = widget.workerModel.phoneNumber.toString();
    aboutController.text = widget.workerModel.about;
    locationController.text = widget.workerModel.location;
    professionController.text = widget.workerModel.profession;
    skillsController.text = widget.workerModel.skills;
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    aboutController.dispose();
    locationController.dispose();
    professionController.dispose();
    skillsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Profile"),
        centerTitle: true,
      ),
      body: Form(
        key: editWorkerProfileFormKey,
        child: Material(
          child: ListView(
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              // pp
              Align(
                alignment: Alignment.center,
                child: Stack(
                  children: <Widget>[
                    Container(
                      child: ClipOval(
                          child: _profileImage != null
                              ? Image.file(
                                  _profileImage,
                                  width: 150.0,
                                  height: 150.0,
                                  fit: BoxFit.cover,
                                )
                              : buildPicHolder(
                                  radius: 130.0,
                                  url: widget.workerModel.profilPicUrl,
                                  isWorker: true,
                                )),
                    ),
                    Positioned(
                      bottom: 20.0,
                      right: 10.0,
                      child: IconButton(
                        icon: const Icon(
                          Icons.camera_alt,
                          size: 50.0,
                          color: Colors.grey,
                        ),
                        onPressed: () async {
                          await pickImageFromGallery();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              // name
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Your Name",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write your name";
                    }
                  },
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Your Name",
                  ),
                ),
              ),
              // profession
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Profession",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  controller: professionController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Your Profession or Field You are Expert In",
                  ),
                ),
              ),
              // About
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "About You",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  controller: aboutController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Write Something About You",
                  ),
                  minLines: 10,
                  maxLines: 20,
                ),
              ),
              // email
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Email",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  validator: (string) {
                    if (string.isEmpty) {
                      return "Write your email";
                    }
                  },
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Your Email",
                  ),
                ),
              ),
              // location
              Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Location",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  controller: locationController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Your Location",
                  ),
                ),
              ),

              // gender
              Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(right: 20.0),
                    child: const Text(
                      "Gender",
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  DropdownButton(
                    items: AppStrings.genders.map((gender) {
                      return DropdownMenuItem(
                        value: gender,
                        child: Text(gender),
                      );
                    }).toList(),
                    onChanged: (cGender) async {
                      setState(() {
                        this.gender = cGender;
                      });
                    },
                    value: gender,
                  ),
                ],
              ),

              // phone number
              Container(
                padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                child: const Text(
                  "Phone Number",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  CountryPicker(
                    initialCountryCode: widget.workerModel.countryCode != null
                        ? widget.workerModel.countryCode
                        : country.dialCode,
                    onChanged: (c) {
                      setState(() {
                        country = c;
                      });
                    },
                  ),
                  const SizedBox(width: 5.0),
                  Container(
                    width: 200.0,
                    child: TextFormField(
                      controller: phoneController,
                      decoration: const InputDecoration(
                        border: const OutlineInputBorder(),
                        hintText: "Phone Number",
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                  ),
                ],
              ),
              // skills
              Container(
                padding: EdgeInsets.only(bottom: 10.0),
                child: const Text(
                  "Skills You Have",
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: TextFormField(
                  controller: skillsController,
                  decoration: const InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: "Your Skills",
                  ),
                ),
              ),
              // button
              InkWell(
                onTap: () async {
                  if (editWorkerProfileFormKey.currentState.validate()) {
                    progressDialog.show();

                    String dpUrl = _profileImage != null
                        ? await usersRepository
                            .uploadProfileImageToDatabase(_profileImage)
                        : widget.workerModel.profilPicUrl;

                    // update profile
                    WorkerModel wrkrModel = WorkerModel(
                      gender: gender,
                      profilPicUrl: dpUrl,
                      name: nameController.text,
                      email: emailController.text,
                      location: locationController.text.toString(),
                      about: aboutController.text,
                      phoneNumber: int.tryParse(phoneController.text),
                      profession: professionController.text,
                      skills: skillsController.text,
                      countryCode: Helper.isNullOrEmpty(country.dialCode)
                          ? widget.workerModel.countryCode
                          : country.dialCode,
                    );

                    bool isConnected =
                        await ConnectivityService().hasConnection();

                    if (isConnected) {
                      await workerRepository
                          .updateWorkerProfile(wrkrModel)
                          .then((_) {
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Successfully Updated",
                          backgroundColor: Colors.blue,
                        );
                        Navigator.pop(context);
                      }).catchError((e) {
                        progressDialog.hide();
                        Fluttertoast.showToast(
                          msg: "Error : ${e.toString()}",
                          backgroundColor: Colors.red,
                        );
                      });
                    } else {
                      progressDialog.hide();
                      Fluttertoast.showToast(
                        msg: "Check Internet Connection",
                      );
                    }
                  }
                },
                child: Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  height: 60.0,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.blue,
                  ),
                  child: const Text(
                    "Update Profile",
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
