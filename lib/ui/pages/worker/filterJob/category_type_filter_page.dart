import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/res/strings/job_categories.dart';
import 'package:jobs/ui/pages/worker/filtered_job_feed_page.dart';
import 'package:jobs/ui/widgets/custom_choice_chip.dart';
import 'package:jobs/utils/helper.dart';

class CategoryTypeFilterPage extends StatefulWidget {
  @override
  _CategoryTypeFilterPageState createState() => _CategoryTypeFilterPageState();
}

class _CategoryTypeFilterPageState extends State<CategoryTypeFilterPage> {
  bool chipSelected = false;

  String selectedJobTYpe = AppStrings.fullTime;

  String _jobCategory = "Designer";

  Widget _buildCategoriesChipWidget(bool isLarge) {
    return GridView.count(
      crossAxisCount: 3,
      children: <Widget>[
        CustomChoiceChip(
          title: JobCategories.designer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.borderStyle,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.designer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.uiux,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.uikit,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.uiux;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.engineer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.stackExchange,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.engineer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.doctor,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.hospital,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.doctor;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.chef,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.cookie,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.chef;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.cleaning,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.handsWash,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.cleaning;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.warehouse,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.warehouse,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.warehouse;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.official,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.suitcase,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.official;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.salesAndMarketing,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.salesforce,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.salesAndMarketing;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.driverAndCourier,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.truck,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.driverAndCourier;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.construction,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.building,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.construction;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.teacher,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.chalkboardTeacher,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.teacher;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.lawyer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.male,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.lawyer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.photographer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.camera,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.photographer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.receptionist,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.female,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.receptionist;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.customerSupport,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.phoneAlt,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.customerSupport;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.eventManagement,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.moneyBillWave,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.eventManagement;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.waiter,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.restaurant,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.waiter;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.bartender,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.glasses,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.bartender;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.kitchen_helper,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.kitchen,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.kitchen_helper;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.salesman,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.mail_outline,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.salesman;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.cosmetics,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.party_mode,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.cosmetics;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.hair_designer,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.party_mode,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.hair_designer;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.barista,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.beer,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.barista;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.language_tutor,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: FontAwesomeIcons.language,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.language_tutor;
            });
          },
        ),
        CustomChoiceChip(
          title: JobCategories.others,
          defTitle: _jobCategory,
          isLarge: isLarge,
          iconData: Icons.devices_other,
          onSelected: (_) {
            setState(() {
              _jobCategory = JobCategories.others;
            });
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 50.0),
            child: ListView(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
              children: <Widget>[
                // 1st filter. category selection.
                Container(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: const Text(
                    "Job Category",
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Divider(height: 20.0),
                Container(
                  height: Helper.screenHeightPortion(context, time: 0.7),
                  child: _buildCategoriesChipWidget(true),
                ),
                // 2nd filter. job type selection
                Container(
                  padding: EdgeInsets.only(top: 10.0),
                  child: const Text(
                    "Job Type",
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Divider(height: 20.0),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      // part time
                      Container(
                        height: 30.0,
                        child: Row(
                          children: <Widget>[
                            const Text(AppStrings.partTime),
                            Checkbox(
                              value: selectedJobTYpe == AppStrings.partTime,
                              onChanged: (bool value) {
                                setState(() {
                                  selectedJobTYpe = AppStrings.partTime;
                                });
                              },
                            )
                          ],
                        ),
                      ),
                      // full time
                      Container(
                        height: 30.0,
                        child: Row(
                          children: <Widget>[
                            const Text(AppStrings.fullTime),
                            Checkbox(
                              value: selectedJobTYpe == AppStrings.fullTime,
                              onChanged: (bool value) {
                                setState(() {
                                  selectedJobTYpe = AppStrings.fullTime;
                                });
                              },
                            )
                          ],
                        ),
                      ),
                      // freelancer
                      Container(
                        height: 30.0,
                        child: Row(
                          children: <Widget>[
                            const Text(AppStrings.freelancer),
                            Checkbox(
                              value: selectedJobTYpe == AppStrings.freelancer,
                              onChanged: (bool value) {
                                setState(() {
                                  selectedJobTYpe = AppStrings.freelancer;
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: InkWell(
              onTap: () {
                Get.to(
                  FilteredJobFeedPageParent(
                    category: _jobCategory,
                    type: selectedJobTYpe,
                  ),
                );
              },
              child: Container(
                width: 100.0,
                margin: const EdgeInsets.symmetric(vertical: 20.0),
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(200.0),
                  color: Colors.green,
                ),
                child: const Text(
                  "APPLY",
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
