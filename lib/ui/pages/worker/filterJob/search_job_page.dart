import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/workerBloc/searchPageBloc/search_page_bloc.dart';
import 'package:jobs/data/repositories/jobs_repository.dart';
import 'package:jobs/res/language_codes.dart';
import 'package:jobs/ui/pages/worker/new_search_page.dart';

class SearchJobPageParent extends StatelessWidget {
  JobsRepository jobsRepository = JobsRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SearchPageBloc>(
      create: (context) => SearchPageBloc(
        jobsRepository: jobsRepository,
      ),
      child: SearchJobPage(),
    );
  }
}

class SearchJobPage extends StatefulWidget {
  @override
  _SearchJobPageState createState() => _SearchJobPageState();
}

class _SearchJobPageState extends State<SearchJobPage> {
  // default job language
  String jobLanguage = "English";
  // default nationality
  String nationality = "United States";
  Country country;
  SearchPageBloc _searchPageBloc;

  @override
  Widget build(BuildContext context) {
    _searchPageBloc = BlocProvider.of<SearchPageBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Search Jobs",
          style: const TextStyle(
            color: Colors.blue,
          ),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          // heading
          Container(
            padding: EdgeInsets.all(5.0),
            child: const Text(
              "Search Jobs By Employer's Nationality and Language",
              style: const TextStyle(color: Colors.grey),
            ),
            alignment: Alignment.center,
          ),
          // language
          _buildLanguagePickerUi(),
          const Divider(
            color: Colors.grey,
            height: 20.0,
          ),
          _buildNationalityPickerUi(),
          _buildSearchBtnUi(),
        ],
      ),
    );
  }

  Widget _buildSearchBtnUi() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: MaterialButton(
        elevation: 10.0,
        color: Colors.blue[300],
        child: const Text(
          "Search",
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
        onPressed: () {
          Get.to(NewSearchPage(
            language: jobLanguage,
            nationality: nationality,
          ));
        },
      ),
    );
  }

  Widget _buildLanguagePickerUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _buildHeaderText("Job Language"),
        DropdownButton(
          items: LanguageCodes.jobLanguages.map((language) {
            return DropdownMenuItem(
              value: language,
              child: Text(language),
            );
          }).toList(),
          onChanged: (changedLanguage) async {
            setState(() {
              this.jobLanguage = changedLanguage;
            });
          },
          value: jobLanguage,
        ),
      ],
    );
  }

  Widget _buildNationalityPickerUi() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _buildHeaderText("Nationality"),
        _buildCountryPicker(),
      ],
    );
  }

  Widget _buildHeaderText(String text) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
        alignment: Alignment.centerLeft,
        width: 120.0,
        margin: const EdgeInsets.only(bottom: 5.0),
        color: Color(0xffc5cad3),
        child: Text(
          text,
          style: const TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildCountryPicker() {
    return Container(
      child: CountryPicker(
        onChanged: (c) {
          setState(() {
            country = c;
            nationality = country.name;
          });
        },
        showCurrency: false,
        showCurrencyISO: false,
        showDialingCode: false,
        showFlag: true,
        showName: true,
        selectedCountry: country,
      ),
    );
  }
}
