import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/blocs/loginBloc/login_bloc.dart';
import 'package:jobs/blocs/loginBloc/login_event.dart';
import 'package:jobs/blocs/loginBloc/login_state.dart';
import 'package:jobs/blocs/regBloc/workerRegBloc/worker_reg_bloc.dart';
import 'package:jobs/blocs/regBloc/workerRegBloc/worker_reg_event.dart';
import 'package:jobs/blocs/regBloc/workerRegBloc/worker_reg_state.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/res/user_type.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/common/login_page.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/ui/pages/worker/worker_home.dart';
import 'package:jobs/ui/widgets/bubble.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:meta/meta.dart';

class WorkerRegPageParent extends StatelessWidget {
  bool fromGuest;

  AuthService authService = AuthService();
  UsersService usersService = UsersService();
  EmployersRepository employersRepository = EmployersRepository();
  WorkersRepository workersRepository = WorkersRepository();

  WorkerRegPageParent({@required this.fromGuest});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              WorkerRegBloc(workersRepository: workersRepository),
        ),
        BlocProvider(
          create: (context) => LoginBloc(
            authService: authService,
            usersService: usersService,
            workersRepository: workersRepository,
            employersRepository: employersRepository,
          ),
        )
      ],
      child: WorkerRegPage(fromGuest: fromGuest),
    );
  }
}

class WorkerRegPage extends StatefulWidget {
  bool fromGuest;

  WorkerRegPage({@required this.fromGuest});

  @override
  _WorkerRegPageState createState() => _WorkerRegPageState();
}

class _WorkerRegPageState extends State<WorkerRegPage> {
  TextEditingController nameCntrlr = TextEditingController();
  TextEditingController emailCntrlr = TextEditingController();
  TextEditingController passwordCntrlr = TextEditingController();

  UsersRepository usersRepository;
  ProgressDialog progressDialog;
  WorkerRepository workerRepository;

  _WorkerRegPageState() {
    usersRepository = UsersRepository();
    workerRepository = WorkerRepository();
  }

  var workerSignUpFormKey = GlobalKey<FormState>();

  WorkerRegBloc workerRegBloc;
  LoginBloc loginBloc;

  double scrHeight, scrWidth;

  @override
  Widget build(BuildContext context) {
    scrHeight = MediaQuery.of(context).size.height;
    scrWidth = MediaQuery.of(context).size.width;
    progressDialog = ProgressDialog(context);
    workerRegBloc = BlocProvider.of<WorkerRegBloc>(context);
    loginBloc = BlocProvider.of<LoginBloc>(context);
    return WillPopScope(
      onWillPop: () async => Future.value(false),
      child: Scaffold(
        body: Form(
          key: workerSignUpFormKey,
          child: SingleChildScrollView(
            child: Container(
              height: scrHeight,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.blue[800], Colors.blue[400]],
                ),
              ),
              child: Stack(
                children: <Widget>[
                  // for bloc
                  MultiBlocListener(
                    listeners: [
                      // listener for workerreg bloc
                      BlocListener<WorkerRegBloc, WorkerRegState>(
                        listener: (context, state) {
                          if (state is WorkerRegSuccess) {
                            progressDialog.hide();
                            print("Success listened");
                            return navigateToWorkerHomePage(context);
                          } else if (state is WorkerRegFailure) {
                            progressDialog.hide();
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(state.message),
                            ));
                          } else if (state is ThirdPartySignInEmployer) {
                            progressDialog.hide();
                            return navigateToEmployerHomePage(context);
                          } else if (state is ThirdPartySignInWorker) {
                            progressDialog.hide();
                            return navigateToWorkerHomePage(context);
                          } else if (state is LinkWithThirdPartySuccess) {
                            progressDialog.hide();
                            return navigateToWorkerHomePage(context);
                          }
                        },
                      ),
                      // listener for login bloc, for guest login
                      BlocListener<LoginBloc, LoginState>(
                        listener: (context, state) {
                          if (state is LoginFailure) {
                            progressDialog.hide();
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text("Error : ${state.message}"),
                              ),
                            );
                          } else if (state is WorkerLoginSuccess) {
                            progressDialog.hide();
                            return navigateToWorkerHomePage(context);
                          }
                        },
                        child: BlocBuilder<LoginBloc, LoginState>(
                          builder: (context, state) {
                            if (state is WorkerLoginSuccess) {
                              return Container();
                            } else if (state is LoginFailure) {
                              return Container();
                            }
                          },
                        ),
                      ),
                    ],
                    child: BlocBuilder<WorkerRegBloc, WorkerRegState>(
                      builder: (context, state) {
                        if (state is WorkerRegInitial) {
                          print("WorkerReg Initial");
                          return Container();
                        } else if (state is WorkerRegLoading) {
                          print("WorkerReg Loading");
                          return buildLoadingUi();
                        } else if (state is WorkerRegSuccess) {
                          print("WorkerReg Success");
                          return Container();
                        } else if (state is WorkerRegFailure) {
                          print("WorkerReg Failure");
                          return Container();
                        } else if (state is ThirdPartySignInEmployer) {
                          print("Google sign in employer");
                          return Container();
                        } else if (state is ThirdPartySignInWorker) {
                          print("Google sign in worker");
                          return Container();
                        } else if (state is LinkWithThirdPartySuccess) {
                          print("Linked worker with google");
                          return Container();
                        }
                      },
                    ),
                  ),
                  // email sign up part
                  Container(
                    margin: EdgeInsets.only(top: 80.0),
                    child: buildEmailSignUpPart(),
                  ),
                  // text
                  Positioned(
                    right: 0.0,
                    left: 0.0,
                    bottom: 120.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Or Sign In With",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  // google sign in button
                  Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildGoogleSignInUi(),
                        buildFacebookSignInUi(),
                      ],
                    ),
                  ),
                  // followings are bubbles
                  Positioned(
                    right: -100.0,
                    top: -70.0,
                    child: Bubble(
                      size: 250.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    bottom: 100.0,
                    child: Bubble(
                      size: 50.0,
                    ),
                  ),
                  Positioned(
                    right: 30.0,
                    bottom: 60.0,
                    child: Bubble(
                      size: 30.0,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  // GmailSignIn and LinkWithGmail events are added
  Widget buildGoogleSignInUi() {
    return Align(
      child: GoogleSignInButton(
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();

          if (isConnected) {
            if (widget.fromGuest) {
              print("Guest");
              workerRegBloc.add(LinkGuestWorkerWithGmail());
            } else {
              print("Not Guest");
              workerRegBloc.add(GoogleSignInBtnPressed());
            }
          } else {
            Fluttertoast.showToast(msg: "Check Internet Connection");
          }
        },
        darkMode: false,
        borderRadius: 50.0,
        text: "Google",
      ),
    );
  }

  void navigateToEmployerHomePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EmployerHomePage();
    }));
  }

  Widget buildLoadingUi() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void navigateToWorkerHomePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerHome();
    }));
  }

  // EmailReg and LinkWithEmail events are added
  Widget buildEmailSignUpPart() {
    return Container(
      child: Column(
        children: <Widget>[
          // header text
          Container(
            padding: EdgeInsets.only(
                top: 30.0, bottom: 25.0, left: 10.0, right: 10.0),
            child: Text(
              "Sign Up & Start Searching Jobs",
              style: TextStyle(
                  fontSize: 20.0, color: Colors.white, fontFamily: "RussoOne"),
            ),
          ),
          // name textfield
          Container(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Write Your Name Please";
                  }
                },
                style: TextStyle(color: Colors.white),
                controller: nameCntrlr,
                decoration: InputDecoration(
                  errorStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "Your Name",
                  hintStyle: TextStyle(color: Colors.white),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: Icon(Icons.info, color: Colors.white),
                ),
                keyboardType: TextInputType.text,
              ),
            ),
          ),
          // email textfield
          Container(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Enter a valid email address";
                  }
                },
                style: TextStyle(color: Colors.white),
                controller: emailCntrlr,
                decoration: InputDecoration(
                  errorStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "E-mail",
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: Icon(Icons.email, color: Colors.white),
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
          ),
          // password textfield
          Container(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            width: scrWidth * 0.85,
            padding: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Password can't be empty";
                  }
                  if (value.length < 6) {
                    return "Password length should be at least 6";
                  }
                },
                style: TextStyle(color: Colors.white),
                controller: passwordCntrlr,
                decoration: InputDecoration(
                  errorStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "Password",
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: Icon(Icons.lock, color: Colors.white),
                ),
                obscureText: true,
              ),
            ),
          ),
          MaterialButton(
            minWidth: scrWidth * 0.8,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80.0)),
            child: Text(
              "SIGN UP NOW",
              style: TextStyle(
                color: Colors.blue[300],
                fontFamily: "RussoOne",
              ),
            ),
            onPressed: () async {
              if (workerSignUpFormKey.currentState.validate()) {
                var model = WorkerModel(
                  email: emailCntrlr.text,
                  name: nameCntrlr.text,
                );

                bool isConnected = await ConnectivityService().hasConnection();

                if (isConnected) {
                  progressDialog.show();

                  if (widget.fromGuest) {
                    print("Guest");
                    workerRegBloc.add(LinkGuestWorkerWithEmail(
                      workerModel: model,
                      email: emailCntrlr.text,
                      password: passwordCntrlr.text,
                    ));
                  } else {
                    print("Not Guest");
                    workerRegBloc.add(WorkerRegButtonPressed(
                      workerModel: model,
                      email: emailCntrlr.text,
                      password: passwordCntrlr.text,
                    ));
                  }
                } else {
                  Fluttertoast.showToast(
                    msg: "Check Internet Connection",
                  );
                }
              }
            },
            color: Colors.white,
          ),
          // guest sign in button
          buildGuestSignInButton(scrWidth),
          // text and loginpage, homepage navigation
          Container(
            padding: EdgeInsets.symmetric(
              vertical: 5.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Already registered?   ",
                  style: TextStyle(color: Colors.white),
                ),
                InkWell(
                  child: Text(
                    "Login",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    navigateToLoginPage(context);
                  },
                ),
                // go home
                SizedBox(width: 10.0),
                Text("Or   ", style: TextStyle(color: Colors.white)),
                // go home
                InkWell(
                  onTap: () {
                    navigateToAppHome(context);
                  },
                  child: Text(
                    "GO HOME",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  void navigateToLoginPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return LoginPageParent(loginWorker: true);
    }));
  }

  void navigateToAppHome(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EmployeeEmployerPage();
    }));
  }

  @override
  void dispose() {
    nameCntrlr.dispose();
    emailCntrlr.dispose();
    passwordCntrlr.dispose();
    super.dispose();
  }

  Widget buildGuestSignInButton(double scrWidth) {
    return MaterialButton(
      minWidth: scrWidth * 0.8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
      child: Text(
        "SIGN IN AS GUEST",
        style: TextStyle(
          color: Colors.blue[300],
          fontFamily: "RussoOne",
        ),
      ),
      onPressed: () async {
        bool isConnected = await ConnectivityService().hasConnection();
        if (isConnected) {
          progressDialog.show();
          loginBloc.add(SignInWorkerAsGuest());
        } else {
          Fluttertoast.showToast(
            msg: "Check Internet Connection",
          );
        }
      },
      color: Colors.white,
    );
  }

  Widget buildFacebookSignInUi() {
    return Align(
      child: FacebookSignInButton(
        text: "Facebook",
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();
          if (isConnected) {
            if (widget.fromGuest) {
              print("Guest");
              workerRegBloc.add(LinkGuestWorkerWithFacebook());
            } else {
              print("Not Guest");
              workerRegBloc.add(FacebookSignInBtnPressed());
            }
          } else {
            Fluttertoast.showToast(msg: "Check Internet Connection");
          }
        },
        borderRadius: 50.0,
      ),
    );
  }
}
