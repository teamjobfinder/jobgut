import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobs/blocs/workerBloc/jobFeedBloc/job_feed_bloc.dart';
import 'package:jobs/blocs/workerBloc/jobFeedBloc/job_feed_event.dart';
import 'package:jobs/blocs/workerBloc/jobFeedBloc/job_feed_state.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:jobs/ui/pages/worker/worker_drawer.dart';
import 'package:jobs/ui/widgets/drag_drawer.dart';
import 'package:jobs/ui/widgets/errors/center_error.dart';
import 'package:jobs/ui/widgets/feed_thumbnail.dart';
import 'package:jobs/ui/widgets/loadings/circular_loading.dart';
import 'package:jobs/ui/widgets/loadings/feed_loading.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:jobs/utils/helper.dart';

/**
 * 1. theme checked
 */
class JobFeedPageDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DragDrawer(
      child: JobFeedPageParent(),
      drawer: WorkerDrawer(),
    );
  }
}

class JobFeedPageParent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<JobFeedBloc>(
      create: (context) => JobFeedBloc(),
      child: JobFeedPage(),
    );
  }
}

class JobFeedPage extends StatefulWidget {
  @override
  _JobFeedPageState createState() => _JobFeedPageState();
}

class _JobFeedPageState extends State<JobFeedPage> {
  JobFeedBloc _jobFeedBloc;
  final String _TAG = "_JobFeedPageState";

  @override
  void initState() {
    super.initState();
    _jobFeedBloc = BlocProvider.of<JobFeedBloc>(context);
    _jobFeedBloc.add(CheckGps());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppConfig.appName),
        leading: Builder(
          builder: (context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () => DragDrawer.of(context).open(),
            );
          },
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: BlocBuilder<JobFeedBloc, JobFeedState>(
        builder: (context, state) {
          if (state is LoadingRequest) {
            Helper.logPrint(_TAG, "LoadingRequest");
            return const CircularLoading();
          } else if (state is CurrentLocationLoaded) {
            Helper.logPrint(_TAG, "CurrentLocationLoaded");
            return const SizedBox(height: 0.0);
          } else if (state is LoadingJobPosts) {
            Helper.logPrint(_TAG, "LoadingJobPosts");
            return const FeedLoading();
          } else if (state is JobPostsLoaded) {
            Helper.logPrint(_TAG, "JobPostsLoaded");
            return _JobPostsLoadedUi(jobPosts: state.jobPosts);
          } else if (state is Error) {
            Helper.logPrint(_TAG, "Error");
            return CenterError(message: state.message);
          }
        },
      ),
    );
  }
}

class _JobPostsLoadedUi extends StatelessWidget {
  final String _TAG = "_JobPostsLoadedUi";
  final List<JobPostDocumentModel> jobPosts;

  const _JobPostsLoadedUi({Key key, @required this.jobPosts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Helper.logPrint(_TAG, AppStrings.logBuildFunction);
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      physics: ScrollPhysics(),
      itemCount: jobPosts.length,
      itemBuilder: (context, position) {
        JobPostDocumentModel jobPostDocumentModel = jobPosts[position];
        JobPostModel jobPostModel = jobPostDocumentModel.jobPostModel;
        DocumentReference documentReference =
            jobPostDocumentModel.documentReference;
        DocumentSnapshot documentSnapshot =
            jobPostDocumentModel.documentSnapshot;
        return InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return PostDetailsPageParent(
                isWorker: true,
                jobPostModel: jobPostModel,
                documentReference: documentReference,
                documentSnapshot: documentSnapshot,
              );
            }));
          },
          child: FeedThumbnail(jobPostModel: jobPostModel),
        );
      },
    );
  }
}
