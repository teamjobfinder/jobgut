import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:jobs/ui/pages/worker/worker_home.dart';
import 'package:jobs/ui/temp/map_with_markers.dart';
import 'package:jobs/ui/widgets/feed_thumbnail.dart';
import 'package:meta/meta.dart';

/// For searched jobs from map
class JobSearchFeedPage extends StatefulWidget {
  TheLocation location;

  JobSearchFeedPage({@required this.location});

  @override
  _JobSearchFeedPageState createState() => _JobSearchFeedPageState();
}

class _JobSearchFeedPageState extends State<JobSearchFeedPage> {
  WorkerRepository workerRepository;
  List<JobPostModel> jobs;
  List<DocumentSnapshot> documentSnapShots;

  _JobSearchFeedPageState() {
    workerRepository = WorkerRepository();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return new Future(() => false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              widget.location.country == null
                  ? "Available Jobs"
                  : "Jobs In ${widget.location.country}",
              style: TextStyle(color: Colors.black),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.grey),
          leading: IconButton(
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => WorkerHome(),
                ),
                (Route<dynamic> route) => false,
              );
            },
            icon: BackButtonIcon(),
          ),
        ),
        body: Container(
          child: Stack(
            children: <Widget>[
              StreamBuilder(
                stream: Firestore.instance
                    .collectionGroup("posts")
                    .where(
                      "country",
                      isEqualTo: widget.location.country,
                    )
                    .snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print("Has Data ${snapshot.data.documents}");
                    documentSnapShots = snapshot.data.documents;
                    return FutureBuilder(
                      future: workerRepository
                          .searchJobsInALocation(documentSnapShots),
                      builder: (context, jobListSnap) {
                        if (jobListSnap.hasError) {
                          return builErrorUi(jobListSnap.error);
                        }
                        if (jobListSnap.hasData) {
                          if (jobListSnap.data.length == 0) {
                            return buildNoJobsFoundUi(widget.location.country);
                          } else {
                            int jobsNum = jobListSnap.data.length;
                            jobs = jobListSnap.data;
                            return buildFoundJobsUi(
                              jobsNum,
                              jobs,
                              documentSnapShots,
                            );
                          }
                        } else {
                          return Center(
                            child: Text("Please Wait ... ..."),
                          );
                        }
                      },
                    );
                  } else {
                    print("No Data");
                    return buildNoJobsFoundUi(widget.location.country);
                  }
                },
              ),
              Positioned(
                bottom: 15.0,
                right: 15.0,
                child: FloatingActionButton(
                  onPressed: () {
                    if (documentSnapShots != null) {
                      
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return MapWithMarkers(
                          jobs: jobs,
                          location: widget.location,
                          docSnaps: documentSnapShots,
                        );
                      }));
                    } else {
                      Fluttertoast.showToast(
                          msg: "Please wait till full loading.");
                    }
                  },
                  child: Icon(Icons.place),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // error ui
  Widget builErrorUi(String message) {
    return Text("Error : $message");
  }

  // no jobs found ui
  Widget buildNoJobsFoundUi(String location) {
    return Center(
      child: Text("No jobs found here in $location"),
    );
  }

  Widget buildFoundJobsUi(
      int itemNum, List<JobPostModel> list, List<DocumentSnapshot> docSnaps) {
    return ListView.builder(
      itemCount: itemNum,
      itemBuilder: (context, position) {
        DocumentSnapshot docSnap = docSnaps[position];
        DocumentReference docRef = docSnap.reference;
        JobPostModel model = list[position];
        return InkWell(
          onTap: () {
            Get.to(PostDetailsPageParent(
              isWorker: true,
              jobPostModel: model,
              documentReference: docRef,
              documentSnapshot: docSnap,
            ));
          },
          child: FeedThumbnail(
            jobPostModel: model,
          ),
        );
      },
    );
  }
}
