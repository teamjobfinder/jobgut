import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/workerBloc/filteredJobBloc/filtered_job_bloc.dart';
import 'package:jobs/blocs/workerBloc/filteredJobBloc/filtered_job_event.dart';
import 'package:jobs/blocs/workerBloc/filteredJobBloc/filtered_job_state.dart';
import 'package:jobs/data/models/job_post_document_model.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:jobs/ui/widgets/failures/failure_ui.dart';
import 'package:jobs/ui/widgets/feed_thumbnail.dart';
import 'package:jobs/ui/widgets/loadings/feed_loading.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';

class FilteredJobFeedPageParent extends StatelessWidget {
  final String category;
  final String type;

  const FilteredJobFeedPageParent(
      {Key key, @required this.category, @required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FilteredJobBloc>(
      create: (context) => FilteredJobBloc(),
      child: FilteredJobFeedPage(
        jobType: type,
        jobCategory: category,
      ),
    );
  }
}

class FilteredJobFeedPage extends StatefulWidget {
  final String jobType, jobCategory;

  FilteredJobFeedPage({@required this.jobType, @required this.jobCategory});

  @override
  _FilteredJobFeedPageState createState() => _FilteredJobFeedPageState();
}

class _FilteredJobFeedPageState extends State<FilteredJobFeedPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FilteredJobBloc>(context)
      ..add(
        FetchJobsByCategoryAndType(
          category: widget.jobCategory,
          type: widget.jobType,
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Filtered Jobs"),
      ),
      body: BlocBuilder<FilteredJobBloc, FilteredJobState>(
        builder: (context, state) {
          if (state is FilteredJobsInitial) {
            return const FeedLoading();
          } else if (state is FilteredJobsLoading) {
            return _NoJobsFoundUi(); // TODO temporary solution
          } else if (state is FilteredJobsLoaded) {
            return _JobsLoadedUi(jobs: state.filteredJobs);
          } else if (state is NoFilteredJobsFound) {
            return _NoJobsFoundUi(type: state.type, category: state.category);
          } else if (state is FilteredJobsLoadFailure) {
            return FailureUi(message: state.message);
          }
        },
      ),
    );
  }
}

class _JobsLoadedUi extends StatelessWidget {
  final List<JobPostDocumentModel> jobs;

  const _JobsLoadedUi({Key key, @required this.jobs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: jobs.length,
      itemBuilder: (context, position) {
        JobPostDocumentModel jobPostDocumentModel = jobs[position];
        return InkWell(
          onTap: () {
            Get.to(PostDetailsPageParent(
              isWorker: true,
              jobPostModel: jobPostDocumentModel.jobPostModel,
              documentReference: jobPostDocumentModel.documentReference,
              documentSnapshot: jobPostDocumentModel.documentSnapshot,
            ));
          },
          child: FeedThumbnail(jobPostModel: jobPostDocumentModel.jobPostModel),
        );
      },
    );
  }
}

class _NoJobsFoundUi extends StatelessWidget {
  final String category, type;

  const _NoJobsFoundUi({Key key, this.category, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Text(Helper.isNullOrEmpty(category)
          ? "No Jobs Found"
          : "No Jobs Found for Type : $type & Category : $category"),
    );
  }
}
