import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/ui/pages/worker/applied_post_details_page.dart';
import 'package:jobs/ui/widgets/feed_thumbnail.dart';
import 'package:jobs/ui/widgets/loadings/feed_loading.dart';

/**
 * 1. theme checked
 * 2. const checked
 */
class AppliesPage extends StatefulWidget {
  @override
  _AppliesPageState createState() => _AppliesPageState();
}

class _AppliesPageState extends State<AppliesPage> {
  WorkerRepository workerRepository = WorkerRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Applied Jobs"),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: workerRepository.getCurrentWorkerId(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var userId = snapshot.data.uid;
            return StreamBuilder(
              stream: Firestore.instance
                  .collectionGroup("applications")
                  .where(
                    "workerId",
                    isEqualTo: userId,
                  )
                  .snapshots(),
              builder: (context, streamSnap) {
                if (streamSnap.hasError) {
                  return Text(streamSnap.error.toString());
                }
                if (streamSnap.hasData) {
                  List<DocumentSnapshot> qs = streamSnap.data.documents;

                  return FutureBuilder(
                    future: workerRepository.getListOfAppliedJobs(qs),
                    builder: (context, jobListSnap) {
                      if (jobListSnap.hasError) {
                        return Text("Error : ${jobListSnap.error}");
                      }
                      if (jobListSnap.hasData) {
                        if (jobListSnap.data.length == 0) {
                          return const Center(
                            child:
                                const Text("Haven't applied in any job yet"),
                          );
                        } else {
                          return ListView.builder(
                            itemCount: jobListSnap.data.length,
                            itemBuilder: (context, position) {
                              JobPostModel model = jobListSnap.data[position];
                              return model != null
                                  ? InkWell(
                                      onTap: () {
                                        Get.to(
                                          AppliedPostDetailsPage(
                                            jobPostModel: model,
                                          ),
                                        );
                                      },
                                      child: FeedThumbnail(
                                        jobPostModel: model,
                                      ),
                                    )
                                  : const SizedBox(height: 0.0);
                            },
                          );
                        }
                      } else {
                        return const FeedLoading();
                      }
                    },
                  );
                } else {
                  return const Center(child: const _NotAppliedUi());
                }
              },
            );
          } else {
            return const Center(
              child: const Text("Loading... ..."),
            );
          }
        },
      ),
    );
  }
}

class _NotAppliedUi extends StatelessWidget {
  const _NotAppliedUi();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const ColorizeAnimatedTextKit(
          text: const ["You Have Not Applied Yet"],
          textStyle: const TextStyle(fontSize: 20.0, fontFamily: "RussoOne"),
          colors: [
            Colors.blue,
            Colors.blue,
            Colors.yellow,
            Colors.red,
          ],
          textAlign: TextAlign.start,
          alignment: AlignmentDirectional.topStart,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(width: 20.0, height: 100.0),
            const Text(
              "GO!",
              style: const TextStyle(fontSize: 43.0),
            ),
            const SizedBox(width: 20.0, height: 100.0),
            const RotateAnimatedTextKit(
              text: const ["READY", "STEADY", "APPLY!"],
              textStyle: const TextStyle(fontSize: 40.0, fontFamily: "RussoOne"),
              textAlign: TextAlign.start,
              totalRepeatCount: 50,
            ),
          ],
        ),
      ],
    );
  }
}
