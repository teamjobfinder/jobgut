import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/ui/pages/worker/applies_page.dart';
import 'package:jobs/ui/pages/worker/filter_page.dart';
import 'package:jobs/ui/pages/worker/guest_page.dart';
import 'package:jobs/ui/pages/worker/job_feed_page.dart';
import 'package:jobs/ui/pages/worker/worker_profile_page.dart';
import 'package:jobs/ui/temp/temp_map.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';

class WorkerHome extends StatefulWidget {
  const WorkerHome();

  @override
  _WorkerHomeState createState() => _WorkerHomeState();
}

class _WorkerHomeState extends State<WorkerHome> {
  int current_tab = 0;
  DateTime currentBackPressTime;
  WorkerRepository _workerRepository = WorkerRepository();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              current_tab = index;
            });
          },
          currentIndex: current_tab,
          items: [
            const BottomNavigationBarItem(
              icon: const Icon(Icons.home),
              title: const Text("Home"),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(Icons.assignment_turned_in),
              title: const Text("Applications"),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(Icons.sort),
              title: const Text("Filter"),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(Icons.place),
              title: const Text("Map"),
            ),
            const BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              title: const Text("Profile"),
            ),
          ],
        ),
        body: IndexedStack(
          children: <Widget>[
            JobFeedPageDrawer(),
            AppliesPage(),
            FilterPage(),
            JobMaps(fromWorkerContext: true),
            buildProfilePage(),
          ],
          index: current_tab,
        ),
      ),
    );
  }

  Widget buildProfilePage() {
    return FutureBuilder(
      future: _workerRepository.isGuest(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return buildErrorUi(snapshot.error.toString());
        }
        if (snapshot.hasData) {
          bool isGuest = snapshot.data;
          if (isGuest) {
            return const GuestPage();
          } else {
            return const WorkerProfilePageParent();
          }
        } else {
          return buildLoadingIndicator();
        }
      },
    );
  }

  Future<bool> _onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Press again to exit");
      print("SINGLE PRESSED");
      return Future.value(false);
    }
    print("DOUBLE PRESSED");
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    // return Future.value(true);
  }
}
