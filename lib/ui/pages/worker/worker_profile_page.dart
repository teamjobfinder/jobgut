import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jobs/blocs/workerBloc/workerProfilePageBloc/worker_profile_page_bloc.dart';
import 'package:jobs/blocs/workerBloc/workerProfilePageBloc/worker_profile_page_event.dart';
import 'package:jobs/blocs/workerBloc/workerProfilePageBloc/worker_profile_page_state.dart';
import 'package:jobs/data/models/worker_model.dart';
import 'package:jobs/data/repositories/workerRepository/worker_repository.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/worker/edit_worker_profile_page.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/dialog_helper.dart';
import 'package:jobs/utils/helper.dart';
import 'package:url_launcher/url_launcher.dart';

class WorkerProfilePageParent extends StatelessWidget {
  const WorkerProfilePageParent();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<WorkerProfilePageBloc>(
      create: (context) => WorkerProfilePageBloc(),
      child: WorkerProfilePage(),
    );
  }
}

class WorkerProfilePage extends StatefulWidget {
  const WorkerProfilePage();

  @override
  _WorkerProfilePageState createState() => _WorkerProfilePageState();
}

class _WorkerProfilePageState extends State<WorkerProfilePage> {
  var scrWidth;
  WorkerProfilePageBloc _workerProfilePageBloc;
  WorkerRepository _workerRepository = WorkerRepository();

  @override
  void initState() {
    super.initState();
    _workerProfilePageBloc = BlocProvider.of<WorkerProfilePageBloc>(context);
    _workerProfilePageBloc.add(FetchWorkerProfile());
  }

  @override
  Widget build(BuildContext context) {
    scrWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            onPressed: () {
              DialogHelper.showClassicDialog(
                context,
                "Want to Log Out?",
                "You Will be Logged Out",
                onCancel: () {
                  return Navigator.of(context).pop();
                },
                onOk: () {
                  _workerProfilePageBloc.add(LogOutWorker());
                },
              );
            },
          ),
        ],
      ),
      body: BlocListener<WorkerProfilePageBloc, WorkerProfilePageState>(
        listener: (context, state) {
          if (state is WorkerSignedOut) {
            Get.to(EmployeeEmployerPage());
          }
        },
        child: BlocBuilder<WorkerProfilePageBloc, WorkerProfilePageState>(
          builder: (context, state) {
            if (state is WorkerProfilePageInitial) {
              return buildLoadingIndicator();
            } else if (state is WorkerProfilePageLoading) {
              return buildLoadingIndicator();
            } else if (state is WorkerProfilePageLoaded) {
              return WorkerPrfoileUi(model: state.workerModel);
            } else if (state is WorkerProfilePageError) {
              return buildErrorUi(state.message);
            } else if (state is WorkerSignedOut) {
              return const SizedBox(height: 0.0);
            }
          },
        ),
      ),
    );
  }
}

class WorkerPrfoileUi extends StatelessWidget {
  final WorkerModel model;

  const WorkerPrfoileUi({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var scrWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      width: scrWidth,
      child: ListView(
        children: <Widget>[
          // 1st section pp, name, profession
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              children: <Widget>[
                // pp
                Container(
                  margin: const EdgeInsets.only(right: 20.0),
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: buildPicHolder(
                    radius: 70.0,
                    isWorker: true,
                    url: model.profilPicUrl,
                  ),
                ),
                // name and profession and gender
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    // name
                    Container(
                      width: (scrWidth / 3) * 2,
                      child: Text(
                        model.name == null ? "N/A" : model.name,
                        style: TextStyle(
                          color: Colors.blue[600],
                          fontWeight: FontWeight.bold,
                          fontSize: 25.0,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                    // profession
                    Container(
                      width: (scrWidth / 3) * 2,
                      child: Text(
                        model.profession != null
                            ? model.profession
                            : "Edit profile To Add Profession",
                        style: const TextStyle(
                          color: Colors.grey,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ),
                    // gender
                    Helper.isNullOrEmpty(model.gender)
                        ? const SizedBox(height: 0.0)
                        : Container(
                            width: (scrWidth / 3) * 2,
                            child: Text(
                              model.gender,
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                            ),
                          ),
                  ],
                ),
              ],
            ),
          ),
          // edit profile
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return EditWorkerProfilePage(workerModel: model);
              }));
            },
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(vertical: 10),
              margin: const EdgeInsets.only(bottom: 10.0),
              child: const Text(
                "Edit Profile",
                style: const TextStyle(
                  fontSize: 17.0,
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ),
          // 2nd section phone
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: <Widget>[
                // icon
                Container(
                  child: const Icon(Icons.phone, size: 30.0),
                ),
                const SizedBox(width: 45.0),
                // text
                Container(
                  width: (scrWidth / 3) * 2,
                  child: InkWell(
                    onTap: () async {
                      var number = model.phoneNumber.toString();
                      if (Helper.isNullOrEmpty(number)) {
                        return;
                      }
                      var url = "tel:$number";
                      if (await canLaunch(url)) {
                        await launch(url);
                      }
                    },
                    child: Text(
                      model.phoneNumber != null
                          ? "${model.countryCode} ${model.phoneNumber.toString()}"
                          : "Edit Profile to Add Phone Number",
                      style: const TextStyle(
                        fontSize: 18.0,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // 3rd section email
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: <Widget>[
                // icon
                Container(
                  child: const Icon(Icons.email, size: 30.0),
                ),
                const SizedBox(width: 45.0),
                // text
                Container(
                  child: Text(
                    model.email,
                    style: const TextStyle(fontSize: 18.0),
                  ),
                ),
              ],
            ),
          ),
          const Divider(height: 40.0),
          // 4th section about
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: <Widget>[
                // icon
                Container(
                  child: const Icon(Icons.info, size: 30.0),
                ),
                const SizedBox(width: 45.0),
                Column(
                  children: <Widget>[
                    // title
                    Container(
                      width: (scrWidth / 3) * 2,
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: const Text(
                        "About",
                        style: const TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    // about text
                    Container(
                      width: (scrWidth / 3) * 2,
                      child: Text(
                        model.about != null
                            ? model.about
                            : "Edit Profile to Add Your Info",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 20,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const Divider(height: 40.0),
          // 5th section location
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: <Widget>[
                // icon
                Container(
                  child: const Icon(Icons.place, size: 30.0),
                ),
                const SizedBox(width: 45.0),
                Column(
                  children: <Widget>[
                    // title
                    Container(
                      width: (scrWidth / 3) * 2,
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: const Text(
                        "Location",
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    // about text
                    Container(
                      width: (scrWidth / 3) * 2,
                      child: Text(
                        model.location != null
                            ? model.location
                            : "Edit Profile to Add Your Location",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const Divider(height: 40.0),
          // 6th section skills
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: <Widget>[
                // icon
                Container(
                  child: const Icon(Icons.work, size: 30.0),
                ),
                const SizedBox(width: 45.0),
                Column(
                  children: <Widget>[
                    // title
                    Container(
                      width: (scrWidth / 3) * 2,
                      padding: EdgeInsets.only(bottom: 5.0),
                      child: const Text(
                        "Skills",
                        style: const TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    // about text
                    Container(
                      width: (scrWidth / 3) * 2,
                      child: Text(
                        model.skills != null
                            ? model.skills
                            : "Edit Profile To Add Your Skills",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 10,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
