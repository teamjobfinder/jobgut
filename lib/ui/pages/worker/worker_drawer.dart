import 'package:flutter/material.dart';
import 'package:jobs/notifiers/theme_notifier.dart';
import 'package:jobs/res/app_strings.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class WorkerDrawer extends StatelessWidget {
  var scrHeight, scrWidth;

  @override
  Widget build(BuildContext context) {
    scrHeight = MediaQuery.of(context).size.height;
    scrWidth = MediaQuery.of(context).size.width;

    return Material(
      color: Theme.of(context).primaryColor,
      child: SafeArea(
        child: Theme(
          data: ThemeData(brightness: Brightness.dark),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 2.0),
                height: scrHeight * 0.3,
                color: Theme.of(context).primaryColor,
                width: scrWidth * 0.6,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      AppConfig.appName,
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: "RussoOne",
                        fontSize: 25.0,
                        letterSpacing: 2.5,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: const Text(
                        AppConfig.appVersion,
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: "RussoOne",
                          fontSize: 17.0,
                          letterSpacing: 2.5,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(height: 2.0, color: Colors.white70),

              // drawer items
              // share app
              ListTile(
                leading: const Icon(Icons.share),
                title: const Text("Share App"),
                onTap: () {
                  String title = "Download ${AppConfig.appName}";
                  String text = "$title\n ${AppConfig.playStoreLink}";
                  Share.share(text);
                },
              ),
              const Divider(height: 2.0, color: Colors.white70),
              // rate app
              ListTile(
                leading: const Icon(Icons.star_border),
                title: const Text("Rate This App"),
                onTap: () async {
                  String url = AppConfig.playStoreLink;
                  if (await canLaunch(url)) {
                    await launch(url);
                  }
                },
              ),
              const Divider(height: 2.0, color: Colors.white70),
              // switch theme
              Consumer<ThemeNotifier>(
                builder: (context, themeNotifier, child) {
                  return SwitchListTile(
                    secondary: const Icon(Icons.lightbulb_outline),
                    title: const Text("Switch Theme"),
                    onChanged: (val) {
                      themeNotifier.switchTheme();
                    },
                    value: themeNotifier.getTheme,
                  );
                },
              ),
              const Divider(height: 2.0, color: Colors.white70),
              // about app
              ListTile(
                leading: const Icon(Icons.info),
                title: const Text("About"),
                onTap: () {
                  showAboutDialog(
                    context: context,
                    applicationName: AppStrings.appName,
                    applicationVersion: "Version : ${AppConfig.appVersion}",
                    children: [
                      Text(
                        "Simple App For Searching Jobs and For Hiring Employee",
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
