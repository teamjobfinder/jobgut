import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/res/app_assets.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:jobs/utils/helper.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

class AppliedPostDetailsPage extends StatelessWidget {
  var currentSelected;
  var scrHeight, scrWidth;

  var leftItemsFlex = 2;
  var rightItemsFlex = 3;
  final double buttonRadius = 50.0;
  static const double _logoSize = 70.0;

  final JobPostModel jobPostModel;

  AppliedPostDetailsPage({@required this.jobPostModel});

  @override
  Widget build(BuildContext context) {
    scrWidth = MediaQuery.of(context).size.width - 40.0;
    scrHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(AppConfig.appName),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.all(10.0),
        children: <Widget>[
          //1. date
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text(
                  "Posted on  ${Helper.formatDateTime(jobPostModel.date)}",
                  style: const TextStyle(color: Colors.grey),
                ),
              ),
              // languagesDropDown(),
            ],
          ),
          //2. post image
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: Helper.isNullOrEmpty(jobPostModel.jobImageUrl) != true
                    ? Image.network(
                        jobPostModel.jobImageUrl,
                        width: scrWidth,
                        height: 200.0,
                        fit: BoxFit.cover,
                      )
                    : const SizedBox(height: 0.0)),
          ),
          //3. title
          Container(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0),
            child: Text(
              jobPostModel.title,
              style: const TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          //4. position
          Container(
            padding: const EdgeInsets.only(top: 10.0, left: 20.0, bottom: 25.0),
            child: Text(
              jobPostModel.jobPosition,
              style: const TextStyle(fontSize: 17.0),
            ),
          ),
          //5. company name and logo
          Container(
            padding: const EdgeInsets.only(left: 20.0, bottom: 20.0),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 20.0),
                  child: ClipOval(
                    child: jobPostModel.companyLogoUrl == null
                        ? Image.asset(
                            AppAssets.workerPlaceHolderImage,
                            height: _logoSize,
                            width: _logoSize,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            jobPostModel.companyLogoUrl,
                            width: _logoSize,
                            height: _logoSize,
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
                // Container(
                //   width: scrWidth * 0.6,
                //   child: Text(
                //     jobPostModel.name.toUpperCase(),
                //     style: const TextStyle(
                //       fontSize: 19.0,
                //       color: Colors.blue,
                //       fontWeight: FontWeight.bold,
                //     ),
                //     overflow: TextOverflow.ellipsis,
                //     maxLines: 3,
                //   ),
                // ),
              ],
            ),
          ),
          //6. description
          Container(
            width: scrWidth * 0.8,
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              jobPostModel.jobDescription,
              overflow: TextOverflow.ellipsis,
              maxLines: 6,
              style: const TextStyle(
                fontSize: 15.0,
                height: 1.5,
              ),
            ),
          ),
          // Experience
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Experience  ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  width: scrWidth * 0.55,
                  child: Text(
                    jobPostModel.experience,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // vacancies
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Vacancies ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  width: scrWidth * 0.5,
                  child: Text(
                    jobPostModel.vacancies.toString(),
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // language
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Job Language ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  width: scrWidth * 0.5,
                  child: Text(
                    jobPostModel.language != null
                        ? jobPostModel.language
                        : "N/A",
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // job type
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Job Type ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  width: scrWidth * 0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    jobPostModel.jobType,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // preferred gender
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Preferred Gender ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  width: scrWidth * 0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    jobPostModel.preferredGender != null
                        ? jobPostModel.preferredGender
                        : "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // job category
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Job Category ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  width: scrWidth * 0.5,
                  child: Text(
                    jobPostModel.jobCategory,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // salary range
          Container(
            width: scrWidth,
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Salary Range ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  width: scrWidth * 0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    jobPostModel.salaryRange + " " + jobPostModel.salaryType,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 6,
                  ),
                ),
              ],
            ),
          ),
          // schedule
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Schedule ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  width: scrWidth * 0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    jobPostModel.schedule,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // location
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Location ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  width: scrWidth * 0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Wrap(
                    children: <Widget>[
                      Text(
                        "${jobPostModel.name}, ${jobPostModel.subLocality}, ${jobPostModel.country}",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // deadline
          Container(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0, bottom: 10.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: scrWidth * 0.4,
                  child: const Text(
                    "Deadline ",
                    style: const TextStyle(fontSize: 17.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  width: scrWidth * 0.5,
                  child: Text(
                    // TODO deadline
                    Helper.formatDateTime(jobPostModel.deadline),
                    style: const TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // Address
          const Padding(
            padding: EdgeInsets.only(left: 20.0, bottom: 10.0, top: 20.0),
            child: const Text(
              "Address :",
              style: const TextStyle(
                // color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
          ),
          Container(
              width: scrWidth * 0.7,
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                Helper.isNullOrEmpty(jobPostModel.address)
                    ? "N/A"
                    : jobPostModel.address,
                style: const TextStyle(
                  fontSize: 16.0,
                  // color: Colors.black,
                ),
              )),
          const SizedBox(
            height: 200.0,
          )
        ],
      ),
    );
  }
}
