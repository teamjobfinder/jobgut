import 'package:flutter/material.dart';
import 'package:jobs/ui/pages/worker/worker_reg_page.dart';
import 'package:jobs/ui/widgets/bubble.dart';

class GuestPage extends StatelessWidget {

  const GuestPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [Colors.blue[800], Colors.blue[400]],
              ),
            ),
          ),
          Positioned(
            child: Align(
              alignment: Alignment.center,
              child: MaterialButton(
                elevation: 10.0,
                onPressed: () {
                  navigateToWorkerRegPage(context);
                },
                child: const Text(
                  "Sign Up To Have Full Access",
                  style: const TextStyle(
                      color: Colors.blue, fontFamily: "RussoOne"),
                ),
                color: Colors.white70,
              ),
            ),
          ),
          const Positioned(
            bottom: 20.0,
            left: 30.0,
            child: const Bubble(
              size: 90,
            ),
          ),
          const Positioned(
            top: 70.0,
            left: 30.0,
            child: const Bubble(
              size: 40,
            ),
          ),
          const Positioned(
            bottom: 20.0,
            right: 30.0,
            child: const Bubble(
              size: 30,
            ),
          ),
          const Positioned(
            top: 170.0,
            right: 50.0,
            child: const Bubble(
              size: 50,
            ),
          ),
        ],
      ),
    );
  }

  void navigateToWorkerRegPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerRegPageParent(fromGuest: true);
    }));
  }
}
