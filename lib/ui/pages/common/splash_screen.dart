import 'package:flutter/material.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:shimmer/shimmer.dart';

class SplashScreen extends StatefulWidget {

  const SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.cyan,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Shimmer.fromColors(
                baseColor: Colors.black,
                highlightColor: Colors.grey,
                child: const Text(
                  AppConfig.appName,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 45.0,
                    letterSpacing: 2.5,
                    fontFamily: "RussoOne",
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 5.0),
                child: const Text(
                  "Simple Job Search",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                    letterSpacing: 2.5,
                    fontFamily: "RussoOne",
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
