import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/ui/pages/employer/employer_registration_page.dart';
import 'package:jobs/ui/pages/worker/worker_reg_page.dart';
import 'package:jobs/ui/widgets/bubble.dart';
import 'package:jobs/ui/widgets/glowing_stars.dart';
import 'package:jobs/utils/app_config.dart';
import 'package:jobs/utils/location_helper.dart';

/**
 * const checked
 */
class EmployeeEmployerPage extends StatefulWidget {

  const EmployeeEmployerPage();

  @override
  _EmployeeEmployerPageState createState() => _EmployeeEmployerPageState();
}

class _EmployeeEmployerPageState extends State<EmployeeEmployerPage> {
  DateTime currentBackPressTime;

  var scrWidth, scrHeight;

  @override
  void initState() {
    LocationHelper().requestLocation();
  }

  @override
  Widget build(BuildContext context) {
    scrWidth = MediaQuery.of(context).size.width;
    scrHeight = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: Container(
          height: scrHeight,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [Colors.blue[800], Colors.blue[400]],
            ),
          ),
          child: Stack(
            children: <Widget>[
              // header text
              Positioned(
                left: 0.0,
                right: 0.0,
                top: 100.0,
                child: Align(
                  child: Container(
                    child: buildHeaderText(),
                  ),
                ),
              ),
              // 2 buttons
              Positioned(
                left: 0.0,
                right: 0.0,
                top: 300.0,
                child: Align(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildGetJobsButton(context),
                        const SizedBox(width: 30.0),
                        buildHireStaffButton(context),
                      ],
                    ),
                  ),
                ),
              ),
              // followings are bubble and stars
              const Positioned(
                left: 50.0,
                top: 440.0,
                child: const GlowingStars(),
              ),
              const Positioned(
                right: 50.0,
                bottom: 440.0,
                child: const GlowingStars(),
              ),
              const Positioned(
                left: 50.0,
                top: 100.0,
                child: const GlowingStars(),
              ),
              const Positioned(
                left: 200.0,
                top: 450.0,
                child: const Bubble(size: 20.0),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void navigateToWorkerRegPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerRegPageParent(fromGuest: false);
    }));
  }

  void navigateToEmployerRegPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EmployerRegistrationPageParent();
    }));
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Press again to exit");
      print("SINGLE PRESSED");
      return Future.value(false);
    }
    print("DOUBLE PRESSED");
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    // return Future.value(true);
  }

  Widget buildHireStaffButton(BuildContext context) {
    return Material(
      elevation: 30.0,
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          navigateToEmployerRegPage(context);
        }, // needed
        child: Container(
          width: scrWidth * 0.33,
          height: 150.0,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.blue,
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: const Text(
            "HIRE STAFF",
            style: const TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontFamily: "RussoOne",
              letterSpacing: 2.5,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ),
      ),
    );
  }

  Widget buildGetJobsButton(BuildContext context) {
    return Material(
      elevation: 30.0,
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          navigateToWorkerRegPage(context);
        }, // needed
        child: Container(
          width: scrWidth * 0.33,
          height: 150.0,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(30.0),
            color: Colors.white,
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 22.0),
          child: const Text(
            "GET JOB",
            style: const TextStyle(
              color: Colors.black,
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              fontFamily: "RussoOne",
              letterSpacing: 2.5,
            ),
          ),
        ),
      ),
    );
  }

  Widget buildHeaderText() {
    return Column(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: const Text(
            AppConfig.appName,
            style: const TextStyle(
                color: Colors.white, fontSize: 35.0, fontFamily: "RussoOne"),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: const Text(
            "Simple Job Search",
            style: const TextStyle(
                color: Colors.white, fontSize: 20.0, fontFamily: "RussoOne"),
          ),
        ),
      ],
    );
  }
}
