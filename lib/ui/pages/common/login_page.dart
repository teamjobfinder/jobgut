import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jobs/blocs/loginBloc/login_bloc.dart';
import 'package:jobs/blocs/loginBloc/login_event.dart';
import 'package:jobs/blocs/loginBloc/login_state.dart';
import 'package:jobs/data/repositories/commonRepository/users_repository.dart';
import 'package:jobs/data/repositories/employers_repository.dart';
import 'package:jobs/data/repositories/workers_repository.dart';
import 'package:jobs/data/services/auth_service.dart';
import 'package:jobs/data/services/users_service.dart';
import 'package:jobs/ui/pages/common/employee_employer_page.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/ui/pages/employer/employer_registration_page.dart';
import 'package:jobs/ui/pages/worker/worker_home.dart';
import 'package:jobs/ui/pages/worker/worker_reg_page.dart';
import 'package:jobs/ui/widgets/bubble.dart';
import 'package:jobs/ui/widgets/widgets_collection.dart';
import 'package:jobs/utils/connectivity_service.dart';
import 'package:meta/meta.dart';
import 'package:progress_dialog/progress_dialog.dart';

/**
 * const checked
 */
class LoginPageParent extends StatelessWidget {
  bool loginWorker;

  AuthService authService = AuthService();
  UsersService usersService = UsersService();
  WorkersRepository workersRepository = WorkersRepository();
  EmployersRepository employersRepository = EmployersRepository();

  LoginPageParent({@required this.loginWorker});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(
        authService: authService,
        usersService: usersService,
        workersRepository: workersRepository,
        employersRepository: employersRepository,
      ),
      child: LoginPage(loginWorker: loginWorker),
    );
  }
}

class LoginPage extends StatefulWidget {
  bool loginWorker;

  LoginPage({@required this.loginWorker});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var loginFormKey = GlobalKey<FormState>();

  TextEditingController emailControlller = TextEditingController();
  TextEditingController passwordControlller = TextEditingController();
  LoginBloc loginBloc;
  UsersRepository usersRepository = UsersRepository();
  ProgressDialog progressDialog;

  @override
  Widget build(BuildContext context) {
    progressDialog = ProgressDialog(context);
    var scrWidth = MediaQuery.of(context).size.width;
    var scrHeight = MediaQuery.of(context).size.height;
    loginBloc = BlocProvider.of<LoginBloc>(context);

    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: Form(
          key: loginFormKey,
          child: SingleChildScrollView(
            child: Container(
              height: scrHeight,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.blue[800], Colors.blue[400]],
                ),
              ),
              child: Stack(
                children: <Widget>[
                  // bloc part
                  BlocListener<LoginBloc, LoginState>(
                    listener: (context, state) {
                      if (state is EmployerLoginSuccess) {
                        progressDialog.hide();
                        navigateToEmployerHomepage(context);
                      } else if (state is WorkerLoginSuccess) {
                        progressDialog.hide();
                        navigateToWorkerHomePage(context);
                      } else if (state is LoginFailure) {
                        progressDialog.hide();
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text(state.message),
                        ));
                      }
                    },
                    child: BlocBuilder<LoginBloc, LoginState>(
                      builder: (context, state) {
                        if (state is LoginInitial) {
                          print("login Initial");
                          return Container();
                        } else if (state is LoginLoading) {
                          print("login Loading");
                          return buildLoadingUi();
                        } else if (state is EmployerLoginSuccess) {
                          print("employerlogin Success");
                          return Container();
                        } else if (state is WorkerLoginSuccess) {
                          print("WorkerLogin Success");
                          return Container();
                        } else if (state is LoginFailure) {
                          print("Login failure");
                          progressDialog.hide();
                          return Container();
                        }
                      },
                    ),
                  ),
                  // email sign in part
                  buildEmailLoginPart(scrHeight, scrWidth),
                  // text
                  Positioned(
                    bottom: 150.0,
                    left: 0.0,
                    right: 0.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: const Text(
                        "OR",
                        style: const TextStyle(color: Colors.white),
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                    ),
                  ),
                  // text
                  Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 100.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: const Text(
                        "Sign In With",
                        style: const TextStyle(color: Colors.white),
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                    ),
                  ),
                  // google sign in button
                  Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 60.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildGoogleSignInUi(),
                        buildFacebookSignInUi(),
                      ],
                    ),
                  ),
                  // followings are bubbles
                  const Positioned(
                    right: 20.0,
                    top: 40.0,
                    child: const Bubble(
                      size: 70.0,
                    ),
                  ),
                  const Positioned(
                    left: 20.0,
                    bottom: 40.0,
                    child: const Bubble(
                      size: 50.0,
                    ),
                  ),
                  const Positioned(
                    right: 30.0,
                    bottom: 130.0,
                    child: const Bubble(
                      size: 40.0,
                      color: Colors.white54,
                    ),
                  ),
                  const Positioned(
                    left: 130.0,
                    top: 110.0,
                    child: const Bubble(
                      size: 20.0,
                    ),
                  ),
                  const Positioned(
                    left: 100.0,
                    top: 110.0,
                    child: const Bubble(size: 10.0),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  // google sign in event added here
  Widget buildGoogleSignInUi() {
    return Align(
      child: GoogleSignInButton(
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();
          if (isConnected) {
            progressDialog.show();
            loginBloc.add(GoogleSignInPressed(isWorker: widget.loginWorker));
          } else {
            progressDialog.hide();
            Fluttertoast.showToast(
              msg: "Check Internet Connection",
            );
          }
          progressDialog.hide();
        },
        borderRadius: 50.0,
        text: "Google",
      ),
    );
  }

  Widget buildEmailLoginPart(double scrHeight, double scrWidth) {
    return Container(
      margin: EdgeInsets.only(top: 80.0),
      child: Column(
        children: <Widget>[
          // text
          Container(
            padding: const EdgeInsets.only(top: 50.0, bottom: 35.0),
            child: const Text(
              "Sign In",
              style: const TextStyle(
                fontSize: 30.0,
                color: Colors.white,
                fontFamily: "RussoOne",
              ),
            ),
          ),
          // email textfield
          Container(
            width: scrWidth * 0.85,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Enter a valid email address";
                  }
                },
                style: const TextStyle(color: Colors.white),
                controller: emailControlller,
                decoration: InputDecoration(
                  errorStyle: const TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "E-mail",
                  hintStyle: const TextStyle(color: Colors.white),
                  border: const OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: Icon(Icons.email, color: Colors.white),
                ),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
          ),
          const SizedBox(height: 10.0),
          // password textfield
          Container(
            width: scrWidth * 0.85,
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Material(
              elevation: 10.0,
              color: Colors.blue[300],
              child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Password can't be empty";
                  }
                  if (value.length < 6) {
                    return "Password length should be at least 6";
                  }
                },
                style: const TextStyle(color: Colors.white),
                controller: passwordControlller,
                decoration: InputDecoration(
                  errorStyle: const TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.blue[300],
                  hintText: "Password",
                  hintStyle: const TextStyle(
                    color: Colors.white,
                  ),
                  border: const OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue[300], width: 5.0),
                  ),
                  prefixIcon: const Icon(Icons.lock, color: Colors.white),
                ),
                obscureText: true,
              ),
            ),
          ),
          const SizedBox(height: 10.0),
          // login button
          buildLoginButton(scrWidth),
          // guest sign in button
          widget.loginWorker == true
              ? buildGuestSignInButton(scrWidth)
              : const SizedBox(height: 0.0),
          // text and signupscreen homescreen navigation
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  "New Here?   ",
                  style: const TextStyle(color: Colors.white),
                ),
                InkWell(
                  onTap: () {
                    widget.loginWorker == true
                        ? navigateToSignUpWorker(context)
                        : navigateToSignUpEmployer(context);
                  },
                  child: const Text(
                    "Sign Up",
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),

                const SizedBox(width: 10.0),
                const Text("Or   ",
                    style: const TextStyle(color: Colors.white)),
                // go home
                InkWell(
                  onTap: () {
                    navigateToAppHome(context);
                  },
                  child: const Text(
                    "GO HOME",
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  void navigateToSignUpWorker(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerRegPageParent(fromGuest: false);
    }));
  }

  void navigateToSignUpEmployer(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EmployerRegistrationPageParent();
    }));
  }

  Widget buildLoadingUi() {
    return const Center(
      child: const CircularProgressIndicator(),
    );
  }

  void navigateToEmployerHomepage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return EmployerHomePage();
    }));
  }

  void navigateToWorkerHomePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return WorkerHome();
    }));
  }

  void navigateToAppHome(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return const EmployeeEmployerPage();
    }));
  }

  @override
  void dispose() {
    emailControlller.dispose();
    passwordControlller.dispose();
    super.dispose();
  }

  Future<bool> onWillPop() {
    return Future.value(false);
  }

  // email login event added here
  Widget buildLoginButton(double scrWidth) {
    return MaterialButton(
      minWidth: scrWidth * 0.8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
      child: Text(
        "LOGIN",
        style: TextStyle(
          color: Colors.blue[300],
          fontFamily: "RussoOne",
        ),
      ),
      onPressed: () async {
        if (loginFormKey.currentState.validate()) {
          bool isConnected = await ConnectivityService().hasConnection();

          if (isConnected) {
            print("Login bloc added");
            progressDialog.show();
            loginBloc.add(LoginButtonPressed(
              email: emailControlller.text,
              password: passwordControlller.text,
            ));
            print("Login bloc added 2");
          } else {
            Fluttertoast.showToast(
              msg: "Check Internet Connection",
            );
          }
        }
      },
      color: Colors.white,
    );
  }

  // guest sign in event added here
  Widget buildGuestSignInButton(double scrWidth) {
    return MaterialButton(
      minWidth: scrWidth * 0.8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
      child: Text(
        "SIGN IN AS GUEST",
        style: TextStyle(
          color: Colors.blue[300],
          fontFamily: "RussoOne",
        ),
      ),
      onPressed: () async {
        bool isConnected = await ConnectivityService().hasConnection();
        if (isConnected) {
          progressDialog.show();
          loginBloc.add(SignInWorkerAsGuest());
        } else {
          Fluttertoast.showToast(
            msg: "Check Internet Connection",
          );
        }
      },
      color: Colors.white,
    );
  }

  Widget buildFacebookSignInUi() {
    return Align(
      child: FacebookSignInButton(
        text: "Facebook",
        onPressed: () async {
          bool isConnected = await ConnectivityService().hasConnection();
          if (isConnected) {
            loginBloc.add(FacebookSignInPressed(isWorker: widget.loginWorker));
          } else {
            Fluttertoast.showToast(msg: "Check Internet Connection");
          }
        },
        borderRadius: 50.0,
      ),
    );
  }
}
