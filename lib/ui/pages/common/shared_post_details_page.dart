import 'package:flutter/material.dart';
import 'package:jobs/utils/app_config.dart';

class SharedPostDetailsPage extends StatefulWidget {
  String title;
  SharedPostDetailsPage({@required this.title});
  @override
  _SharedPostDetailsPageState createState() => _SharedPostDetailsPageState();
}

class _SharedPostDetailsPageState extends State<SharedPostDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppConfig.appName),
        centerTitle: true,
      ),
      body: Container(
        child: Center(
          child: Text(widget.title),
        ),
      ),
    );
  }
}
