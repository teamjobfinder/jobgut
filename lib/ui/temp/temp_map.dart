import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/ui/pages/worker/job_search_feed_page.dart';
import 'package:jobs/utils/helper.dart';
import 'package:jobs/utils/location_service.dart';
import 'package:meta/meta.dart';

class JobMaps extends StatefulWidget {
  bool fromWorkerContext;

  JobMaps({@required this.fromWorkerContext});

  @override
  _JobMapsState createState() => _JobMapsState();
}

class _JobMapsState extends State<JobMaps> {
  final Geolocator geolocator = Geolocator()
    ..forceAndroidLocationManager = true;

  GoogleMapController googleMapController;
  // this will be returned
  TheLocation selectedLocation = TheLocation(
    latitude: 37.5665,
    longitude: 126.9780, // initial location, seoul
  );
  String searchText;
  // to track the moved marker
  LatLng markedPosition;

  Set<Marker> _markers = {};

  LocationService locationService;

  Future _mapFuture = Future.delayed(Duration(milliseconds: 250), () => true);

  _JobMapsState() {
    locationService = LocationService();
  }

  @override
  Widget build(BuildContext context) {
    print("-----------------------------Wroker Map");
    // marker for inital location
    _markers.add(
      Marker(
        markerId: MarkerId('homeMarker'),
        position: LatLng(selectedLocation.latitude, selectedLocation.longitude),
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(title: selectedLocation.name),
        draggable: false,
      ),
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          FutureBuilder(
            future: _mapFuture,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                print("Empty");
                return Container();
              } else {
                return GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                      selectedLocation.latitude,
                      selectedLocation.longitude,
                    ),
                    zoom: 15,
                  ),
                  markers: _markers,
                  onMapCreated: onMapCreated,
                  onCameraMove: ((_position) => _updatePosition(_position)),
                );
              }
            },
          ),
          // set location button
          Positioned(
            bottom: 10.0,
            left: 10,
            child: RaisedButton(
              child: Text(
                widget.fromWorkerContext == false
                    ? "Set This Location"
                    : "Show Jobs",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () async {
                print("on show jobs");
                if (markedPosition != null) {
                  Placemark markedPlacedMark = await getMarkedLocation();
                  var markedPlaceJson = markedPlacedMark.toJson();
                  Helper.logPrint("LOCPBL", markedPlaceJson.toString());
                  selectedLocation = TheLocation.fromJson(markedPlaceJson);
                  print(
                      "Will show data for searched : ${selectedLocation.country}");
                  widget.fromWorkerContext == true
                      ? Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => JobSearchFeedPage(
                              location: selectedLocation,
                            ),
                          ),
                          (Route<dynamic> route) => false,
                        )
                      : Navigator.pop(context, selectedLocation);
                } else {
                  // data for current location
                  print(
                      "Will show data for seoul : ${selectedLocation.country}");
                  widget.fromWorkerContext == true
                      ? Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => JobSearchFeedPage(
                              location: selectedLocation,
                            ),
                          ),
                          (Route<dynamic> route) => false,
                        )
                      : Navigator.pop(context, selectedLocation.country);
                }
              },
              color: Colors.teal,
            ),
          ),
          // searchfield on top
          Positioned(
            top: 30.0,
            right: 15.0,
            left: 15.0,
            height: 50.0,
            child: Material(
              elevation: 10.0,
              child: TextField(
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  hintText: "Search Location",
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search, color: Colors.blue),
                    onPressed: () {
                      if (searchText != null) {
                        searchLocation();
                      }
                    },
                  ),
                ),
                onChanged: (value) {
                  setState(() {
                    searchText = value;
                  });
                },
                onSubmitted: (value) {
                  if (searchText != null) {
                    searchLocation();
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _updatePosition(CameraPosition _position) {
    Marker marker = _markers.firstWhere(
        (p) => p.markerId == MarkerId('homeMarker'),
        orElse: () => null);

    _markers.remove(marker);
    _markers.add(
      Marker(
        markerId: MarkerId('marker_2'),
        position: LatLng(_position.target.latitude, _position.target.longitude),
        draggable: true,
        icon: BitmapDescriptor.defaultMarker,
      ),
    );
    setState(() {
      markedPosition = _position.target;
    });
  }

  void searchLocation() async {
    await geolocator.placemarkFromAddress(searchText).then((placemarks) {
      googleMapController
          .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(
            placemarks[0].position.latitude, placemarks[0].position.longitude),
        zoom: 15.0,
      )));
    });
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      googleMapController = controller;
    });
  }

  // location of the moved marker
  Future<Placemark> getMarkedLocation() async {
    List<Placemark> placeMarks;
    placeMarks = await geolocator.placemarkFromCoordinates(
        markedPosition.latitude, markedPosition.longitude);
    return placeMarks[0];
  }

  void _onShowJobs() async {
    if (markedPosition != null) {
      Placemark markedPlacedMark = await getMarkedLocation();
      var markedPlaceJson = markedPlacedMark.toJson();
      selectedLocation = TheLocation.fromJson(markedPlaceJson);
      print("Will show data for searched : ${selectedLocation.country}");
      widget.fromWorkerContext == true
          ? Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => JobSearchFeedPage(
                  location: selectedLocation,
                ),
              ),
              (Route<dynamic> route) => false,
            )
          : Navigator.pop(context, selectedLocation);
    } else {
      // data for current location
      print("Will show data for seoul : ${selectedLocation.country}");
      widget.fromWorkerContext == true
          ? Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => JobSearchFeedPage(
                  location: selectedLocation,
                ),
              ),
              (Route<dynamic> route) => false,
            )
          : Navigator.pop(context, selectedLocation.country);
    }
  }
}
