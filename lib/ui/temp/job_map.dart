import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobs/data/models/employer_model.dart';
import 'package:meta/meta.dart';

class JobMap extends StatefulWidget {
  final EmployerModel employerModel;

  const JobMap({Key key, @required this.employerModel}) : super(key: key);

  @override
  _JobMapState createState() => _JobMapState();
}

class _JobMapState extends State<JobMap> {

  final Set<Marker> _markers = {};

  @override
  void initState() { 
    super.initState();
    _markers.add(
      Marker(
        markerId: MarkerId('jobMapMarkerId'),
        position: LatLng(widget.employerModel.latitude, widget.employerModel.longitude,),
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(
          title: widget.employerModel.name,
          snippet: widget.employerModel.location,
        ),
        draggable: false,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.employerModel?.name),
        centerTitle: true,
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(
            widget.employerModel.latitude,
            widget.employerModel.longitude,
          ),
          zoom: 15.0,
        ),
        markers: _markers,
      ),
    );
  }
}
