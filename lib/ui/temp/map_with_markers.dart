import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobs/data/models/job_post_model.dart';
import 'package:jobs/data/models/the_location.dart';
import 'package:jobs/ui/pages/common/post_details_page.dart';
import 'package:meta/meta.dart';

/**
 * Instead of reading data from db, this map will put markers on given data
 * We can read from network, if needed
 */
class MapWithMarkers extends StatefulWidget {
  List<JobPostModel> jobs;
  TheLocation location;
  List<DocumentSnapshot> docSnaps;

  MapWithMarkers({
    @required this.jobs,
    @required this.location,
    @required this.docSnaps,
  });

  @override
  _MapWithMarkersState createState() => _MapWithMarkersState();
}

class _MapWithMarkersState extends State<MapWithMarkers> {
  GoogleMapController googleMapController;

  var scrWidth;

  Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    int id = 0;
    for (var item in widget.jobs) {
      _markers.add(
        Marker(
          markerId: MarkerId("$id"),
          position: LatLng(item.latitude, item.longitude),
          draggable: false,
          icon: BitmapDescriptor.defaultMarker,
          infoWindow: InfoWindow(
            title: widget.jobs[id].title,
          ),
        ),
      );
      id++;
    }
  }

  @override
  Widget build(BuildContext context) {
    scrWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            GoogleMap(
              initialCameraPosition: CameraPosition(
                target:
                    LatLng(widget.location.latitude, widget.location.longitude),
                zoom: 7.0,
              ),
              markers: _markers,
              onMapCreated: _onMapCreated,
            ),
            // top banner
            Positioned(
              top: 30.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                alignment: Alignment.center,
                height: 30.0,
                width: 200.0,
                margin: EdgeInsets.symmetric(horizontal: 40.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Text(
                  widget.jobs.length > 1
                      ? "${widget.jobs.length} jobs found here"
                      : "${widget.jobs.length} job found here",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.deepOrange,
                  ),
                ),
              ),
            ),
            // slider at bottom
            Positioned(
              bottom: 10.0,
              right: 0.0,
              left: 0.0,
              child: CarouselSlider(
                items: _buildSliderItems(),
                options: CarouselOptions(
                  aspectRatio: 16 / 6,
                  onPageChanged: (pos, _) {
                    JobPostModel jobPostModel = widget.jobs[pos];
                    setState(() {
                      _moveCamera(jobPostModel, pos);
                    });
                  },
                ),
              ),
            ),
            // close button
            Positioned(
              top: 20.0,
              left: 0.0,
              child: IconButton(
                icon: Icon(Icons.close, color: Colors.deepOrange),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      googleMapController = controller;
    });
  }

  List<Widget> _buildSliderItems() {
    List<Widget> sliders = [];
    for (var i = 0; i < widget.docSnaps.length; i++) {
      DocumentSnapshot docSnap = widget.docSnaps[i];
      DocumentReference docRef = docSnap.reference;
      JobPostModel jobPostModel = widget.jobs[i];
      Widget item = InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return PostDetailsPageParent(
              isWorker: true,
              jobPostModel: jobPostModel,
              documentReference: docRef,
              documentSnapshot: docSnap,
            );
          }));
        },
        child: Container(
          height: 100.0,
          margin: const EdgeInsets.symmetric(horizontal: 20.0),
          width: scrWidth - 30,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            color: Colors.white,
          ),
          child: Material(
            elevation: 10.0,
            borderRadius: BorderRadius.circular(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                // job position
                Container(
                  padding: const
                      EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0,),
                  child: Text(
                    jobPostModel.jobPosition,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                      color: Colors.orange,
                      fontFamily: "RussoOne",
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                // salary range
                Container(
                  padding: const
                      EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0,),
                  child: Text(
                    "${jobPostModel.salaryRange} ${jobPostModel.salaryType}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
                // location
                Container(
                  padding:
                      EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
                  child: Text(
                    "${jobPostModel.name}, ${jobPostModel.subAdministrativeArea}, ${jobPostModel.country}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
      sliders.add(item);
    }
    return sliders;
  }

  Widget _buildBottomSlider() {
    return Container(
      height: 100.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.jobs.length,
        itemBuilder: (context, position) {
          DocumentSnapshot docSnap = widget.docSnaps[position];
          DocumentReference docRef = docSnap.reference;
          JobPostModel jobPostModel = widget.jobs[position];
          return InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return PostDetailsPageParent(
                  isWorker: true,
                  jobPostModel: jobPostModel,
                  documentReference: docRef,
                  documentSnapshot: docSnap,
                );
              }));
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              width: scrWidth - 30,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: Colors.white,
              ),
              child: Material(
                elevation: 10.0,
                borderRadius: BorderRadius.circular(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // job position
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 10.0),
                      child: Text(
                        jobPostModel.jobPosition,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                          fontFamily: "RussoOne",
                          color: Colors.orange,
                        ),
                      ),
                    ),
                    // salary range
                    Container(
                      padding: EdgeInsets.only(
                          left: 15.0, right: 15.0, bottom: 10.0),
                      child: Text(
                        "${jobPostModel.salaryRange} ${jobPostModel.salaryType}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    // location
                    Container(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, bottom: 10.0),
                      child: Text(
                        "${jobPostModel.name}, ${jobPostModel.subAdministrativeArea}, ${jobPostModel.country}",
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  _moveCamera(JobPostModel jobPostModel, int markerId) {
    googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            jobPostModel.latitude,
            jobPostModel.longitude,
          ),
          zoom: 10.0,
        ),
      ),
    );
    MarkerId id = MarkerId("$markerId");
    googleMapController.showMarkerInfoWindow(id);
  }
}
