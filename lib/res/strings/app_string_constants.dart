/**
 * This file holds the string keys like firestore collection names
 */
class AppStringConstants {

  static const String appTitle = "Jobbee";
  
  static const String applications = "applications";
  static const String posts = "posts";
  static const String employers = "employers";
  static const String workers = "workers";
  static const String users = "users";
  static const String profile = "Profile";
  static const String employerType = "Employer";
  static const String workerType = "Worker";
  static const String jobImagesRef = "images/jobImages/";
  static const String profileImageRef = "images/profilePics/";
  static const String jobPostId = "jobPostId";
  static const String type = "type";
  static const String workerId = "workerId";

  static const String country = "country";

  // field of workerModel
  static String workerLocation = "location";
  static String workerJobType = "jobType";
  static String workerCategory = "jobcategory";
  static String nationality = "nationality";
  static String language = "language";

  // for timestamo
  static String createdAt = "createdAt";
  

}
