class JobType {
  // job type
  static const String fullTime = "Full Time";
  static const String partTime = "Part Time";
  static const String freelancer = "Freelancer";
}
