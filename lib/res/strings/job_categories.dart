class JobCategories {

  static const String designer = "Designer";
  static const String uiux = "UI/UX";
  static const String engineer = "Engineer";
  static const String doctor = "Doctor";
  static const String chef = "Chef";
  static const String cleaning = "Cleaning";
  static const String warehouse = "Warehouse";
  static const String official = "Official";
  static const String salesAndMarketing = "Sales & Marketing";
  static const String driverAndCourier = "Driver & Courier";
  static const String construction = "Construction";
  static const String teacher = "Teacher";
  static const String lawyer = "Lawyer";
  static const String photographer = "Photographer";
  static const String receptionist = "Receptionist";
  static const String customerSupport = "Customer Support";
  static const String eventManagement = "Event Management";
  static const String waiter = "Waiter";
  static const String bartender = "Bartender";
  static const String kitchen_helper = "Kitchen Helper";
  static const String salesman = "Salesman";
  static const String cosmetics = "Cosmetics";
  static const String hair_designer = "Hair Designer";
  static const String barista = "Barista";
  static const String language_tutor = "Language Tutor";
  static const String others = "Others";
}
