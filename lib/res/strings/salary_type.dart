class SalaryType {
  static const String hourly = "HOURLY";
  static const String daily = "DAILY";
  static const String weekly = "WEEKLY";
  static const String monthly = "MONTHLY";
  static const String yearly = "YEARLY";
  static const String fixed = "FIXED";
}