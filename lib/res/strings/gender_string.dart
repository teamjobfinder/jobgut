class GenderString {
  static const String male = "MALE";
  static const String female = "FEMALE";
  static const String any = "ANY";
}