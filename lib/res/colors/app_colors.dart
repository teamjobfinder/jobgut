import 'package:flutter/material.dart';

class AppColors {
  static LinearGradient blueWhiteGradient = LinearGradient(
    colors: [
      Color.fromARGB(255, 65, 178, 243),
      Color.fromARGB(255, 23, 118, 173)
    ],
  );

  static LinearGradient greyWhiteGradient = LinearGradient(
    colors: [
      Colors.grey,
      Colors.grey[700],
    ],
  );

  // colors
  static var randomColors = [
    Colors.blue,
    Colors.teal,
    Colors.purple,
    Colors.orange[900],
    Colors.indigo[900],
  ];

  static Color bgColor = Colors.grey[100];

  static Color textBoxColor = Colors.grey[200];

  static Color deepBlue = Color.fromARGB(1, 108, 0, 123);
  static Color lightBlue = Color.fromARGB(1, 105, 69, 197);
  static Color blueDeep = Color(0xff4942CF);
  static Color blueLight = Color(0xff7484F7);

  static Color fullTimeJob = Colors.deepOrange;
  static Color partTimeJob = Colors.red;
  static Color freelanceJob = Color.fromARGB(255, 232, 14, 208);

}
