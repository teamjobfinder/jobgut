class JobExperience {
  static const List<String> experiences = [
    "Not Required",
    "6 Months",
    "1 Year",
    "2 Years",
    "3 Years",
    "4 Years",
    "5 Years",
  ];
}
