class AppStrings {
  static String dummyDescription =
      "This document is meant for Android developers looking to apply their existing Android knowledge to build mobile apps with Flutter. If you understand the fundamentals of the Android framework then you can use this document as a jump start to Flutter development. Your Android knowledge and skill set are highly valuable when building with Flutter, because Flutter relies on the mobile operating system for numerous capabilities and configurations. Flutter is a new way to build UIs for mobile, but it has a plugin system to communicate with Android (and iOS) for non-UI tasks. If you’re an expert with Android, you don’t have to relearn everything to use Flutter. This document can be used as a cookbook by jumping around and finding questions that are most relevant to your needs.";
  static String dummyAboutMe =
      "Flutter is Google’s UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.";
  static String dummyEmail = "example@example.com";
  static int dummyPhoneNumber = 12345567889011;
  static String dummyLocation = "Seoul, Korea";
  static String dummySkill =
      "Web Developer, UI/UX design, Photography, Wordpress, Flutter, iOS, Android";

  static String appName = "JobGut";

  static const String ovMenuEdit = "Edit";
  static const String ovMenuDelete = "Delete";
  static const String ovMenuShare = "Share";

  static const List<String> ovMenuChoices = [
    ovMenuEdit,
    ovMenuDelete,
    ovMenuShare
  ];

  // field of workerModel
  static String workerLocation = "location";
  static String workerJobType = "jobType";
  static String workerCategory = "jobcategory";

  // job type
  static const String fullTime = "Full Time";
  static const String partTime = "Part Time";
  static const String freelancer = "Freelancer";

  // preferred gender
  static const String male = "Male";
  static const String female = "Female";
  static const String any = "Any";

  // job language
  static const List<String> genders = [
    'Male',
    'Female',
  ];

  static String noGoogleAccountFound = "No Google Account Found/Selected";
  static String noInternetConnection = "No Internet Connection";

  static String goolePlayAppLink =
      "https://play.google.com/store/apps/details?id=com.jobbee.jobs";

  static String logBuildFunction = "************* inside build() **********";
}
