class LanguageCodes {
  static const String chinese = "zh-TW";
  static const String chineseSimple = "zh-CN";
  static const String korean = "ko";
  static const String indonesian = "id";
  static const String thai = "th";
  static const String japanese = "ja";
  static const String vietnam = "vi";
  static const String english = "en";
  static const String spanish = "es";
  static const String russian = "ru";

  // translateable languages
  static const List<String> languages = [
    "English",
    "Chinese",
    "Simple Chinese",
    "Korean",
    "Indonesian",
    "Thai",
    "Japanese",
    "Vietnam",
    "Spanish",
    "Russian",
  ];

  // job language
  static const List<String> jobLanguages = [
    "English",
    "Chinese",
    "Simple Chinese",
    "Korean",
    "Indonesian",
    "Thai",
    "Japanese",
    "Vietnam",
    "Spanish",
    "Russian",
    "Others"
  ];

}
