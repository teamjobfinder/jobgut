// import 'package:permission_handler/permission_handler.dart';

// class PermissionService {
//   PermissionHandler permissionHandler = PermissionHandler();

//   // generic function to request permission
//   Future<bool> requestPermission(PermissionGroup permissionGroup) async {
//     var result = await permissionHandler.requestPermissions([permissionGroup]);
//     if (result[permissionGroup] == PermissionStatus.granted) {
//       return true;
//     }
//     return false;
//   }

//   // generic function to check any permission
//   Future<bool> hasPermission(PermissionGroup permission) async {
//     var permissionStatus =
//         await permissionHandler.checkPermissionStatus(permission);
//     return permissionStatus == PermissionStatus.granted;
//   }

//   // request for location
//   Future<bool> requestLocationPermission() async {
//     return requestPermission(PermissionGroup.locationWhenInUse);
//   }

//   // check permission for location
//   Future<bool> hasLocationPermission() async {
//     return hasPermission(PermissionGroup.location);
//   }
// }
