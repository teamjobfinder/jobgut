// kind of service
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// contains dialogs of various types and styles. No dependency on flutter
class DialogHelper {
  /// dialog with ok button & cancel button
  static void showClassicDialog(
    BuildContext context,
    String title,
    String message, {
    Function onCancel,
    Function onOk,
  }) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: <Widget>[
              MaterialButton(
                child: Text("OK"),
                color: Colors.green,
                elevation: 10.0,
                onPressed: onOk,
              ),
              MaterialButton(
                child: Text("Cancel"),
                color: Colors.red,
                elevation: 10.0,
                onPressed: onCancel,
              ),
            ],
          );
        });
  }
}
