import 'package:location/location.dart';

/**
 * Seperate class because of the conflict between two location packages
 */

class LocationHelper {
  var location = Location();

  // checks gps and requets
  Future requestLocation() async {
    if (!await location.serviceEnabled()) {
      location.requestService();
    }
  }

  // only requests
  Future showLocationDialog() async {
    await location.requestPermission();
  }

}
