class AppConfig {

  static const String appName = "JOBGUT";
  static const String appVersion = "1.1.2";
  static const String packageName = "com.jobbee.jobs";
  static const String playStoreLink = "https://play.google.com/store/apps/details?id=com.jobbee.jobs";
  
}