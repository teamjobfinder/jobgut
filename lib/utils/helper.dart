import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jobs/ui/pages/employer/employer_home_page.dart';
import 'package:jobs/utils/error_codes.dart';

class Helper {
  static Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static Future<File> pickImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    return image;
  }

  static bool isNullOrEmpty(Object o) => o == null || "" == o;

  static String convertErrorCodesToMessage(String code) {
    String authError = "";
    switch (code) {
      case ErrorCodes.ERROR_WEAK_PASSWORD:
        authError = ErrorMessages.MSG_WEAK_PASSWORD;
        break;
      case ErrorCodes.ERROR_INVALID_EMAIL:
        authError = ErrorMessages.MSG_INVALID_EMAIL;
        break;
      case ErrorCodes.ERROR_EMAIL_ALREADY_IN_USE:
        authError = ErrorMessages.MSG_EMAIL_ALREADY_IN_USE;
        break;
      case ErrorCodes.ERROR_WRONG_PASSWORD:
        authError = ErrorMessages.MSG_WRONG_PASSWORD;
        break;
      case ErrorCodes.ERROR_USER_NOT_FOUND:
        authError = ErrorMessages.MSG_USER_NOT_FOUND;
        break;
      case ErrorCodes.ERROR_USER_DISABLED:
        authError = ErrorMessages.MSG_USER_DISABLED;
        break;
      case ErrorCodes.ERROR_TOO_MANY_REQUESTS:
        authError = ErrorMessages.MSG_TOO_MANY_REQUESTS;
        break;
      case ErrorCodes.ERROR_OPERATION_NOT_ALLOWED:
        authError = ErrorMessages.MSG_OPERATION_NOT_ALLOWED;
        break;
      case ErrorCodes.ERROR_INVALID_CREDENTIAL:
        authError = ErrorMessages.MSG_INVALID_CREDENTIAL;
        break;
      case ErrorCodes.ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL:
        authError = ErrorMessages.MSG_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL;
        break;
      case ErrorCodes.ERROR_INVALID_ACTION_CODE:
        authError = ErrorMessages.MSG_INVALID_ACTION_CODE;
        break;
      case ErrorCodes.ERROR_CREDENTIAL_ALREADY_IN_USE:
        authError = ErrorMessages.MSG_CREDENTIAL_ALREADY_IN_USE;
        break;
      case ErrorCodes.ERROR_REQUIRES_RECENT_LOGIN:
        authError = ErrorMessages.MSG_REQUIRES_RECENT_LOGIN;
        break;
      case ErrorCodes.ERROR_PROVIDER_ALREADY_LINKED:
        authError = ErrorMessages.MSG_PROVIDER_ALREADY_LINKED;
        break;
      default:
        authError = ErrorMessages.MSG_DEFAULT;
        break;
    }
    return authError;
  }

  static double getScreenHeight(BuildContext context) {
    return screenSize(context).height;
  }

  static double getScreenWidth(BuildContext context) {
    return screenSize(context).width;
  }

  static double screenHeightPortion(
    BuildContext context, {
    double time = 1,
    double reducedBy = 0.0,
  }) {
    return (screenSize(context).height * time) - reducedBy;
  }

  static double screenWidthPortion(
    BuildContext context, {
    double time = 1,
    double reducedBy = 0.0,
  }) {
    return (screenSize(context).width * time) - reducedBy;
  }

  // show dialog
  static void handleJobPostCloseDialog(
    BuildContext context,
  ) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Cancel Posting?"),
          content: new Text("Are you sure? You will lose the draft."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => EmployerHomePage()),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  // format a datetime string
  static String formatDateTime(String dateTimeString) {
    var list = dateTimeString?.split("-");
    String monthName;
    String monthNumber = list[1];

    switch (monthNumber) {
      case "01":
        monthName = "January";
        break;
      case "02":
        monthName = "February";
        break;
      case "03":
        monthName = "March";
        break;
      case "04":
        monthName = "April";
        break;
      case "05":
        monthName = "May";
        break;
      case "06":
        monthName = "June";
        break;
      case "07":
        monthName = "July";
        break;
      case "08":
        monthName = "August";
        break;
      case "09":
        monthName = "September";
        break;
      case "10":
        monthName = "October";
        break;
      case "11":
        monthName = "November";
        break;
      case "12":
        monthName = "December";
        break;
    }
    return "${list[2]} $monthName, ${list[0]}";
  }

  static void logPrint(String tag, [String message]) {
    debugPrint("$tag  ::  $message");
  }

}
