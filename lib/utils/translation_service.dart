import 'package:jobs/data/models/job_post_model.dart';
import 'package:translator/translator.dart';

class TranslationService {
  static Future<String> translateInto(String languageCode, String text) async {
    var translator = GoogleTranslator();
    var txt = await translator.translate(text, to: 'bn');
    return txt;
  }

  static Future<JobPostModel> translateJobPost(
      String languageCode, JobPostModel jobPostModel) async {
    var translator = GoogleTranslator();
    JobPostModel copiedPost = jobPostModel;
    String title = await translator.translate(copiedPost.title, to: languageCode);
    
    String jobDescription =
        await translator.translate(copiedPost.jobDescription, to: languageCode);
    copiedPost.title = title;
    copiedPost.jobDescription = jobDescription;
    return copiedPost;
  }
}
