import 'dart:io';

class ConnectivityService {
  
  Future<bool> hasConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet Connected");
        return true;
      } else {
        print("Internet not connected");
        return false;
      }
    } on SocketException catch (_) {
      print("Socketexception, no internet");
      return false;
    }
  }

}