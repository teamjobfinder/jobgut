class ErrorCodes {
  static const String ERROR_WEAK_PASSWORD = "ERROR_WEAK_PASSWORD";
  static const String ERROR_INVALID_EMAIL = "ERROR_INVALID_EMAIL";
  static const String ERROR_EMAIL_ALREADY_IN_USE = "ERROR_EMAIL_ALREADY_IN_USE";
  static const String ERROR_WRONG_PASSWORD = "ERROR_WRONG_PASSWORD";
  static const String ERROR_USER_NOT_FOUND = "ERROR_USER_NOT_FOUND";
  static const String ERROR_USER_DISABLED = "ERROR_USER_DISABLED";
  static const String ERROR_TOO_MANY_REQUESTS = "ERROR_TOO_MANY_REQUESTS";
  static const String ERROR_OPERATION_NOT_ALLOWED =
      "ERROR_OPERATION_NOT_ALLOWED";
  static const String ERROR_INVALID_CREDENTIAL = "ERROR_INVALID_CREDENTIAL";
  static const String ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL =
      "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL";
  static const String ERROR_INVALID_ACTION_CODE = "ERROR_INVALID_ACTION_CODE";
  static const String ERROR_CREDENTIAL_ALREADY_IN_USE =
      "ERROR_CREDENTIAL_ALREADY_IN_USE";
  static const String ERROR_REQUIRES_RECENT_LOGIN =
      "ERROR_REQUIRES_RECENT_LOGIN";
  static const String ERROR_PROVIDER_ALREADY_LINKED =
      "ERROR_PROVIDER_ALREADY_LINKED";
}

class ErrorMessages {
  static const String MSG_WEAK_PASSWORD = "Password is too weak";
  static const String MSG_INVALID_EMAIL = "The email is invalid";
  static const String MSG_EMAIL_ALREADY_IN_USE = "The email is already in use";
  static const String MSG_WRONG_PASSWORD = "Password is wrong";
  static const String MSG_USER_NOT_FOUND = "User not found";
  static const String MSG_USER_DISABLED = "The user is disabled";
  static const String MSG_TOO_MANY_REQUESTS = "Too many requests for this user";
  static const String MSG_OPERATION_NOT_ALLOWED =
      "This operation is not allowed";
  static const String MSG_INVALID_CREDENTIAL = "Invalid credential";
  static const String MSG_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL =
      "This account exists with different credential";
  static const String MSG_INVALID_ACTION_CODE =
      "Action code in the link is malformed, expired or has already been used";
  static const String MSG_CREDENTIAL_ALREADY_IN_USE =
      "Account is already in use by a different account";
  static const String MSG_REQUIRES_RECENT_LOGIN = "Requires recent login";
  static const String MSG_PROVIDER_ALREADY_LINKED =
      "Current user has already an account of this type linked";
  static const String MSG_DEFAULT = "Unknown Error!";
}
