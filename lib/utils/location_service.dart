import 'package:geolocator/geolocator.dart';
import 'package:jobs/data/models/the_location.dart';

class LocationService {
  final Geolocator geolocator = Geolocator()
    ..forceAndroidLocationManager = true;

  Future<TheLocation> getCurrentLocation() async {
    List<Placemark> placeMarks;
    await geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) async {
      placeMarks = await Geolocator().placemarkFromCoordinates(
        position.latitude,
        position.longitude,
        localeIdentifier: "en_US",
      );
    }).catchError((e) {
      print("Location error $e");
    });
    var json = placeMarks[0].toJson();
    TheLocation location = TheLocation.fromJson(json);
    return location;
  }

  Future<bool> isGpsOn() async {
    if (await Geolocator().isLocationServiceEnabled()) {
      return true;
    } else {
      return false;
    }
  }

}
